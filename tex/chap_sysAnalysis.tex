\chapter{Systems Analysis}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{System Types}\end{center}

	A system may possess a combination of these attributes.
	
	\begin{tabular}{|c l c|}
		\hline
		\textit{Autonomous} & \multirow{2}{*}{The evolution does not explicitly depend on time} & \multirow{2}{*}{$\dot{\vec{x}} = \vec{f}(\vec{x})$}\\
		\textit{Time-Invariant} & &\\
		\textbf{OR}&&\\
		\textit{Nonautonomous} & \multirow{2}{*}{System evolution explicitly depends on time} & \multirow{2}{*}{$\dot{\vec{x}} = \vec{f}(\vec{x},t)$}\\
		\textit{Time-Varying} & &\\
		\hline \hline
		\textit{Linear} & The system is defined as a linear function of the state variables & $\dot{\vec{x}} = \coxmat{A} \vec{x}$\\
		\textbf{OR}&&\\
		\textit{Nonlinear} & The system is defined as an arbitrary function of the state variables & $\dot{\vec{x}} = \vec{f}(\vec{x}, \cdot)$\\
		\hline \hline
		\textit{Continuous} & The system is defined at every value of time & $\dot{\vec{x}} = \vec{f}(\vec{x}, \cdot)$\\
		\textbf{OR}&&\\
		\textit{Discrete} & The system is defined at discrete time intervals & $\vec{x}(k+1) = \vec{f}(\vec{x}(k), \cdot)$\\
		\hline
	\end{tabular}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Strategies}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Nonlinear Systems}\end{center}
		\vspace{-1em}
		\begin{enumerate}
			\item Linearize about equilibrium solutions and explore behavior nearby.
			\item Nonlinear stability analysis (Lyapunov Functions) 
		\end{enumerate}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Linear Systems}\end{center}
		\vspace{-1em}
		\begin{enumerate}
			\item Put in state space form, explore eigenvalues/eigenvectors, stability
			\item Convert to transfer function, apply frequency analysis
		\end{enumerate}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Equilibrium Solutions}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Continuous}\end{center}
		\vspace{-1em}
		Set all time-derivatives to zero, solve for state that satisfies resulting equations. E.g.,
		\begin{align*}
			&\dot{x} = \sin(x) \quad \to \quad 0 = \sin(x)\\
			&\therefore x = \pm 2n\pi, \,\, n = 0,1,2,\dots
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Discrete}\end{center}
		\vspace{-1em}
		Subsequent iterations are the same, i.e., $\vec{x}(k+1) = \vec{x}(k) = \vec{x}_e \, \forall \, k$. E.g.,
		\begin{align*}
			&x(k+1) = x(k)^2 \quad \to \quad x_e = x_e^2\\
			&\therefore x_e = 0, 1
		\end{align*}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Linearization of Nonlinear Equations}\end{center}
	\index{Nonlinear Systems!linearization}
	$f$ is a function of a set of variables $x_1,\,\, \dots,\,\, x_m$ and their derivatives up to $n$th order, i.e., 
	\[f = f \left( x_1,\,\, \dots,\,\, x_m,\,\, \dot{x}_1,\,\, \dots,\,\, \dot{x}_m,\,\, \dots,\,\, x^{(n)}_1,\,\, \dots,\,\, x^{(n)}_m \right)\]
	The linear, variational equation of $f$ about some reference solution $f_*$ is
	\[
		\delta f = f - f_* = \eval[3]{\dpd{f}{x_1}}_* \delta x_1 + \dots + \eval[3]{\dpd{f}{\dot{x}_1}}_* + \dots = \sum\limits_{i=1}^m \sum\limits_{o = 0}^n  \eval[3]{\dpd{f}{x_i}}_* \delta x_i^{(o)}
	\]
	Analysis of this linear system (may be transformed to 1st order vector diff eq., find eigenvalues/vectors, etc.) is useful to determine local behavior in nonlinear system
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Continuous Time Stability and Boundedness}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Boundedness}\end{center}
		\vspace{-1em}
		A solution $\vec{x}(t)$ is \textit{bounded} about the equilibrium $\vec{x}_e$ if $\exists\, \beta \geq 0$ such that $\norm{\vec{x}(t) - \vec{x}_e} \leq \beta \, \forall\, t \geq 0$. A solution is \textit{unbounded} if it is not bounded.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{stable} if for each $\epsilon > 0$ $\exists\, \delta > 0$ such that whenever $\norm{\vec{x}(0) - \vec{x}_e} < \delta$ one has $\norm{\vec{x}(t) - \vec{x}_e} < \epsilon\, \forall\, t \geq 0$.
	\end{minipage}

	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Global Asymptotic Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{globally asymptotically stable} (GAS) if
		\begin{enumerate}
			\item It is stable
			\item Every solution $\vec{x}(t)$ converges to $\vec{x}_e$ with increasing time: $\underset{t \to \infty}{\lim} \vec{x}(t) = \vec{x}_e$
		\end{enumerate}
		If $\vec{x}_e$ is GAS, then there are no other equilibrium states and all solutions are bounded.
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Asymptotic Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{asymptotically stable} (AS) if
		\begin{enumerate}
			\item It is stable
			\item $\exists\, R > 0$ such that whenever $\norm{\vec{x}(0) - \vec{x}_e} < R$ one has $\underset{t \to \infty}{\lim}\vec{x}(t) = \vec{x}_e$
		\end{enumerate}
		The \textit{region of attraction} of $\vec{x}_e$ is the set of initial states which converge to $\vec{x}_e$
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Global Exponential Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{globally exponentially stable} (GES) with \textit{rate of convergence} $\alpha > 0$ if $\exists\, \beta > 0$ such that every solution satisfies
		\[ \norm{\vec{x}(t) - \vec{x}_e} \leq \beta \norm{\vec{x}(0) - \vec{x}_e} \exp(-\alpha t) \quad \forall\, t \geq 0\]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Exponential Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{exponentially stable} with \textit{rate of convergence} $\alpha > 0$ if $\exists\, R>0$ and $\beta > 0$ such that whenver $\norm{\vec{x}(0) - \vec{x}_e} < R$ one has
		\[ \norm{\vec{x}(t) - \vec{x}_e} \leq \beta \norm{\vec{x}(0) - \vec{x}_e} \exp(-\alpha t) \quad \forall\, t \geq 0 \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Discrete Time Stability and Boundedness}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Boundedness}\end{center}
		\vspace{-1em}
		A solution $\vec{x}(k)$ is \textit{bounded} about the equilibrium $\vec{x}_e$ if $\exists\, \beta \geq 0$ such that $\norm{\vec{x}(k) - \vec{x}_e} \leq \beta \, \forall\, k \geq 0$. A solution is \textit{unbounded} if it is not bounded.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{stable} if for each $\epsilon > 0$ $\exists\, \delta > 0$ such that whenever $\norm{\vec{x}(0) - \vec{x}_e} < \delta$ one has $\norm{\vec{x}(k) - \vec{x}_e} < \epsilon\, \forall\, k \geq 0$.
	\end{minipage}

	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Global Asymptotic Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{globally asymptotically stable} (GAS) if
		\begin{enumerate}
			\item It is stable
			\item Every solution $\vec{x}(k)$ converges to $\vec{x}_e$: $\underset{k \to \infty}{\lim} \vec{x}(k) = \vec{x}_e$
		\end{enumerate}
		If $\vec{x}_e$ is GAS, then there are no other equilibrium states and all solutions are bounded.
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Asymptotic Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{asymptotically stable} (AS) if
		\begin{enumerate}
			\item It is stable
			\item $\exists\, R > 0$ such that whenever $\norm{\vec{x}(0) - \vec{x}_e} < R$ one has $\underset{k \to \infty}{\lim}\vec{x}(k) = \vec{x}_e$
		\end{enumerate}
		The \textit{region of attraction} of $\vec{x}_e$ is the set of initial states which converge to $\vec{x}_e$
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Global Exponential Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{globally exponentially stable} (GES) if $\exists\, 0 \leq \lambda \leq 1$ and $\beta > 0$ such that every solution satisfies
		\[ \norm{\vec{x}(k) - \vec{x}_e} \leq \beta \lambda^k \norm{\vec{x}(0) - \vec{x}_e} \quad \forall\, k \geq 0\]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Exponential Stability}\end{center}
		\vspace{-1em}
		An equilibrium state $\vec{x}_e$ is \textit{exponentially stable} if $\exists\, R>0$, $0 \leq \lambda \leq 1$, and $\beta > 0$ such that whenver $\norm{\vec{x}(0) - \vec{x}_e} < R$ one has
		\[ \norm{\vec{x}(k) - \vec{x}_e} \leq \beta \lambda^k \norm{\vec{x}(0) - \vec{x}_e} \exp(-\alpha t) \quad \forall\, k \geq 1 \]
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linear Systems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{ODE Analysis}\end{center}
	\begin{itemize}
		\item Given system, e.g. a spring-mass-damper: $m\ddot{x}(t) + c \dot{x}(t) + kx(t) = u(t)$
		\item Take Laplace transform (here assume I.C.s are zero): $(m s^2 + cs + k) X(s) = U(x)$
	\end{itemize}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Define Output}\end{center}
		\vspace{-1em}
		Let $y(t)$ be the desired output:
		\begin{itemize}
			\item Position: $y(t) = x(t) \quad \to \quad Y(s) = X(s)$
			\item Velocity: $y(t) = \dot{x}(t) \quad \to \quad Y(x) = sX(s)$
			\item Etc.
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Define Transfer Function}\end{center}
		\vspace{-1em}
		\[ G(s) = \frac{Y(s)}{U(s)} = \frac{\text{Output}}{\text{Input}} \]
		Analysis of $G(s)$ has many applications.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Transfer Function}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\[ G(s) = \frac{\text{num}(s)}{\text{den}(s)} \]
		\begin{itemize}
			\item \textit{zeros}: roots of num$(s)$; at the zeros, $G(s) \to 0$
			\item \textit{poles}: roots of den$(s)$; at the poles, $G(s) \to \infty$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item Transfer function (TF) is \textit{proper} if the order(num) $<$ order(den)
			\item If order(num) $>$ order(den), TF is not proper and no SS representation can be written
			\item TF is \textit{rational} if it can be represented as the ratio of two polynomials
			\item The TF of a finite-dimensional LTI system is always rational and proper
		\end{itemize}
	\end{minipage}
	
	\begin{itemize}
		\item If order(num) = order(den), solve to get a proper TF + constant, e.g.,
			\[ G(s) = \frac{10s^3 + 1}{s^3 + 6s + 2} \quad \to \quad \frac{10(s^3 + 6s + 2) - 60s - 19}{s^3 + 6s + 2} \quad \to \quad \frac{-60s - 19}{s^3 + 6s + 2} + 10 \]
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{State Space Representation}\end{center}
	
	\begin{minipage}[t]{0.15\textwidth}
		\begin{align*}
			\dot{\vec{x}} &= \coxmat{A} \vec{x} + \coxmat{B}\vec{u}\\
			\vec{y} &= \coxmat{C} \vec{x} + \coxmat{D} \vec{u}
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.1\textwidth}
		\begin{align*}
			\vec{x} &\in \mathbb{R}^{n \times 1}\\			
			\vec{u} &\in \mathbb{R}^{m \times 1}\\
			\vec{y} &\in \mathbb{R}^{p \times 1}\\
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.1\textwidth}
		\begin{align*}
			\coxmat{A} &\in \mathbb{R}^{n \times n}\\
			\coxmat{B} &\in \mathbb{R}^{n \times m}\\
			\coxmat{C} &\in \mathbb{R}^{p \times n}\\
			\coxmat{D} &\in \mathbb{R}^{p \times m}
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.6\textwidth}
		\begin{itemize}
			\item System is linear with constant coefficients in $\coxmat{A}$, $\coxmat{B}$, $\coxmat{C}$, and $\coxmat{D}$
			\item General case: Multi-Input, Multi-Output (MIMO) with $m > 1$ and $p > 1$
			\item Special case: Single-Input, Single-Output (SISO) with $m = p = 1$
			\item Frequently useful to transform system of 1st order ODEs into a state space (SS), or a higher-order ODE into system of 1st order into SS. As long as the eqs can be solved for the highest order term, a SS representation is available
			\item Nonlinear systems may be linearized to arrive at SS representation
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{TF $\to$ SS}\end{center}
	\begin{itemize}
		\item Matrix method: $G(s) = \coxmat{C} \left( s \coxmat{I} - \coxmat{A} \right)^{-1} \coxmat{B} + \coxmat{D}$
		\[ G(s) = \frac{\beta_{n-1}s^{n-1} + \dots + \beta_1 s + \beta_0}{s^n + \alpha_{n-1}s^{n-1} + \dots + \alpha_1 s + \alpha_0} + d \]
		\item Poles(TF) = eigenvalues($\coxmat{A}$) = roots(characteristic poly.)
	\end{itemize}
	\textbf{Realization of SISO systems:}
	\begin{itemize}
		\item controllable canonical form:
		\[ \coxmat{A} = \begin{bmatrix}
			0 & 1 & 0 & \dots & 0\\
			0 & 0 & 1 & & 0\\
			\vdots & \vdots & & \ddots & \vdots\\
			-\alpha_0 & -\alpha_2 & \dots & \dots & -\alpha_{n-1}
		\end{bmatrix}
		\qquad \coxmat{B} = \begin{Bmatrix}0 \\ 0\\ \vdots\\ 0\\ 1 \end{Bmatrix}
		\qquad \coxmat{C} = \begin{Bmatrix} \beta_0\\ \beta_1\\ \vdots \\ \beta_{n-1} \end{Bmatrix}^T
		\qquad \coxmat{D} = d		
		\]
		\item observable canonical form:
		\[ \coxmat{A} = \begin{bmatrix}
			0 & 0 & \dots & 0 & -\alpha_0\\
			1 & 0 & \dots & 0 & -\alpha_1\\
			0 & 1 & \ddots & 0 & -\alpha_2\\
			\vdots & & \ddots & \ddots & \vdots\\
			0 & 0 & \dots & 1 & -\alpha_{n-1}
		\end{bmatrix}
		\qquad \coxmat{B} = \begin{Bmatrix}\beta_0 \\ \beta_1\\ \vdots\\ \beta_{n-1}\end{Bmatrix}
		\qquad \coxmat{C} = \begin{Bmatrix} 0 \\ 0 \\ \vdots \\ 0 \\ 1 \end{Bmatrix}^T
		\qquad \coxmat{D} = d		
		\]
		\item TF must be in proper, strictly rational form before completing state space realization
		\item \textit{Minimal Realization}: a realization that has dimensions less than any other realization; cannot be reduced - easy to see in a TF, not so much in the matrices
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{LTI Stability}\end{center}
	Consider the linear time-invariant (LTI) system $\dot{\vec{x}} = \coxmat{A}\vec{x}$. The stability is determined by the eigenvalues:
	\begin{center}
	\begin{tabular}{|l |c|c|}
		\hline
		Stability & Continuous & Discrete\\
		\hline \hline
		Global Asymptotic Stability \& Boundedness & $\real{\lambda} < 0 \forall \lambda$ & $|\lambda| < 1 \forall \lambda$ \\
		\hline
		Stability \& Boundedness & $\real{\lambda} \leq 0 \forall \lambda$ & $|\lambda| \leq 1 \forall 1$\\
		``Marginal Stability'' & $\real{\lambda} < 0$ if $\lambda$ is defective & $|\lambda| < 1$ if $\lambda$ is defective\\
		\hline
		\multirow{2}{*}{Instability} & $\real{\lambda} > 0$ for any $\lambda$ & $|\lambda| > 1$ for any $\lambda$\\
		& $\real{\lambda} = 0$, $\lambda$ is defective & $|\lambda| = 1$, $\lambda$ is defective\\
		\hline
	\end{tabular}
	\end{center}
	
	\vspace{1em}
	If linear system represents linearization of nonlinear system, some info about \textit{local} stability of nonlinear system is available:
	\begin{itemize}
		\item Linear system has global asymptotic stability: nonlinear system has local exponential stability
		\item Linear system is unstable with no defective $\lambda$'s: nonlinear system is unstable
		\item Linear system is marginally stable or unstable with defective $\lambda$'s: No conclusions about nonlinear system can be made
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Observability}\end{center}
	Consider a linear state-space representation, $\dot{\vec{x}} = \coxmat{A} \vec{x} + \coxmat{B} \vec{u}$, $\vec{y} = \coxmat{C} \vec{x} + \coxmat{D} \vec{u}$ with $n$ states, $m$ inputs, and $p$ outputs. We know $u(t)$ and $y(t)$; can we uniquely determine the initial state $x_0$ $\forall$ $u(t)$ and $x_0$? If yes, system is \textit{observable}.
	
	\begin{minipage}[t]{0.46\textwidth}
		\textit{Observability Matrix}:
		\[ \coxmat{Q_O} = \begin{bmatrix} \coxmat{C} & \coxmat{CA} & \dots & \coxmat{CA}^{n-1}\end{bmatrix}^T \quad (\dim = pn \times n) \]
		Check singular values to determine if $\coxmat{Q_O}$ is close to dropping rank
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item We say the system is \textit{observable} if $\rank(\coxmat{Q_O}) = n$
			\item For single output systems ($p=1$), the system is observable iff $\det(\coxmat{Q_O}) \neq 0$
			\item Individual eigenvalues are observable iff $\begin{bmatrix} \coxmat{A} - \lambda \coxmat{I} & \coxmat{C} \end{bmatrix}^T$ has max rank.
			\item Unobservable Subspace: nullspace of $\coxmat{Q_O}$
		\end{itemize}	
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Controllability}\end{center}
	Consider a linear state-space representation, $\dot{\vec{x}} = \coxmat{A} \vec{x} + \coxmat{B} \vec{u}$ with $n$ states and $m$ inputs. A system is \textit{controllable} if we can drive it from any $\vec{x}_0$ to any $\vec{x}_f$ with the input $\vec{u}(t)$
	
	\begin{minipage}[t]{0.46\textwidth}
		\textit{Controllability Matrix}:
		\[ \coxmat{Q_C} = \begin{bmatrix} \coxmat{B} & \coxmat{AB} & \dots & \coxmat{A}^{n-1} \coxmat{B} \end{bmatrix} \quad (\dim = n \times nm ) \]
		Check singular values to see if $\coxmat{Q_C}$ is close to dropping rank
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item We say the system is \textit{controllable} if $\rank(\coxmat{Q_C}) = n$
			\item For single input systems ($m=1$), the system is controllable if $\det(\coxmat{Q_C}) \neq 0$
			\item Individual eigenvalues are controllable iff $\begin{bmatrix}\coxmat{A} - \lambda \coxmat{I} & \coxmat{B} \end{bmatrix}$ has max rank.
			\item Controllable subspace: column space of $\coxmat{Q_C}$.
		\end{itemize}	
	\end{minipage}
	
	\textit{Controllable Subsystem}
	\begin{enumerate}
		\item Find controllable subspace, $C(\coxmat{Q_C})$
		\item Expand to all of $\mathbb{R}^n$, $n$ = system dimension ($\coxmat{A}$ is $n \times n$)
		\item $\coxmat{T}$ = matrix whose columns are the expanded basis from prev. step
	\end{enumerate}
	\begin{minipage}[t]{0.5\textwidth}
		\begin{alignat*}{5}
			\coxmat{\tilde{A}} &= \coxmat{T}^{-1} \coxmat{AT} & \coxmat{\tilde{B}} &= \coxmat{T}^{-1}\coxmat{B} & \coxmat{\tilde{C}} &= \coxmat{CT}\\
			\coxmat{\tilde{A}} &= \begin{bmatrix} \coxmat{A_{cc}} & \coxmat{A_{cu}}\\ \coxmat{0} & \coxmat{A_{uu}} \end{bmatrix} \quad & \coxmat{\tilde{B}} &= \begin{bmatrix} \coxmat{B_c} \\ \coxmat{0} \end{bmatrix} \qquad & \coxmat{\tilde{C}} &= \begin{bmatrix} \coxmat{C_c} & \coxmat{C_u} \end{bmatrix}
		\end{alignat*}
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{align*}
			\dot{\tilde{x}}_c &= \coxmat{A_{cc}} \tilde{x}_c + \coxmat{A_{cu}} \tilde{x}_u + \coxmat{B_c} \vec{u} \qquad \text{(controllable subsystem)}\\
			\dot{\tilde{x}}_u &= \coxmat{A_{uu}} \tilde{x}_u \qquad \text{(uncontrollable subsystem)}\\
			\vec{y} &= \coxmat{C_c} \tilde{x}_c + \coxmat{C_u} \tilde{x}_u + \coxmat{D} \vec{u}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Observability / Controllability Duality}\end{center}
	\vspace{-1em}
	\begin{minipage}[t]{0.46\textwidth}
		\[ \left( \coxmat{A},\, \coxmat{B} \right) \text{ is controllable} \Longleftrightarrow \, \left( \coxmat{B}^H,\, \coxmat{A}^H \right) \text{ is observable} \]
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\[ \left( \coxmat{C},\, \coxmat{A} \right) \text{ is observable} \Longleftrightarrow \, \left( \coxmat{A}^H,\, \coxmat{C}^H \right) \text{ is controllable} \]
	\end{minipage}
	\vspace{1em}
	\begin{center}$\coxmat{A}^H$ is complex conjugate transpose, i.e., Hermitian operator\end{center}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Nonlinear Systems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Lyapunov Function}\end{center}
	A scalar-valued function of the system state: $V = V(\vec{x})$. 
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Continuous Time}\end{center}
		\vspace{-1em}
		The derivative of $V$ along a solution $\vec{x}(t)$ is given by
		\[ \dod{V}{t}\left( \vec{x}(t) \right) = \left[ \vec{\nabla}V \right]^T \dot{\vec{x}}(t) \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Discrete Time}\end{center}
		\vspace{-1em}
		The derivative of $V$ along a solution $\vec{x}(k)$ is given by
		\[ \Delta V(\vec{x}(k)) = V(\vec{x}(k+1)) -  V(\vec{x}(k)) \]
	\end{minipage}%
\end{contentBox}


\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Conditions on the Lyapunov Function}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Locally Positive Definite (LPD)}\end{center}
		\vspace{-1em}
		\begin{itemize}
			\item A function $V$ is \textit{locally positive definite} about $\vec{x}_e$ if $V(\vec{x}_e) = 0$ and $\exists\, R > 0$ such that $V(\vec{x}) > 0$ whenever $\vec{x} \neq \vec{x}_e$ and $\norm{\vec{x} - \vec{x}_e} < R$
			\item A function is LPD about $\vec{x}_e$ if it is zero at $\vec{x}_e$ and has a strict local minima at $\vec{x}_e$
			\item $V$ is LPD about $\vec{x}_e$ if
			\begin{align*}
				V(\vec{x}_e) &= 0\\
				\vec{\nabla}V(\vec{x}_e) &= \vec{0}\\
				\coxmat{D^2 V}(\vec{x}_e) &> \coxmat{0} \quad \text{(Hessian of }V)
			\end{align*}
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Radially Unbounded (RU)}\end{center}
		\vspace{-1em}
		\begin{itemize}
			\item $V$ is \textit{radially unbounded} if $\underset{\vec{x} \to \infty}{\lim} V(\vec{x}) = \infty$
			\item $V$ is RU if $\coxmat{D^2 V} \geq P > 0$ for $\norm{\vec{x}} \geq R \geq 0$ ($\coxmat{P}$ is symmetric).
		\end{itemize}
		
		\begin{center}\textbf{Lemma}\end{center}
		\vspace{-1em}
		\begin{align*}
			V(0) &= 0\\
			\vec{\nabla}V(0) &= 0 \qquad \qquad \Rightarrow V(\vec{x}) > 0\, \forall\, \vec{x}\\
			\coxmat{D^2V}(\vec{x}) &> 0 \, \forall\, \vec{x}
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Positive Definite (PD)}\end{center}
		\vspace{-1em}
		$V$ is \textit{positive definite} if
		\vspace{-1em}
		\begin{align*}
			V(0) &= 0\\
			V(\vec{x}) &> 0 \quad \forall\, \vec{x} \neq \vec{0}\\
			\underset{\vec{x}\to \infty}{\lim} V(\vec{x}) &= \infty \quad \text{(RU)}
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Lemma}\end{center}
		\vspace{-1em}
		
		If $V(0) = 0$ and $\vec{\nabla}V(0) = \vec{0}$ and $\exists\, \coxmat{P} = \coxmat{P}^T > 0$ such that $\coxmat{D^2V}(\vec{x}) \geq \coxmat{P} \, \forall \, \vec{x}$ then
		\[ V(\vec{x}) \geq \frac{1}{2} \vec{x}^T \coxmat{P} \vec{x} \quad \forall\, \vec{x} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Lyapunov Function Stability Results: Continuous Time}\end{center}
	A nonlinear system has stability properties about equilibrium $\vec{x}_e$ based on its Lyapunov function.
	
	\begin{center}
	\begin{tabular}{|c|c|cc|}
		\hline
		$V$ & $DV = \vec{\nabla}V \dot{x}$ & $\Rightarrow$ & Nonlin. Sys. Stability\\
		\hline \hline
		LPD & $\leq 0$ for $\norm{\vec{x} - \vec{x}_e} < R > 0$ & $\Rightarrow$ & Stable\\
		\hline
		LPD & $< 0$ for $\norm{\vec{x} - \vec{x}_e} \leq R > 0$, $\vec{x} \neq \vec{x}_e$ & $\Rightarrow$ & Asymptotic Stability\\
		\hline
		RU & $\leq 0$ for $\norm{\vec{x} - \vec{x}_e} \geq R \geq 0$ & $\Rightarrow$ & Bounded\\
		\hline
		PD & $< 0$ for $\vec{x} \neq \vec{x}_e$ & $\Rightarrow$ & Global Asymptotic Stability\\
		\hline
		$\beta_1 \norm{\vec{x} - \vec{x}_e}^2 \leq V(\vec{x}) \leq \beta_2 \norm{\vec{x} - \vec{x}_e}^2$ & $\leq -2 \alpha V(\vec{x})$ & \multirow{2}{*}{$\Rightarrow$} & \multirow{2}{*}{Global Exponential Stability}\\
		$\beta_1,\, \beta_2 > 0$ & $\alpha > 0$ & &\\
		\hline
		$\beta_1 \norm{\vec{x} - \vec{x}_e}^2 \leq V(\vec{x}) \leq \beta_2 \norm{\vec{x} - \vec{x}_e}^2$ & $\leq -2 \alpha V(\vec{x})$ & \multirow{2}{*}{$\Rightarrow$} & \multirow{2}{*}{Exponential Stability}\\
		for $\norm{\vec{x} - \vec{x}_e} \leq R$, $\beta_1,\, \beta_2,\, R > 0$ & $\alpha > 0$ & &\\
		\hline
	\end{tabular}
	\end{center}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Lyapunov Function Stability Results: Discrete Time}\end{center}
	A nonlinear system has stability properties about equilibrium $\vec{x}_e$ based on its Lyapunov function.
	
	\begin{center}
	\begin{tabular}{|c|c|cc|}
		\hline
		$V$ & & $\Rightarrow$ & Nonlin. Sys. Stability\\
		\hline \hline
		LPD & $\Delta V \leq 0$ for $\norm{\vec{x} - \vec{x}_e} < R > 0$ & $\Rightarrow$ & Stable\\
		\hline
		LPD & $\Delta V < 0$ for $\norm{\vec{x} - \vec{x}_e} \leq R > 0$, $\vec{x} \neq \vec{x}_e$ & $\Rightarrow$ & Asymptotic Stability\\
		\hline
		RU & $\Delta V \leq 0$ for $\norm{\vec{x} - \vec{x}_e} \geq R \geq 0$ & $\Rightarrow$ & Bounded\\
		\hline
		PD & $\Delta V < 0$ for $\vec{x} \neq \vec{x}_e$ & $\Rightarrow$ & Global Asymptotic Stability\\
		\hline
		$\beta_1 \norm{\vec{x} - \vec{x}_e}^2 \leq V(\vec{x}) \leq \beta_2 \norm{\vec{x} - \vec{x}_e}^2$ & $V(\vec{x}(k+1)) \leq \alpha^2 V(\vec{x})$ & \multirow{2}{*}{$\Rightarrow$} & \multirow{2}{*}{Global Exponential Stability}\\
		$\beta_1,\, \beta_2 > 0$ & $0 \leq \alpha < 1$ & &\\
		\hline
	\end{tabular}
	\end{center}
\end{contentBox}

%\begin{contentBox}
%	\footnotesize%
%	\begin{center}\textbf{Title}\end{center}
%	
%	\begin{minipage}[t]{0.46\textwidth}
%		Stuff
%	\end{minipage}%
%	\hspace{0.03\textwidth}%
%	\begin{minipage}[t]{0.46\textwidth}
%		Stuff
%	\end{minipage}
%	
%\end{contentBox}
