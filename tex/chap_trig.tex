\chapter{Trigonometry}
\section{Identities}
\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Sum-Difference}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin(\theta \pm \psi) &= \sin \theta \cos \psi \pm \cos \theta \sin \psi\\
			\cos(\theta \pm \psi) &= \cos \theta \cos \psi \mp \sin \theta \sin \psi\\
			\tan(\theta \pm \psi) &= \frac{\tan \theta \pm \tan \psi}{1 \mp \tan \theta \tan \psi}
		\end{align*}
		\vfill
	\end{minipage}%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Double Angle}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin(2\theta) &= 2 \sin \theta \cos \theta\\
			\cos(2\theta) &= \cos^2 \theta - \sin^2 \theta\\
			&= 2\cos^2 \theta - 1\\
			&= 1 - 2\sin^2 \theta\\
			\tan(2\theta) &= \frac{2 \tan \theta}{1 - \tan^2 \theta}
		\end{align*}
		\vfill
	\end{minipage}%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Pythagorean}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin^2 \theta + \cos^2 \theta &= 1 \\
			1 + \tan^2 \theta &= \sec^2 \theta \\
			1 + \cot^2 \theta &= \csc^2 \theta
		\end{align*}
	\end{minipage}
	\vfill
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Even-Odd}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin(-\theta) &= -\sin \theta \\
			\cos(-\theta) &= \cos \theta \\
			\tan(-\theta) &= -\tan \theta \\
			\csc(-\theta) &= -\csc \theta \\
			\sec(-\theta) &= \sec \theta \\
			\cot(-\theta) &= -\cot \theta
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Co-Function}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin(\frac{\pi}{2} - \theta) &= \cos \theta \\
			\cos(\frac{\pi}{2} - \theta) &= \sin \theta \\
			\tan(\frac{\pi}{2} - \theta) &= \cot \theta \\
			\csc(\frac{\pi}{2} - \theta) &= \sec \theta \\
			\sec(\frac{\pi}{2} - \theta) &= \csc \theta \\
			\cot(\frac{\pi}{2} - \theta) &= \tan \theta
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Power-Reducing}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin^2 \theta &= \frac{1 - \cos(2 \theta)}{2}\\
			\cos^2 \theta &= \frac{1 + \cos(2 \theta)}{2}\\
			\tan^2 \theta &= \frac{1 - \cos(2 \theta)}{1 + \cos(2 \theta)}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.25\textwidth}
		\begin{center}\textbf{Quotient}\end{center}\vspace{-5mm}
		\begin{align*}
			\tan \theta &= \frac{\sin \theta}{\cos \theta} \\
			\cot \theta &= \frac{\cos \theta}{\sin \theta}
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.25\textwidth}
		\begin{center}\textbf{Reciprocal}\end{center}\vspace{-5mm}
		\begin{align*}
			\csc \theta &= \frac{1}{\sin \theta} \\
			\sec \theta &= \frac{1}{\cos \theta} \\
			\cot \theta &= \frac{1}{\tan \theta}
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Law of Sines and Cosines}\end{center}%\vspace{-5mm}
		\begin{minipage}{0.3\textwidth}
			\begin{tikzpicture}[scale=0.75]
				\draw (0,0) -- (2,2) node[midway, anchor=east]{$a$} -- (3,-1) node[midway, anchor=west]{$b$} -- (0,0) node[midway, anchor=north]{$c$};
				\draw (2.8,-0.8) node[anchor=south east]{$A$};
				\draw (0.1,-0.1) node[anchor=south west]{$B$};
				\draw (1.9,1.8) node[anchor=north]{$C$};
			\end{tikzpicture}
		\end{minipage}%
		\begin{minipage}{0.7\textwidth}
			\begin{align*}
				b^2 &= a^2 + c^2 - 2 a c \cos B\\
				\frac{\sin A}{a} &= \frac{\sin B}{b} = \frac{\sin C}{c}
			\end{align*}
		\end{minipage}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Sum-To-Product}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin \theta + \sin \psi &= 2 \sin \left( \frac{\theta + \psi}{2} \right) \cos \left( \frac{\theta - \psi}{2} \right)\\
			\sin \theta - \sin \psi &= 2 \cos \left( \frac{\theta + \psi}{2} \right) \sin \left( \frac{\theta - \psi}{2} \right)\\
			\cos \theta + \cos \psi &= 2 \cos \left( \frac{\theta + \psi}{2} \right) \cos \left( \frac{\theta - \psi}{2} \right)\\
			\cos \theta - \cos \psi &= -2 \sin \left( \frac{\theta + \psi}{2} \right) \sin \left( \frac{\theta - \psi}{2} \right)
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Product-To-Sum}\end{center}\vspace{-5mm}
		\begin{align*}
			\sin \theta \sin \psi &= \frac{1}{2}[ \cos(\theta - \psi) - \cos(\theta + \psi)]\\
			\cos \theta \cos \psi &= \frac{1}{2}[ \cos(\theta - \psi) + \cos(\theta + \psi)]\\
			\sin \theta \cos \psi &= \frac{1}{2}[ \sin(\theta + \psi) + \sin(\theta - \psi)]\\
			\cos \theta \sin \psi &= \frac{1}{2}[ \sin(\theta + \psi) - \sin(\theta - \psi)]
		\end{align*}
	\end{minipage}
\end{contentBox}

\section{Hyperbolic Trig}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Definitions}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\[ \sinh \theta = \frac{1}{2}\left( e^{\theta} - e^{-\theta} \right) \]
		Odd Function: $\sinh -\theta = -\sinh \theta$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\[ \cosh \theta = \frac{1}{2} \left( e^{\theta} - e^{-\theta} \right) \]
		Even Function: $\cosh -\theta = \cosh \theta$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Identities}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{align*}
			\cosh^2(x) - \sinh^2(x) &= 1\\
			\tanh^2(x) + \sech^2(x) &= 1\\
			\coth^2(x) - \csch^2(x) &= 1
		\end{align*}
	\end{minipage}
\end{contentBox}

\section{Spherical Trig}
See AAE490\_OrbitDesign\_Notes, page H1-9