\chapter{Functional Analysis}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Inner Product}\end{center}
	
	\begin{minipage}[t!]{0.35\textwidth}
		\begin{center}\textit{Definition}\end{center}
		Inner product on a linear space $\mathcal{K}$ denoted by $\langle\cdot\,, \cdot\rangle$ is a complex valued function mapping $\mathcal{K}\times\mathcal{K}$ into the set of all complex numbers $\mathbb{C}$
		\begin{center}\textit{Standard Inner Product}\end{center}
		If $f$ and $g$ are continuous functions on [$a$, $b$], the standard inner product is
		\[ \langle f, g\rangle = \int\limits_a^b f(x) g(x) \dif x \]	
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.55\textwidth}
		\begin{center}\textit{Properties}\end{center}
		\vspace{-1em}
		$f$, $g$, $h$, vectors in $\mathcal{K}$; $\alpha$, $\beta$ complex scalars:
		\begin{enumerate}
			\item $\langle f,h \rangle = \overline{\langle h,f \rangle}$
			\item $\langle \alpha f + \beta g, h \rangle = \alpha \langle f, h \rangle + \beta \langle g, h \rangle$
			\item $\langle f,f \rangle \geq 0\quad \forall\,\, f \in \mathcal{K}$
			\item $\langle f,f \rangle = 0 \Leftrightarrow f = 0$
		\end{enumerate}
		``Vector'' is a general term: may represent function.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textbf{Inner Product Space}\end{center}\vspace{-3mm}
		A linear space, $\mathcal{K}$, (closed under linear combinations) with an inner product that stays within the space
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textbf{Norm of $h\in \mathcal{K}$}\end{center}\vspace{-5mm}
		\begin{align*}
			\norm{h} &= + \sqrt{\langle h,h \rangle}\\
			\norm{\alpha h} &= |\alpha|\norm{h}\\
			\norm{h} &= 0 \Leftrightarrow h = 0\\
			\norm{f + h}^2 &= \norm{f}^2 + 2\text{Re}\langle f,h \rangle + \norm{h}^2
		\end{align*}
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textbf{Cauchy-Schwartz Inequality}\end{center}
		(holds for any inner product space)
		\begin{align*}
			|\langle f,h \rangle| &\leq \norm{f}\norm{h} \quad (f,h \in \mathcal{K})\\
			|\langle f,h \rangle| &= \norm{f}\norm{h} \Leftrightarrow f,h \text{ lin. dep.}
		\end{align*}
	\end{minipage}
	
	\begin{itemize}
		\item $\{h_i\}_0^{\infty}$ is a \textit{Cauchy sequence} in $\mathcal{K}$ if $\norm{h_i -  h_j} \to 0$ as $i,j \to \infty$
		\item $\mathcal{K}$ is \textit{complete} if every Cauchy sequence converges to some $h\in\mathcal{K}$
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Hilbert Space}\end{center}
	A complete, inner product space. We only consider \textit{separable} (space is closure of a countable set) Hilbert spaces. if $\mathcal{H}$ is a separable Hilbert space, then:
	\begin{itemize}
		\item $\exists$ an orthonormal basis $\{v_j\}_1^m$ for $\mathcal{H}$ where $m = \dim[\mathcal{H}]$
		\item Every $h \in \mathcal{H}$ admits a Fourier series expansion of the form $h = \sum\limits_{j=1}^m \langle h, v_j \rangle v_j$
		\item Parseval's relation: $(h,g) = \sum\limits_{j=1}^m \langle h,v_j \rangle \overline{\langle g, v_j \rangle}\quad (h,g \in \mathcal{H})$
	\end{itemize}
	
	Examples of Hilbert Spaces:
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{$\mathbb{C}^n$ Space}\end{center} \vspace{-3mm}
		Set of all complex $n$ tuples of the form $[x_1, x_2,\dots,x_n]^T$ where $x \in \mathbb{C}\,\,\forall\,\,j = 1,2,\dots,n$
		\begin{itemize}
			\item Inner product: $\langle x,y \rangle = \sum\limits_{j=1}^n x_j \bar{y}_j$
			\item Cauchy-Schwartz:\newline $\left| \sum\limits_{j=1}^n x_j \bar{y}_j \right| \leq \left(\sum\limits_{j=1}^n |x_j|^2 \right)^{1/2} \left( \sum\limits_{j=1}^n |y_j|^2 \right)^{1/2}$
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{$L^2[a,b]$ Space}\end{center} \vspace{-3mm}
		Set of all square integrable Lebesgue measurable functions over $[a,b]$: \[f \in L^2[a,b] \quad \Leftrightarrow \quad \int\limits_a^b |f(t)|^2 \dif t < \infty\] and $f$ is Lebesgue measurable on $[a,b]$
		\begin{itemize}
			\item Inner product: $\langle f,g \rangle = \int\limits_a^b f(t) \overline{g(t)} \dif t$
			\item Cauchy-Schwartz:\newline $\left| \int\limits_a^b f(t) \overline{g(t)} \dif t \right| \leq \left( \int\limits_a^b |f(t)|^2 \dif t \right)^{1/2} \left( \int\limits_a^b |g(t)|^2 \dif t \right)^{1/2}$
		\end{itemize}
	\end{minipage}
	
	\vspace{2mm}
	\begin{minipage}[t]{0.95\textwidth}
		\begin{center}\textbf{Finite-Variance R.V. Space}\end{center}\vspace{-3mm}
		Widely used in filtering theory; set of all finite-variance R.V. $X$ such that $E|X|^2 < \infty$. Variance is $\sigma_x^2 = E|X|^2 - |E[X]|^2$, which implies that the mean $E[X]$ must also be finite if $X\in\mathcal{H}$
		\begin{itemize}
			\item Inner product: $\langle X,Y \rangle = E[X\bar{Y}]$
			\item Norm: $\norm{X} = \sqrt{E|X|^2}$
			\item Cauchy-Schwartz: $\left|E[X\bar{Y}]\right| \leq \sqrt{E|X|^2} \sqrt{E|Y|^2}$, which implies that $E|X| \leq \sqrt{E|X|^2}$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Projection Theorem}\end{center}
	Let $\mathcal{K}$ be a Hilbert space, and $\mathcal{H}$ be a subspace of $\mathcal{K}$ (implying $\mathcal{H}$ is a closed linear space contained in $\mathcal{K}$; $\mathcal{H}$ could be $\{0\}$ or the whole space $\mathcal{K}$).
	\begin{itemize}
		\item Two vectors $f, g \in \mathcal{K}$ are orthogonal ($f \perp g$) if $\langle f,g \rangle = 0$.
		\item $f$ is orthogonal to $\mathcal{H}$ ($f \perp \mathcal{H}$) if $\langle f,g \rangle = 0\,\,\forall \,\, g \in \mathcal{H}$
		\item Orthogonal complement of $\mathcal{H}$ is the subspace of $\mathcal{K}$ defined by $\mathcal{H}^{\perp} = \{ f \in \mathcal{K} : f \perp \mathcal{H}\}$
	\end{itemize}
	
	\begin{minipage}[t]{0.5\textwidth}
		Let $f$ be a vector in $\mathcal{K}$; basic least squares problem is to find an element $\hat{f} \in \mathcal{H}$ which is closer to $f$ than any other element of $\mathcal{H}$; leads to optimization problem: \[ d(f,\mathcal{H}) \norm{f - \hat{f}} = \inf \{ \norm{f - h} : h \in \mathcal{H} \} \]
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
	\vspace{-5mm}
		\[ \left\{ \begin{array}{l}
			d(f, \mathcal{H}) \text{is the distance between } f \text{ and } \mathcal{H}\\
			\inf\{x : x \in (0,1]\} = 0 \neq \text{ minimum because 0 is not}\\
			\quad \text{ in set (closing the set ensures inf =  min)}\\
			\inf\{x : x \in [0,1)\} = 0 = \text{ minimum}
			\end{array}\right. \]
		Supremum is maximum analogue
	\end{minipage}

	\begin{minipage}[t!]{0.15\textwidth}
		\begin{tikzpicture}
			% Plane and axes
			\filldraw[blue, opacity=0.25] (0,0) -- (1.9,0) -- (1.53,-0.5) -- (-0.37,-0.5) -- cycle;
			\draw[blue] (-0.37, -0.5) node[anchor=south west, xshift=2mm]{$\mathcal{H}$};
			\draw[->] (0,0) -- (0,1.5);
			\draw[->] (0,0) -- (-0.45,-0.6);
			\draw[->] (0,0) -- (2,0);
			
			%Vectors and projection
			\draw[->, thick] (0,0) -- (1.3,0.9)node[midway, anchor=south]{$f$};
			\draw[->] (1.3,-0.2) -- (1.3,0.85)node[midway, anchor=west]{$\tilde{f}$};
			\draw[->] (0,0) -- (1.3, -0.2)node[midway, anchor=north]{$\hat{f}$};
		\end{tikzpicture}
	\end{minipage}%
	\begin{minipage}[t!]{0.8\textwidth}
		\textbf{Theorem:}\newline \textit{
			For every $f \in \mathcal{K}$, $\exists$ a unique vector $\hat{f} \in \mathcal{H}$ solving the optimization problem above. Moreover, $\hat{f}$ is the only vector in $\mathcal{H}$ such that $f - \hat{f}$ is $\perp \mathcal{H}$. Finally, if $\tilde{f} = f-\hat{f}$, then the distance (or optimal error) is given by \[ d(f,\mathcal{H})^2 = \norm{\tilde{f}}^2 = \norm{f - \hat{f}}^2 = \norm{f}^2 - \norm{\hat{f}}^2 \]
		}
		\begin{itemize}
			\item $\hat{f}$ is the \textit{orthogonal projection} onto $\mathcal{H}$: $\hat{f} = P_{\mathcal{H}} f$ (range of $P_{\mathcal{H}} = \mathcal{H}$).
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.475\textwidth}
	\begin{center}\textbf{Positive Operators}\end{center}
		Let $T$ be an operator on a finite dimensional Hilbert space $\mathcal{X}$. $T$ is positive ($T \geq 0$) iff:
		\begin{itemize}
			\item $(Tx,x) \geq 0\,\,\forall\,\, x \in \mathcal{X}$
			\item $T = T^*$ ($*$ denotes complex-conjugate transpose) and all eigenvalues of $T$ are positive ($\geq 0$)
		\end{itemize}
		$T$ is strictly positive ($T > 0$) iff:
		\begin{itemize}
			\item $T = T^*$ and all eigenvalues of $T$ are $> 0$
			\item $T$ is positive and invertible
		\end{itemize}
		If $\mathcal{X} = \mathbb{C}^n$ then:
		\begin{itemize}
			\item $T \geq 0$ $\Leftrightarrow$ $x^* T x \geq 0$ $\forall\,\, x \in \mathcal{X}$
			\item $T > 0$ $\Leftrightarrow$ $x^* T x > 0$ $\forall\,\, x \in \mathcal{X}$
		\end{itemize}
		Also extends to infinite-dimensional Hilbert spaces
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Gram Matrices}\end{center}
		Let $\{f_i\}_1^n$ be a set of vectors in a Hilbert space $\mathcal{K}$. Then the Gram matrix associated with $\{f_i\}_1^n$ is
		\[ G = \begin{bmatrix}
			(f_1, f_1) & \dots & (f_1, f_n)\\
			\vdots & \ddots & \vdots\\
			(f_n, f_1) & \dots & (f_n, f_n)
		\end{bmatrix}\]
		\begin{itemize}
			\item $G$ is self-adjoint: $G = G^*$
			\item $G > 0$ $\Leftrightarrow$ $\{f_i\}_1^n$ are linearly independent
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Applying Projection Theorem with Gram Matrices}\end{center}
	\begin{minipage}[t]{0.45\textwidth}
		Let $\{f_i\}_1^n$ be a set of linearly-independent vectors in a Hilbert space $\mathcal{K}$. Let $\mathcal{H}$ be the space spanned by these vectors, and $G$ be the Gram matrix generated by $\{f_i\}_1^n$. Let $f$ be a vector in $\mathcal{K}$. Our optimization problem is
		\[ \norm{\tilde{f}} = \norm{f - \hat{f}} = \inf \left\{ \norm{f - \sum\limits_{i=1}^n \alpha_i f_i} : \alpha_i \in \mathbb{C} \right\} \]
		Then the orthogonal projection $\hat{f}$ of $f$ onto $\mathcal{H}$ is given by
		\[ \hat{f} = P_{\mathcal{H}} f = \sum\limits_{i = 1}^n \alpha_i f_i \]
		where the scalars $\{\alpha_i\}_1^n$ are computed by
		\[ \begin{bmatrix} \alpha_1 & \dots & \alpha_n \end{bmatrix} = 
		\begin{bmatrix} (f, f_1) & \dots & (f, f_n) \end{bmatrix} G^{-1} \]
		The norm of $\hat{f}$ is computed by 
		\begin{align*}
			\norm{\hat{f}}^2 &= \begin{bmatrix} (f, f_1) & \dots & (f, f_n) \end{bmatrix} G^{-1} \begin{bmatrix} (f, f_1) & \dots & (f, f_n) \end{bmatrix}^*\\
			&= \begin{bmatrix} \alpha_1 & \dots & \alpha_n \end{bmatrix} G \begin{bmatrix} \alpha_1 & \dots & \alpha_n \end{bmatrix}^*
		\end{align*}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Let $\mathcal{H} = \text{span}\{g\}$; then $P_{\mathcal{H}} x = \hat{x} = R_{xg}R_g^{-1} g$ where
		\[ R_g = \langle g, g^* \rangle,\quad R_{xg} = \langle x, g^* \rangle\]
		Estimation Error:
		\[ \norm{x - \hat{x}} = \norm{\tilde{x}} = R_x - R_{xg}R_g^{-1}R_{xg}^*\]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Gram-Schmidt via Projection Theorem}\end{center}
	\begin{minipage}[t]{0.50\textwidth}
	Let $\mathcal{M}_{\nu} = \text{span}\{y(k)\}^n_{k=0}$\\
	where $\mathcal{M}_{\nu} = \bigoplus\limits^{\nu}_{k=0} \mathcal{E}_k$, i.e. $\mathcal{E}_k = \mathcal{M}_k \ominus \mathcal{M}_{k-1}$.\\
	Then $\phi(k) = y(k) - P_{\mathcal{M}_{k-1}}y(k) = y(k) - \sum\limits^{k-1}_{j=0}P_{\mathcal{E}_j} y(k)$\\
	Example: $P_{\mathcal{M}_\nu}x = \sum\limits\limits_{k=0}^{\nu}P_{\mathcal{E}_k} x = \sum\limits_{k=0}^{\nu} R_{x\phi_k}R_{\phi_k}^{-1}\phi_k$
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		Notation:
		\begin{itemize}
			\item $F \oplus G = \text{span}\{F, G\}$, $F$ and $G$ orthogonal
			\item $F \oplus G = G \oplus F$
			\item $F \ominus G = \text{span}\{f \in F : f \perp G \}$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\section{General Orthogonal Decomposition: Eigenfunctions}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Sturm-Liouville Equation}\end{center}
	\[ \dod{}{x} \left( p\dod{y}{x} \right) + \left(q + \lambda r \right) y = 0 \]
	on some interval [$a$, $b$] satisfying boundary conditions
	\begin{align*}
		\textbf{(a)} \quad &k_1 y(a) + k_2 y'(a) = 0\\
		\textbf{(b)} \quad & l_1 y(b) + l)2 y'(b) = 0
	\end{align*}
	where $p(x)$, $q(x)$, $r(x)$, and $p'(x)$ are continuous on [$a$, $b$], and the weighting function, $r(x)$ is positive.
	\begin{itemize}
		\item \textit{Eigenfunction}: Solution of Sturm-Liouville ODEs that are not identically zero
		\item \textit{Eigenvalue}: Value of $\lambda$ for which solution is an eigenfunction
	\end{itemize}
	
	\begin{minipage}[t]{0.45\textwidth}
		Functions $y_n(x)$, $y_m(x), \dots$ defined on [$a$, $b$] are orthogonal w.r.t. weight function, $r(x) > 0$, iff inner product is zero:
		\[ \int\limits_a^b r(x) y_m(x) y_n(x) \dif x = 0 \quad \forall\,\, m,n \neq m \]
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Norm, $\norm{y_m}$, defined:
		\[ \norm{y_m} = \left[ \int\limits_a^b r(x) y^2_m(x) \dif x \right]^{1/2} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Reverse Engineering Sturm-Liouville Equation}\end{center}

	\begin{minipage}[t]{0.45\textwidth}
		\[ y'' + fy' + (g + \lambda h)y = 0\]
	\end{minipage}%
	\begin{minipage}[t]{0.45\textwidth}
		Then $p = e^{\int f \dif x}$, $q = pg$, $r = hp$.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Orthogonoality of Eigenfunctions of Sturm-Liouville Problems}\end{center}
	
	If $p(x)$, $q(x)$, $r(x)$, $p'(x)$ are real-valued \& continuous on [$a$, $b$] and $r(x) > 0$, then eigenfunctions $y_m(x)$, $y_n(x)$ of Sturm-Liouvill Eq corresponding to different eigenvalues $\lambda_m$, $\lambda_m$ are orthogonal w.r.t. their weight function.
	
	If $p(a) = 0$, then B.C. $(a)$ can be dropped; if $p(b) = 0$, then B.C. $(b)$ can be dropped. It is then required that $y$ and $y'$ remain bounded at such a point, and the problem is called \textit{singular} as opposed to a regular problem in which both B.C.s are used.
	
	If $p(a) = p(b)$, then B.C.s are replaced by the \textit{periodic boundary conditions}, $y(a) = y(b)$, $y'(a) = y'(b)$.
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Legendre's Equation}\end{center}
	\[ (1 - x^2) \dod[2]{y}{x} - 2x \dod{y}{x} + n(n + 1)y,\qquad -1 \leq x \leq 1 \]
	
	Special case of Sturm-Liouville Equation with $p(x) = 1 - x^2$, $q(x) = 0$, $\lambda = n(n+1)$, and $r(x) = 1$. Solutions are Legendre polynomials:
	\vspace{-1em}
	\begin{minipage}[t]{0.2\textwidth}
		\begin{align*}
			P_0(x) &= 1\\
			P_1(x) &= x
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{align*}
			P_2(x) &= \frac{1}{2}(3x^2 - 1)\\
			P_3(x) &= \frac{1}{2}(5x^3 - 3x)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.4\textwidth}
		\begin{align*}
			P_4(x) &= \frac{1}{8}(35x^4 - 30x^2 + 3)\\
			P_5(x) &= \frac{1}{8}(63 x^5 - 70x^3 + 15x)
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	
	\begin{minipage}[t]{0.45\textwidth}
		In general
	\[ P_n(x) = \sum\limits_{m=0}^M (-1)^m \frac{(2n - 2m)!}{2^n m! (n-m)! (n-2m)!} x^{n-2m} \]
	\end{minipage}%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Legendre-Fourier Series}\end{center}
		\vspace{-1em}
		Project functions into basis of eigenfunctions via inner product to determine coefficients:
		\[ f(x) = \sum\limits_{m=0}^{\infty} a_m f_m(x),\qquad a_m = \frac{2m+1}{2}\int\limits_{-1}^1 f(x) P_m(x) \dif x \]
	\end{minipage}%
\end{contentBox}

\input{tex/calc/Fourier.tex}			% sub-section