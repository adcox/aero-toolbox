\chapter{Time Domain Analysis}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{1st Order System}\end{center}
	
	\begin{minipage}[t]{0.3\textwidth}
		\[ G(s) = \frac{1}{Ts + 1},\quad T > 0 \]
	\end{minipage}
	\begin{minipage}[t]{0.65\textwidth}
		\begin{itemize}
			\item \textit{Rise Time} $t_r$: time for response to go from $0.1 \to 0.9$, $t_r = 2.2T$
			\item \textit{Settling Time} $t_s$: time for response to reach and stay within 2\% of final value, $t_s = 4T$
		\end{itemize}			
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{2nd Order System}\end{center}
	
	\begin{minipage}[t]{0.3\textwidth}
		\begin{align*}
			m \ddot{y}& + c \dot{y} + k y = f(t)\\
			\ddot{y}& + 2 \zeta \omega_n \dot{y} + \omega_n^2 y &= \frac{f(t)}{m}\\
			G(s)& = \frac{\omega_n^2}{s^2 + 2 \zeta \omega_n s + \omega_n^2}
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.65\textwidth}
		For underdamped case:
		\begin{itemize}
			\item \textit{Delay Time}: Time to reach 1/2 the final value.
			\item \textit{Rise Time}: Time to go from $0 \to 1$
				\[ t_r = \frac{1}{\omega_d} \arctan\left( \frac{-\omega_d}{\sigma} \right) = \frac{\pi - \theta}{\omega_d}, \quad \omega_d = \omega_n \sqrt{1 - \zeta^2}, \quad \sigma = \zeta \omega_n \]
			\item \textit{Peak Time}: Time to reach max, $t_p = \pi/\omega_d$
			\item \textit{Maximum Percent Overshoot}: $M_p = \exp \left( -\pi \sigma/\omega_d \right)$
			\item \textit{Settling Time}: Time to reach max and stay within 2\% ($t_s = 4/\sigma$) or within 5\% ($t_s = 3/\sigma$)
		\end{itemize}
	\end{minipage}
	
	Can choose $\zeta$ to achieve desired $M_p$:
	\[ \zeta = \frac{-\ln(M_p)}{\sqrt{\pi^2 + \ln(M_p)}} \]
\end{contentBox}


\chapter{Frequency Domain Analysis}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Frequency Domain}\end{center}
	It is frequently (pun intended) useful to transform a system from the usual time domain to the frequency domain for analysis. This transformation is facilitate via the Laplace and/or Fourier transformations.
\end{contentBox}

\section{Laplace Analysis}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.66\textwidth}
		\begin{center}\textbf{Final Value Theorem}\end{center}
		If $\underset{t \ to \infty}{\lim} f(t)$ is finite and $f(t)$ is bounded, then
		\[ \underset{t \ to \infty}{\lim} f(t) = \underset{s \ to 0}{\lim} F(s)\]
		where $F(s) = \Lapl \left\{ f(t) \right\}$ and $sF(s)$ has all poles in the left-hand plane (none on the imaginary axis).
		\begin{itemize}
			\item If at least one pole has zero real part, $f(t)$ is unbounded or periodic
			\item If all $\real[\text{poles}] = 0$, $f(t)$ is stable (oscillatory) but not asymptotically stable and has no steady state value 
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.26\textwidth}
		\begin{center}\textbf{Initial Value Theorem}\end{center}
		\[ \underset{t \to 0}{\lim} f(t) = \underset{s \to \infty}{\lim} sF(s) \]
		No dependency on system stability or poles.
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Block Diagrams}\end{center}
	
	Complicated systems are constructed from these basic block elements. To write equations for more complicated systems, write equations for smaller segments and then substitute and combine to arrive at a final equation.
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Elementary Block}\end{center}
		\vspace{-1em}
		\includegraphics[width=0.8\textwidth]{block_elementary.pdf}
		\[ Y(s) = U(s)G(s) \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Summing Junction}\end{center}
		\includegraphics[width=0.8\textwidth]{block_sum.pdf}
		\[ Z(s) = X(s) + Y(s) \]
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Branch Point}\end{center}
		\vspace{-1em}
		\includegraphics[width=0.8\textwidth]{block_branch.pdf}
		\[ Y(s) = X(s) \qquad Z(s) = X(s) \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Series Connection}\end{center}
		\vspace{-1em}
		\includegraphics[width=0.8\textwidth]{block_series.pdf}
		\[ \frac{Y(s)}{U(s)} = G_1(s) G_2(s) \]
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Parallel Connection}\end{center}
		\includegraphics[width=0.8\textwidth]{block_parallel.pdf}
		\begin{align*}
			Y(s) &= U(s)G_1(s) + U(s)G_2(s)\\
			\frac{Y(s)}{U(s)} &= G_1(s) + G_2(s)
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Feedback Connection}\end{center}
		\includegraphics[width=0.8\textwidth]{block_feedback.pdf}
		\begin{align*}
			e &= U - a \quad \text{(negative feedback)}\\
			a &= G_2 Y\\
			Y &= G_1 e\\
			\frac{Y(s)}{U(s)} &= \frac{G_1(s)}{1 + G_1(s) G_2(s)}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Steady State Error}\end{center}
	
	\begin{minipage}[t!]{0.35\textwidth}
		\includegraphics[width=1.0\textwidth]{block_simpleFeedback.pdf}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t!]{0.55\textwidth}
		\begin{align*}
			\text{steady state error} = e_{ss} &= \underset{t \to \infty}{\lim} e(t) = \underset{t \to \infty}{\lim} \left[ y(t) - u(t) \right]\\
			\text{via final value thm.} \quad e_{ss} &= \underset{s \to 0}{\lim} \left( \frac{s U(s)}{1 + G(s)} \right)
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t!]{0.22\textwidth}
		\[ G(s) = \frac{K \prod\limits_{i=1}^m (s + z_i)}{s^N \prod\limits_{j=1}^n(s + p_j)} \]
		$m$ zeros, $n + N$ poles; system is ``Type $N$''
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t!]{0.7\textwidth}
		\begin{tabular}{l l l l}
			For unit-step input: & $e_{ss} = \frac{1}{1 + \bar{k}_p}$ & $\bar{k}_p \equiv \underset{s \to 0}{\lim} G(s)$ & If type $\geq$ 1, $e_{ss} = 0$\\
			For unit-ramp input: & $e_{ss} = \frac{1}{\bar{k}_v}$ & $\bar{k}_p \equiv \underset{s \to 0}{\lim} sG(s)$ & If type $\geq$ 2, $e_{ss} = 0$\\
			For parabolic ramp input: & $e_{ss} = \frac{1}{\bar{k}_a}$ & $\bar{k}_a \equiv \underset{s \to 0}{\lim} s^2G(s)$ & If type $\geq$ 3, $e_{ss} = 0$
		\end{tabular}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Root Locus Diagrams}\end{center}
	
	\begin{minipage}[!t]{0.35\textwidth}
		\includegraphics[width=1.0\textwidth]{block_rootLocus.pdf}
	\end{minipage}%
	\begin{minipage}[!t]{0.6\textwidth}
		Let $K(s)G(s) = k L(s)$ Then the TF is \[ \frac{Y(s)}{U(s)} = \frac{k L(s)}{1 + k L(s)} \] The \textit{Root Locus} is a plot of the set 
		\[ \left\{ \eval[2]{s} 1 + kL(s) = 0, \,\, 0 \leq k \leq \infty \right\} \]
	\end{minipage}

	If $s_0$ is on root locus, then:
	\begin{itemize}
		\item angle($L(s_0)) = \pm 180^{\circ}$. If positive feedback, angle is $0^{\circ}$
		\item $|L(s_0)| = 1$
	\end{itemize}
	
	Rules for drawing Root Loci:
	\begin{enumerate}
		\item Find poles and zeros of $L(s)$ (open-loop poles and zeros): $n = $ \# poles, $m=$ \# zeros.
		\begin{itemize}
			\item Root locus always starts from open-loop poles
			\item Root locus always ends at open-loop zeros or $\infty$
		\end{itemize}
		\item Symmetry over real axis
		\item Include portions of real axis that are left$^*$ of odd numbered real poles/zeros ($^*$If numerator has (-)$s$, angle condition changes to $0^{\circ}$ and we use portions of real axis to the \textit{right} of real poles/zeros).
		\item Asymptotes: Centroid at $\sigma_a = (\sum\text{poles} - \sum\text{zeros})/(n-m)$, angle of asymptotes $\theta_a = (180 + 360 l)/(n-m)$, $l = 0,1,\dots,n-m-1$. For positive feedback, $\theta_a = \pm l 360 / (n-m)$
		\item Break-in/away points: $\left\{ \eval[2]{s} -\od{}{s} \left( 1/L(s) \right) = 0 \right\}$
		\item Angle of Departure from Complex Poles: choose $s$ near complex pole, use angle condition to find angle form complex pole to $s$.
		\item Intersection w/ Imaginary Axis: Let $1 + k_0 L(s_0) = 0$; sub $s_0 = \omega_0 j$ $\to$ $1 + k_0 L(\omega_0 j) = 0$. Solve real part = 0 and imaginary part = 0 for $k_0$ and $\omega_0$. The intersection is at $\omega_0$.
	\end{enumerate}
	
	\textit{See AAE364 notes for more details and examples}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Nyquist Plot}\end{center}
	Plot of $G(j\omega)$ over $\omega = [0,\, \infty]$ in complex plane. Starting and ending points are located by evaluating $\underset{\omega \to 0}{\lim} G(j\omega)$ and $\underset{\omega \to \infty}{\lim} G(j\omega)$, respectively. The resulting complex points are the start/end points.\\
	
	\textit{See AAE364 notes for more details and examples}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Nyquist Stability Criterion}\end{center}
	\[ Z = N(-1,\, G(s),\, D) + P \]
	where $P$ = \# unstable open-loop poles, $N(-1,\, G(s),\, D)$ = \# clockwise encirclements of $G(s)$ around -1 as $s$ moves along Nyquist contour $D_1$ ($D_2$ if a pole exists at the origin). Then $Z$ is the \# of unstable closed-loop poles.
	
	\begin{center}\textit{If $z \neq 0$, system is unstable}\end{center}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Summary of Compensators}\end{center}
	
	See AAE364\_Summary
\end{contentBox}

\section{Fourier Analysis}
