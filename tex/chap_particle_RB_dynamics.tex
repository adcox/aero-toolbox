% General Dynamics principals
% Material drawn from:
% - AAE 340
% - AAE 507
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Kinematics and Dynamics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Notation and Definitions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.48\textwidth}
		\begin{center}\textbf{Vectors}\end{center}
		\[\vec{r}^{OP}_s \to {^i\vec{v}^{OP}}_s \to {^i\vec{A}^{OP}}_s\]
		\begin{itemize}
			\item $i$ (left superscript): The observer frame, or frame we will take derivatives in
			\item $s$ (right subscript): the working frame; i.e. the vector is written in terms of $\hat{s}_i$ unit vectors
			\item $\vec{A}$ the vector quantity; position $\vec{r}$ or $\vec{\rho}$, velocity $\vec{v}$, acceleration $\vec{A}$
			\item $OP$: the base-point $O$ and terminal point $P$
		\end{itemize}
		An acceleration or velocity vector is \textbf{generic} if and only if $O$ is fixed in frame $i$; We leave base-points off of generic vectors because the quantity will be the same regardless of base-point: ${^i\vec{A}^P}$
	\end{minipage}
	\hspace{3mm}
	\begin{minipage}[t]{0.48\textwidth}
		\begin{center}\textbf{Basic Kinematic Equation}\end{center}
		A tool to compute derivatives of a vector written in terms of a working frame that rotates relative to the observer frame
		\[ \frac{^a \dif \vec{Q}_b}{\dif t} = \frac{^b \dif \vec{Q}_b}{\dif t} + {^a\vec{\omega}^b_b} \times \vec{Q}_b \]
		where
		\begin{itemize}
			\item Observer Frame: $a$
			\item Working Frame: $b$
			\item Angular velocity of $b$ relative to $a$: ${^a\vec{\omega}^b}_b$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Generalized Coordinates - Change of Variables}\end{center}
	Any set of parameters which can characterize the state of a system (i.e., any set of useful coordinates: Cartesian, polar, spherical, etc. Just pick a set that works!)
	
	\begin{minipage}[t]{0.45\textwidth}
		Transformations from one coord set to another: must be finite, single-valued, continuous, differentiable, nonzero Jacobian. Evaluate $J = 0$ to locate singular points, e.g. $J = r^2 \sin \theta$, singular points at $r = 0$, $\theta = 0, \pi, 2\pi, \dots$.
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		$x_i$: arbitrary coordinate set, $q_i$: generalized coordinate set
		\[ J = \dpd{(x_1, x_2)}{(q_1, q_2)} = \begin{vmatrix}
			\dpd{x_1}{q_1} & \dpd{x_1}{q_2}\\[3mm] \dpd{x_2}{q_1} & \dpd{x_2}{q_2}
		\end{vmatrix}
		\neq 0
		\]
		$J$ is the \textit{Jacobian} of the transformation.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Equations of Constraint (EOCs)}\end{center}
	
	\begin{minipage}[t]{0.47\textwidth}
		\textbf{(1) Holonomic Form}
		\[ \phi_j = \phi_j \left( q_1,\, q_2,\, \dots,\, q_n,\, t \right) = 0 \]
		$n$ = \# gen. coords; $m$ = \# EOCs; $j$ = 1, $\dots$, $m$;
	\end{minipage}%
	\begin{minipage}[t]{0.47\textwidth}
		\textbf{(2) Differential Form}
		\begin{align*}
			\dif \phi_j &= \dpd{\phi_j}{q_1} \dif q_1 + \dots + \dpd{\phi_j}{q_n} \dif q_n + \dpd{\phi_j}{t} \dif t = 0\\
			&= \sum\limits_{i=1}^n \left( \dpd{\phi_j}{q_i} \dif q_i \right) + \dpd{\phi_j}{t} \dif t = \sum\limits_{i=1}^n	\left( a_{ji} \dif q_i \right) + a_{jt} \dif t = 0
		\end{align*}
	\end{minipage}
	
	A constraint is
	
	\begin{minipage}[t]{0.47\textwidth}
		\textit{Holonomic} if:
		\begin{itemize}
			\item (2) is integrable, or \textit{exact} (meets exactness conditions; sufficient, not necessary; May multiply (2) by integrating factor that is fcn of $q_i$ and $t$ to integrate).
			\item Path independent
		\end{itemize}
		\textit{Bilateral} if: equality constraint\\
		\textit{Unilateral} if: inequality constraint\\
		Exactness Conditions:
		\[ \dpd{a_{ij}}{q_k} = \dpd{a_{jk}}{q_i} \,\, \forall\,\, i, k, \qquad \dpd{q_{ji}}{t} = \dpd{a_{jt}}{q_i}\,\, \forall \,\, i \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.47\textwidth}
		\textit{Nonholonomic} if:
		\begin{itemize}
			\item (2) is \underline{not} integrable to get (1)
			\item Path dependent 
		\end{itemize}
		\textit{Rheonomic} if:
		\begin{itemize}
			\item EOCs are time-dependent
		\end{itemize}
		\textit{Schleronomic} if:
		\begin{itemize}
			\item EOCs are not time-dependent
			\item Transformations between coordinates are not time-dependent
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Degrees of Freedom (DOFs)}\end{center}
	Difference between number of generalized coordinates and number of equations of constraint.
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Point Mass}\end{center}
		Maximum: 3 translation DOF
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Rigid Body}\end{center}
		Maximum: 3 translation DOF + 3 rotational DOF
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Center of Mass}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\[ \vec{R}^{QC} = \frac{1}{\mathcal{M}} \sum\limits_{i=1}^n \vec{R}^{Q P_i} m_i \]
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\[ \vec{R}^{QC} = \int\limits_V \rho(\vec{r}) \vec{r} \dif v \]
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equations Of Motion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Laws of Motion}\end{center}
	\begin{minipage}{0.475\textwidth}
		\begin{center}\textbf{Newton}\end{center}
		\[\vec{F} = \mathcal{M} {^i\vec{A}^{C}}\]
		\begin{itemize}
			\item $\mathcal{M}$: total system mass
			\item $C$ represents center of mass
			\item External forces only (no internal friction, sloshing, etc.)
			\item Applies to single particles, systems of particles, and rigid bodies
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.475\textwidth}
		\begin{center}\textbf{Euler}\end{center}
		\[ \vec{M}^Q = \frac{^i \dif {^i\vec{H}^Q}}{\dif t} + \mathcal{M} \vec{\rho}^{\,QC} \times {^i\vec{A}^Q} \]
		\begin{itemize}
			\item Moments due to external forces only
			\item Applies to systems of particles about point $Q$, which may move
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Lagrange's Equation}\end{center}
	Yield EOMs from energy equations, equivalent to Newton and Euler Laws of Motion but require fewer (or no) force diagrams and fewer kinematic derivatives (only velocity, not acceleration). Let $T$ be kinetic energy, $V$ be potential energy $Q$ be a generalized force.
	\vspace{1em}
		
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textbf{Fundamental Form}\end{center}
		\[ \dod{}{t} \left( \dpd{T}{\dot{q}_i} \right) - \dpd{T}{q_i} = Q_i \quad i=1,\,\dots\,n\]
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.6\textwidth}
		\begin{center}\textbf{Holonomic Standard Form}\end{center}
		Assumes: all applied forces derivable from potential function $Q_i = -\dpd{V_i}{q_i}$, $V_i = V(q_1,\,\dots,\,q_n,\,t)$\\
		Lagrangian $\Lagr = \Lagr(q, \dot{q}, t) = T(q, \dot{q}, t) - V(q, t)$
		\[ \dod{}{t} \left( \dpd{\Lagr}{\dot{q}_i} \right) - \dpd{\Lagr}{q_i} = 0 \quad i=1,\,\dots\,n\]
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.95\textwidth}
		\begin{center}\textbf{Nonholonomic Standard Form}\end{center}
		\vspace{-1em}
		$n >$ DOFs, $\therefore$ there are constraint forces $C_i = \sum\limits_{j=1}^m \lambda_j a_{ji}$ in direction of $q_i$. Assuming $\dif q$'s are independent:
		\[ \dod{}{t} \left( \dpd{\Lagr}{\dot{q}_i} \right) - \dpd{\Lagr}{q_i} = \sum\limits_{j=1}^m \lambda_j a_{ji}\quad i = 1, \, \dots, \, n \]
		$\lambda$ is \textit{Lagrange Multiplier}
	\end{minipage}
\end{contentBox}


\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Hamilton's Equation}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		Define the \textit{Hamiltonian:}
		\[ H(q,p,t) \equiv \sum\limits_{i=1}^n \left( p_i \dot{q}_i \right) - L(q, \dot{q}, t) \]
		where $p_i$ are generalized momenta, $q_i$ are generalized coordinates, $L$ is the Lagrangian, and $t$ is time. It is typically useful to substitute expressions of the $p_i$'s in for the $\dot{q}_i$'s. \textit{Note}: The Hamiltonian equation is equivalent to the Jacobi integral equation.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Generalized Momenta}\end{center}
		\[ p_i = \dpd{L}{\dot{q}_i} \]
		
		\begin{center}\textit{Hamilton's Canonical EOMs}\end{center}
		\[ \dot{q}_i = \dpd{H}{p_i} \qquad \dot{p}_i = -\dpd{H}{q_i} \qquad \dpd{L}{t} = -\dpd{H}{t} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Physical Integrals of Motion}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Energy}\end{center}
		\vspace{-1em}
		\begin{itemize}
			\item Each force is derivable from a potential function, $U$
			\item Work $W = U(t_2) - U(t_1)$
			\item Potential Energy $V(t_i) = -U(t_i)$
			\item Total energy is conserved: $E = T + V =$ const.
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Conservative Force Examples
		\begin{itemize}
			\item Uniform Gravity: $U = -mgh$
			\item Inverse-Square Gravity: $U = GMm/r$
			\item Linear Spring: $U = -1/2 k x^2$
		\end{itemize}
		Proof: Find a potential for the force and show that it does no work: $W = 0 \to V = 0 \to F$ is conservative
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Linear Impulse and Linear Momentum}\end{center}
		\vspace{-1em}
		If no forces act in a given inertial direction, momentum in that direction is conserved
		\[ \vec{\mathcal{F}} = \int\limits_{t_1}^{t_2} \vec{F} \dif \tau = \vec{p}_2 - \vec{p}_1 = \Delta \vec{p} \, \quad p = m v\]
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Angular Impulse and Angular Momentum}\end{center}
		\vspace{-1em}
		If moment is zero in an inertial direction, angular momentum is conserved in that direction
		\[ \vec{\mathcal{M}} = \int\limits_{t_1}^{t_2} \vec{M}^Q \dif \tau = {^i \vec{H}^Q} (t_2) - {^i \vec{H}^Q}(t_1) \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Integrals of Motion from Lagrange's Equations}\end{center}
	Obtain further governing equations and simplifications from Integrals
	\vspace{1em}
	
	\begin{minipage}[t]{0.37\textwidth}
		\begin{center}\textbf{``Ignorable'' Coordinates}\end{center}
		\vspace{-1em}
		In a holonomic system, if $\dpd{\Lagr}{q_i} = 0$, then $q_i$ is an \textit{ignorable coordinate}. $\therefore$
		\begin{align*}
			\dod{}{t} \left( \dpd{\Lagr}{\dot{q}_i} \right) &= 0\\
			\dpd{\Lagr}{\dot{q}_i} &= \beta = \text{ const}
		\end{align*}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.53\textwidth}
		\begin{center}\textbf{Jacobi Integral}\end{center}
		\vspace{-1em}
		Similar to energy expression; three conditions must apply:
		\begin{itemize}
			\item Standard form (holonomic or nonholonomic) of LE must apply
			\item $\Lagr$ is not explicit $f(t)$ (may be implicit)
			\item Any constraint can be expressed in differential form with $a_{jt} = 0 \,\, \forall \,\, t$
		\end{itemize}
		then
		\[ h = \sum\limits_{i=1}^n \dpd{\Lagr}{\dot{q}_i} \dot{q}_i - \Lagr = \text{ const} \]
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Energy and Momentum}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Translational Kinetic Energy}\end{center}

	\begin{minipage}[t]{0.46\textwidth}
		\[ T_{trans} = \frac{1}{2}m {^i \vec{v}^{OP} } \]
		where $m$ is the mass of the system, $i$ indicates an inertial observer, and $O$ is inertially fixed.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item Single Particle: ${^i \vec{v}^{OP} }$ represents the velocity of the particle
			\item System of Particles: Can sum energy of each particle or let $m$ represent the system mass and ${^i \vec{v}^{OP} }$ represent the velocity of the CM ($P$ = CM).
			\item Rigid Body: ${^i \vec{v}^{OP} }$ represents the velocity of the CM
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Rotational Kinetic Energy}\end{center}

	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Rigid Body}\end{center}
		\[ T_{rot} = \frac{1}{2} \left({^i \vec{\omega}^b}\right)^T \coxmat{I}^C {^i \vec{\omega}^b} \]
		where $i$ is an inertial frame, $b$ is a body-fixed frame, $\coxmat{I}^C$ is the inertia matrix associated with the body and located at the CM, $C$. Note that $\coxmat{I}$ and $\vec{\omega}$ must use the same working frame ($\hat{b}$ is usually the most convenient).
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{System of Particles}\end{center}
		\[ T_{rot} = \frac{1}{2} \sum\limits_{i=1}^n m_i {^i \vec{v}^{C P_i}} \dotp {^i \vec{v}^{C P_i}} \]
		where $m_i$ is the mass of particle $P_i$ and ${^i \vec{v}^{C P_i}}$ represents the velocity of particle $P_i$ relative to the CM, $C$.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Kinetic Energy with Non-Fixed Base Point}\end{center}
	
	\vspace{1em}
	\begin{minipage}[t!]{0.1\textwidth}
		System\\of\\Particles
	\end{minipage}
	\begin{minipage}[t!]{0.3\textwidth}
		\begin{itemize}
			\item $O$: inertially fixed
			\item $Q$: arbitrary, non-fixed point
			\item $P_i$: particle $i$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t!]{0.5\textwidth}
		\[ T = \frac{1}{2} \mathcal{M} {^i\vec{v}^{OQ}} \dotp {^i\vec{v}^{OQ}} + \mathcal{M} {^i\vec{v}^{OQ}} \dotp {^i\vec{v}^{QC}} + \frac{1}{2} \sum\limits_{i=1}^n m_i {^i\vec{v}^{Q P_i}} \dotp {^i\vec{v}^{Q P_i}} \]
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t!]{0.1\textwidth}
		Rigid Body
	\end{minipage}
	\begin{minipage}[t!]{0.3\textwidth}
		\begin{itemize}
			\item $O$: inertially fixed
			\item $Q$: arbitrary, fixed in RB
			\item $C$: center of mass
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t!]{0.5\textwidth}
		\[ T = \frac{1}{2} m {^i\vec{v}^{OQ}} \dotp {^i\vec{v}^{OQ}} + m {^i\vec{v}^{OQ}} \dotp {^i\vec{v}^{QC}} + \frac{1}{2} \left( {^i \vec{\omega}^b} \right) \coxmat{I}^Q {^i \vec{\omega}^b} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Angular Momentum}\end{center}
	\begin{minipage}{0.475\textwidth}
		\begin{center}System of Particles\end{center}
		\[{^i\vec{H}^Q} = \sum\limits_{j=1}^n \vec{\rho}^{\,Qj} \times m_j \frac{^i \dif \vec{\rho}^{\,Qj}}{\dif t} \]
	\end{minipage}
	\begin{minipage}{0.475\textwidth}
		\begin{center}Rigid Body\end{center}
		\[^i\vec{H}^Q = \coxmat{I}^Q {^i\vec{\omega}^b} \]
		$\hat{b}$ is body-fixed frame, $\hat{i}$ is inertial frame. All vectors must be in same working frame; $\hat{b}$ is typically most convenient for computing $\coxmat{I}^Q$
	\end{minipage}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}Non-Fixed Reference Point\end{center}
		\[ {^i \vec{H} ^O} = {^i \vec{H}^C} + \vec{r}^{OC} \times m {^i \vec{v}^{OC}} \]
		where $O$ is some point, $C$ is fixed in the system
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Inertia}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Inertia Tensor}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\vspace{-1em}
		\[ \coxmat{I} = \begin{bmatrix}
			I_{11} & I_{12} & I_{13}\\
			I_{21} & I_{22} & I_{23}\\
			I_{31} & I_{32} & I_{33}
		\end{bmatrix}
		\]
		Diagonal elements: \textit{Moments of Inertia} (always $> 0$)\\
		Off-diagonal elements: \textit{Products of Inertia}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Position of infinitesimal mass element $P$ with mass $m$ relative to $Q$; entire body has mass $M$:
		\[ \vec{r}^{\,QP} = r_1 \hat{b}_1 + r_2 \hat{b}_2 + r_3 \hat{b}_3 \]
	\end{minipage}
	\[ I_{11} = \int\limits_{M} \left( r_2^2 + r_3^2 \right) \dif m \qquad I_{22} = \int\limits_M \left(r_1^2 + r_3^2 \right) \dif m \qquad I_{33} = \int\limits_M \left(r_1^2 + r_2^2 \right) \dif m \]
	\[ I_{12} = I_{21} = -\int\limits_M r_1 r_2 \dif m \qquad I_{13} = I_{31} = -\int\limits_M r_1 r_3 \dif m \qquad I_{23} = I_{32} = -\int\limits_M r_2 r_3 \dif m \]
	If density $\rho$ is uniform, change integration variable to volume element $\rho \dif v$ and integrate over total volume $V$
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Symmetry Simplifications}\end{center}
		\begin{itemize}
			\item If a rigid body has an axis of symmetry, that axis is always a principal axis
			\item If a rigid body is symmetric about a plane (i.e., $x = 0$), the plane normal (i.e., $\hat{x}$) is a principal axis
			\item Products of Inertia in the same row/column as a principal moment of inertia must be zero
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Rigid Body + Point Masses}\end{center}
		Given a rigid body with inertia $\coxmat{I}_{rb}$ and a set of points masses $m_i$ located at ($x_i$, $y_i$, $z_i$), the total inertia is
		\[ \coxmat{I}_{tot} = \coxmat{I}_{rb} + \sum\limits_{i=1}^n m_i \begin{bmatrix}
			y_i^2 + z_i^2 & -x_i y_i & -x_i z_i\\
			-x_i y_i & x_i^2 + z_i^2 & -y_i z_i\\
			-x_i z_i & -y_i z_i & x_i^2 + y_i^2
		\end{bmatrix} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.55\textwidth}
		\begin{center}\textbf{Parallel Axis Thm}\end{center}
		\[ \underset{\hat{b}}{\coxmat{I}^C} \to \underset{\hat{b}}{\coxmat{I}^Q} \quad \vec{r}^{QC} = c_1 \hat{b}_1 + c_2 \hat{b}_2 + c_3 \hat{b}_c \]
		\[ \underset{\hat{b}}{\coxmat{I}^Q} = \underset{\hat{b}}{\coxmat{I}^C} + \mathcal{M}
		\begin{bmatrix}
			c_2^2 + c_3^2 & -c_1 c_2 & -c_1 c_3\\
			-c_1 c_2 & c_1^2 + c_3^2 & -c_2 c_3\\
			-c_1 c_3 & -c_2 c_3 & c_1^2 + c_2^2
		\end{bmatrix}
		\]
		\vspace{-1em}
		\begin{itemize}
			\item Only works moving to/from CM, $C$
			\item As $Q$ moves away from $C$, moments \underline{always} increase
			\item $\trace \left\{ \underset{\hat{b}}{\coxmat{I}^Q} \right\} \neq \trace \left\{ \underset{\hat{b}}{\coxmat{I}^C} \right\}$
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.35\textwidth}
		\begin{center}\textbf{Similarity Transform}\end{center}
		\[ \underset{\hat{a}}{\coxmat{I}}^Q \to \underset{\hat{b}}{\coxmat{I}}^C \]
		\[ \underset{\hat{b}}{\coxmat{I}}^Q = \underset{\hat{b} \dotp \hat{a}}{\coxmat{L}} \underset{\hat{a}}{\coxmat{I}}^Q \underset{\hat{b} \dotp \hat{a}}{\coxmat{L}}^T \]
		\[\trace \left\{ \underset{\hat{b}}{\coxmat{I}}^Q \right\} = \trace \left\{ \underset{\hat{a}}{\coxmat{I}}^Q \right\} \]
		\begin{center}\textbf{Superposition}\end{center}
		Can sum inertia tensors in same working frame relative to same base point.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Principle Moments and Directions}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		If basis $\hat{a}$ aligns with principle directions about pt $Q$, then
		\[ \underset{\hat{a}}{\coxmat{I}}^Q = \begin{bmatrix}
			I_1 & 0 & 0\\ 0 & I_2 & 0\\ 0 & 0 & I_3
		\end{bmatrix} \]
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{itemize}
			\item $I_1$, $I_2$, $I_3$ are principle moments of inertia; eigenvalues of $\underset{\hat{b}}{\coxmat{I}}^Q$
			\item $\hat{a}_1$, $\hat{a}_2$, $\hat{a}_3$ are principle directions; eigenvectors of $\underset{\hat{a}}{\coxmat{I}}^Q$ associated with eigenvalues $I_i$
		\end{itemize}
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Attitude and Orientation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Rotation Matrices}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Column Form}\end{center}
		\begin{align*}
			\begin{Bmatrix}b_1\\ b_2\\ b_3 \end{Bmatrix} &= \begin{array}{c|ccc}
				& \hat{a}_1 & \hat{a}_2 & \hat{a}_3\\
				\hline
				\hat{b}_1 & l_{11} & l_{12} & l_{13}\\
				\hat{b}_2 & l_{21} & l_{22} & l_{23}\\
				\hat{b}_3 & l_{31} & l_{32} & l_{33}
			\end{array} \begin{Bmatrix} a_1\\ a_2\\ a_3 \end{Bmatrix}\\
			\underset{\hat{b}}{\vec{x}} &= \underset{\hat{b} \dotp \hat{a}}{\coxmat{L}} \underset{\hat{a}}{\vec{x}} \qquad l_{ij} = \hat{b}_i \dotp \hat{a}_j\\
			\underset{\hat{i} \dotp \hat{b}}{\coxmat{L}} &= \underset{\hat{i} \dotp \hat{a}}{\coxmat{L}} \underset{\hat{a} \dotp \hat{b}}{\coxmat{L}} \qquad
			\underset{\hat{b} \dotp \hat{a}}{\coxmat{L}} = \underset{\hat{a} \dotp \hat{b}}{\coxmat{L}}^T \qquad \coxmat{L}^T = \coxmat{L}^{-1}
		\end{align*}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Row Form}\end{center}
		\begin{align*}
			\begin{Bmatrix} b_1 & b_2 & b_3 \end{Bmatrix} &= \begin{Bmatrix} a_1 & a_2 & a_3 \end{Bmatrix} \begin{array}{c|ccc}
				& \hat{b}_1 & \hat{b}_2 & \hat{b}_3\\
				\hline
				\hat{a}_1 & c_{11} & c_{12} & c_{13}\\
				\hat{a}_2 & c_{21} & c_{22} & c_{23}\\
				\hat{a}_3 & c_{31} & c_{32} & c_{33}
			\end{array}\\
			\underset{\hat{b}}{\vec{x}} &= \underset{\hat{a}}{\vec{x}} \left({^a \coxmat{C}^b} \right)\\
		 	{^a \coxmat{C}^b} &= \underset{\hat{b} \dotp \hat{a}}{\coxmat{L}}^T = \underset{\hat{a} \dotp \hat{b}}{\coxmat{L}}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Simple Rotations: Euler Axis, Euler Angle}\end{center}
	Begin in orientation $\hat{a}_i$; Rotate about axis $\hat{\lambda}$ by some angle $\theta$ to achieve orientation $\hat{b}_i$
	
	\begin{minipage}[t]{0.25\textwidth}
		Can compute row form of rotation matrix, ${^a \coxmat{C}^b}$ from Euler axis and Euler angle pair. \textit{Note}: $\hat{\lambda}$ must be defined in $\hat{a}$
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.65\textwidth}
		\[
			{^a \coxmat{C}^b} = \begin{bmatrix}
				C_{\theta} + \lambda_1^2(1 - C_{\theta}) &
				-\lambda_3 S_{\theta} + \lambda_1 \lambda_2(1 - C_{\theta}) &
				\lambda_2 S_{\theta} + \lambda_3 \lambda_1(1 - C_{\theta})\\
				\lambda_3 S_{\theta} + \lambda_1 \lambda_2(1 - C_{\theta}) &
				C_{\theta} + \lambda_2^2(1 - C_{\theta}) &
				-\lambda_1 S_{\theta} + \lambda_2 \lambda_3(1 - C_{\theta})\\
				-\lambda_2 S_{\theta} + \lambda_3 \lambda_1(1 - C_{\theta}) &
				\lambda_1 S_{\theta} + \lambda_2 \lambda_3(1 - C_{\theta}) &
				C_{\theta} + \lambda_3^2(1 - C_{\theta})
			\end{bmatrix}				
		\]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Euler Parameters - Quaternions}\end{center}
	Given an Euler axis and Euler angle pair that describe a rotation from $\hat{a}$ to $\hat{b}$, the rotation may be represented as a four element vector
	\[ {^a \vec{\epsilon}^{\,b}} = \begin{Bmatrix} \epsilon_1 & \epsilon_2 & \epsilon_3 \end{Bmatrix}^T \equiv \hat{\lambda} \sin \left(\frac{\theta}{2} \right) \qquad {^a \epsilon^b} \equiv \cos \left( \frac{\theta}{2} \right) \qquad \vec{q} = \begin{Bmatrix} \epsilon_1 & \epsilon_2 & \epsilon_3 & \epsilon_4 \end{Bmatrix}^T \]
	\textit{Note}: $\hat{\lambda}$ may be in $\hat{a}$ or $\hat{b}$; $\norm{\vec{q}}= 1$
	
	\vspace{1em}
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Conversion to Axis/Angle}\end{center}
		\vspace{-1.7em}
		\begin{align*}
			\hat{\lambda} &= \frac{\epsilon_1 \hat{a}_1 + \epsilon_2 \hat{a}_2 + \epsilon_3 \hat{a}_3}{\sqrt{\epsilon_1^2 + \epsilon_2^2 + \epsilon_3^2}}\\
			\theta &= \arctan(\epsilon_4)^*
		\end{align*}
		${^*}$ requires quadrant check; usually assume $\theta > 0$ as $\hat{\lambda}$ defines the direction
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.4\textwidth}
		\begin{center}\textit{Conversion to/from DCM}\end{center}
		\[ {^a \coxmat{C}^b} = 2
			\begin{bmatrix}
				\frac{1}{2} - \epsilon_2^2 - \epsilon_3^2 & \epsilon_1 \epsilon_2 - \epsilon_3 \epsilon_4 & \epsilon_3 \epsilon_1 + \epsilon_2 \epsilon_4\\
				\epsilon_1 \epsilon_2 + \epsilon_3 \epsilon_4 & \frac{1}{2} - \epsilon_3^2 - \epsilon_1^2 & \epsilon_2 \epsilon_3 - \epsilon_1 \epsilon_4\\
				\epsilon_3 \epsilon_1 - \epsilon_2 \epsilon_4 & \epsilon_2 \epsilon_3 + \epsilon_1 \epsilon_4 & \frac{1}{2} - \epsilon_1^2 - \epsilon_2^2
			\end{bmatrix}		
		\]
	\end{minipage}
	\begin{minipage}[t]{0.2\textwidth}
		\begin{align*}
			\epsilon_1 &= \frac{c_{32} - c_{23}}{4 \epsilon_4}\\
			\epsilon_2 &= \frac{c_{13} - c_{31}}{4 \epsilon_4}\\
			\epsilon_3 &= \frac{c_{21} - c_{12}}{4 \epsilon_4}\\
			\epsilon_4 &= \frac{1}{2}\sqrt{1 + c_{11} + c_{22} + c_{33}}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Simple Rotation Theorem}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Euler Axis/Angle}\end{center}
		\vspace{-1em}
		\[ \vec{b} = \vec{a} \cos\theta - \vec{a} \times \hat{\lambda} \sin \theta + \left( \vec{a} \dotp \hat{\lambda} \right) \left(1 - \cos \theta \right) \hat{\lambda} \]
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Euler Parameters}\end{center}
		\vspace{-1em}
		\[ \vec{v} = \vec{a} - 2 \epsilon_4 \vec{a} \times \vec{\epsilon} + 2 \vec{\epsilon} \times (\vec{\epsilon} \times \vec{a}) \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Successive Rotations}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{DCMs}\end{center}
		\vspace{-1.5em}
		\begin{align*}
			{^a \coxmat{C} ^c} &= {^a \coxmat{C} ^b}{^b \coxmat{C} ^c}\\
			\underset{\hat{c} \dotp \hat{a}}{\coxmat{L}} &= \underset{\hat{c} \dotp \hat{b}}{\coxmat{L}} \underset{\hat{b} \dotp \hat{a}}{\coxmat{L}}
		\end{align*}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Euler Parameters}\end{center}
		\vspace{-1.5em}
		\begin{align*}
			{^a \vec{\epsilon}^{\,c}} &= {^a \vec{\epsilon}^{\,b}} {^b \epsilon_4^c} + {^b \vec{\epsilon}^{\,c}}{^a \epsilon_4^b} + {^b \vec{\epsilon}^{\,c}} \times {^a \vec{\epsilon}^{\,b}}\\
			{^a \epsilon_4^c} &= {^a \epsilon_4^b}{^b \epsilon_4^c} - {^a \vec{\epsilon}^{\,b}} \dotp {^b \vec{\epsilon}^{\,c}}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Angular Velocity}\end{center}
	
	\begin{minipage}[t]{0.3\textwidth}
		The rotation of frame $\hat{b}$ with respect to frame $\hat{a}$ is described by ${^a \vec{\omega}^b}$.
		\[ {^a \vec{\omega}^b} = -{^b \vec{\omega}^a} \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.2\textwidth}
		From a simple rotation,
		\[ {^a \vec{\omega}^b} = \dot{\theta}\hat{\lambda} \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.4\textwidth}
		From Euler parameters,
		\[ {^a \vec{\omega}^b} = 2 {^a \dot{\vec{q}}^{\,b}} {^a \coxmat{E}^b} \]
		where ${^a \vec{\omega}^b}$ is expressed in $\hat{b}_i$ and $\vec{q}$ and $\coxmat{E}$ are expressed in $\hat{a}_i$ or in $\hat{b}_i$, and
		\[ \coxmat{E} = \begin{bmatrix}
			\epsilon_4 & -\epsilon_3 & \epsilon_2 & \epsilon_1\\
			\epsilon_3 & \epsilon_4 & -\epsilon_1 & \epsilon_2\\
			-\epsilon_2 & \epsilon_1 & \epsilon_4 & \epsilon_3\\
			-\epsilon_1 & -\epsilon_2 & -\epsilon_3 & \epsilon_4
		\end{bmatrix} 
		\]
	\end{minipage}
\end{contentBox}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Spring-Mass-Damper Problem}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
		EOMs (various, equivalent forms):
		\begin{align*}
			m \ddot{x} + c\dot{x} + kx &= F(t)\\
			\ddot{x} + p\dot{x} + qx &= \frac{1}{m}F(t)\\
			\ddot{x} + 2\rho \omega_n \dot{x} + \omega_n^2 x &= \frac{1}{m}F(t)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		where:
		\begin{itemize}
			\item $p = \frac{c}{m}$, $q = \frac{k}{m}$
			\item Natural frequency $\omega_n = \sqrt{k/m}$
			\item Damping ratio $\rho = \frac{c}{2 \omega_n}$
			\item Forcing term $F(t)$
		\end{itemize}
	\end{minipage}
	
	\begin{center}\textbf{Solving}\end{center}
	
	\begin{minipage}[t]{0.42\textwidth}
		1) Find characteristic roots
		\begin{align*}
			s &= -\rho \omega_n \pm \omega_n \sqrt{\rho^2 - 1} \quad\text{(general)}\\
			&= \sigma \pm \omega_d j \quad(\rho < 1)
		\end{align*}
		3) Find a particular solution $x_p$
		\begin{enumerate}[label=\roman*]
			\item Pick a form that corresponds to $F(t)$\footnote{More on this topic in diff. EQ chapter}
			\item Plug $x_p$ into original diff. Eq. and solve for constants in $x_p$
		\end{enumerate}
		4) Combine forms for total solution
		\begin{enumerate}[label=\roman*]
			\item $x=x_h + x_p$
			\item Apply B.C. and/or I.C. to find remaining constants
		\end{enumerate}
	\end{minipage}
	\hspace{0.04\textwidth}
	\begin{minipage}[t]{0.48\textwidth}
		2) Find homogeneous solution $x_h$
		\begin{itemize}
			\item Overdamped ($\rho > 1$): distinct, real roots
			\[x_h = c_1 e^{s_1 t} + c_2 e^{s_2 t}\]
			\item Critically damped ($\rho = 1$): non-distinct, real roots \[x_h = (c_1 + c_2 t) e^{st}\]
			\item Underdamped ($\rho < 1$) complex roots
			\begin{align*}	
				x_h &= e^{\sigma t} \left(A \cos(\omega_d t) + B\sin(\omega_d t) \right)\\
				&= R e^{\sigma t} \sin(\omega_d t + \phi), \qquad \phi = \arctan(A/B)\\
				& \qquad R = \sqrt{A^2 + B^2}\\
				&= R e^{\sigma t} \cos(\omega_d t + \psi), \quad \psi = \arctan(-B/A)
			\end{align*}								
		\end{itemize}
	\end{minipage}
\end{contentBox}
