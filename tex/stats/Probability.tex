\section{Probability}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t!]{0.25\textwidth}
		\begin{tikzpicture}
			\draw[blue] (-1.5,1) node[anchor=east]{$S$}--(1.5,1)--(1.5,-1)--(-1.5,-1)--cycle;
			\draw[red] (-0.5,0) circle (0.75);
			\draw[red] (-1,0.48) node[anchor=east]{$A$};
			\draw[dkgreen] (0.5,0) circle (0.75);
			\draw[dkgreen] (1,0.48) node[anchor=west]{$B$};
		\end{tikzpicture}
	\end{minipage}%
	\begin{minipage}[t!]{0.75\textwidth}
		Probability maps events to numbers; $P: \epsilon \to [0,1]$
		\begin{itemize}
			\item \textbf{Sample Space} $S$ or $\Omega$: Contains all possible outcomes/events
			\item \textbf{Complement}: $A^C$ or $A'$ includes all outcomes in $S$ not contained in $A$
			\item \textbf{Union}: $A \cup B$ contains all events in $A$ or $B$
			\item \textbf{Intersection}: $A \cap B$ contains all events in both $A$ and $B$
			\item \textbf{Empty Set} $\emptyset$: contains no events $\to$ If $A \cap B = \emptyset$, $A$ and $B$ are mutually exclusive, or ``disjoint''
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{center}\textbf{Axioms of Probability}\end{center}\vspace{-5mm}
		\begin{enumerate}
			\item For any event $A$, $P(A) \geq 0$
			\item $P(S) = 1$
			\item If events are mutually exclusive, then $P(A_1 \cup A_2 \cup \dots \cup A_n) = \sum\limits^n_{j=1} P(A_j)$
		\end{enumerate}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{center}\textbf{From the Axioms:}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item $P(A \cup B) = P(A) + P(B) - P(A \cap B)$
			\item $P(A \cup B \cup C) = P(A) + P(B) + P(C) - P(A\cap B) - P(A\cap C) - P(B\cap C) + P(A\cap B\cap C)$
			\item $P(A^C) = 1 - P(A)$
			\item $P(A\cap B^C) = P(A) - P(A\cap B)$
		\end{itemize}
	\end{minipage}
	
	\begin{minipage}{0.4\textwidth}
		\textbf{DeMorgan's Law:}\vspace{-3mm}
		\begin{align*}
			(A \cup B)' &= A' \cap B'\\
			(A \cap B)' &= A' \cup B'\\
			(A \cup B \cup C)' &= A' \cap B' \cap C'\\
			&\text{pattern continues}
		\end{align*}
	\end{minipage}
	\begin{minipage}{0.6\textwidth}
		\begin{itemize}
			\item $(A \cap B) \cup C = (A\cup C) \cap (B\cup C)$
			\item $(A \cup B) \cap C = (A \cap C) \cup (B \cap C)$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Conditional Probability}\end{center}
	\begin{minipage}[t!]{0.45\textwidth}
		Probability that $A$ has occurred given that $B$ has occurred (\textit{Baye's Thm}):
		\[ P(A \given B) = \frac{P(A \cap B)}{P(B)} \]
		
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.45\textwidth}
		Satisfies axioms:
		\begin{enumerate}
			\item $P(A\given B) \geq 0$
			\item $P(\Omega \given B) = 1$
			\item If $A\cap B = \emptyset$, $P(A\cup C \given B) = P(A\given B) + P(C \given B)$
		\end{enumerate}
	\end{minipage}
	
	\begin{minipage}[t!]{0.2\textwidth}
		\begin{tikzpicture}
			\draw (-1,1)node[anchor=east]{$\Omega$} -- (1,1) -- (1,-1) -- (-1,-1) --cycle;
			\draw (0,1) -- (0,-1);
			\draw (-1,0) -- (1,0);
			\filldraw [blue, opacity=0.7] (0,0) circle (0.5);
			\draw[blue] (-0.2,0.6) node{$B$};
			\draw (-0.8,0.8) node{$A_1$};
			\draw (0.8,0.8) node{$A_2$};
			\draw (0.8,-0.8) node{$A_3$};
			\draw (-0.8,-0.8) node{$A_4$};
		\end{tikzpicture}
	\end{minipage}%
	\begin{minipage}[t!]{0.8\textwidth}
		If $A_i \cup A_j = \emptyset$, $i \neq j \, \forall \, i,j$ and $A_1 \cup A_2 \cup \dots \cup A_k = \Omega$ then\\ $P(B) = P(B\given A_1)P(A_1) + \dots + P(B\given A_k)P(A_k)$
		\begin{itemize}
			\item[$\to$] $A_1 \dots A_k$ are called a ``partition''
			\item[$\to$] Partitions lend themselves to tree diagrams well!
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Independence}\end{center}
	$A$ and $B$ are ``independent'' iff $P(A\given B) = P(A)$, so $P(A\cap B) = P(A)P(B)$
	\begin{itemize}
		\item[$\to$] Also true: $P(A\cap B') = P(A)P(B')$, $P(A'\cap B) = P(A')P(B)$, $P(A' \cap B') = P(A')P(B')$
		\item[$\to$] Can expand to three or more elements as well. If $A$, $B$, $C$ are independent, then $A$ and $B$ , $B$ and $C$, and $A$ and $C$ are independent, and $P(A\cap B\cap C) = P(A)P(B)P(C)$
		\begin{itemize}
			\item ``Pair-wise independence'' - In a set of 3 or more events, any two are independent of each other but the entire set is not independent: i.e. $A$ and $B$ , $B$ and $C$, and $A$ and $C$ are independent, but $A$, $B$, and $C$ are not independent.
		\end{itemize}
	\end{itemize}
\end{contentBox}