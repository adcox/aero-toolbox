\section{Statistics and Statistic Testing}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Definitions}\end{center}
	\begin{itemize}
		\item \textbf{Statistic}: any quantity whose value can be calculated from sample data (e.g. mean, median, standard deviation). Prior to obtaining data, there is uncertainty as to the value of any particular statistic. Therefore a statistic is a random variable (uppercase letter); the calculated or observed value is not (lowercase letter) - For example, sample mean as a statistic is $\bar{X}$, but the calculated value is $\bar{x}$	
		\item \textbf{Identically and Independently Distributed} (IID): Suppose $X_1, X_2,\dots, X_n$ are (IID). Then, $E(X_1) = E(X_2) = \dots = E(X_n) = \mu$ and $V(X_1)=V(X_2) = \dots = V(X_n) = \sigma^2$
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Distribution of Sample Mean}\end{center}
	Sample mean $\bar{X}$ is useful in drawing conclusions about the population mean $\mu$.
	Let $X_1,X_2,\dots,X_n$ be a random sample from a distribution with  mean $\mu$ and standard deviation $\sigma$. Then
		
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{itemize}
			\item Sample mean $\bar{X} = \frac{1}{n}\sum\limits_{i=1}^n X_i$ \quad $E(\bar{X}) = \mu$ \quad $V(\bar{X}) = \sigma^2/n$
			\item Sample total $T = X_1 + X_2 + \dots + X_n$ \quad $E(T) = n\mu$ \quad $V(T) = n\sigma^2$
		\end{itemize}		
	\end{minipage}%
	\begin{minipage}[t!]{0.05\textwidth}
		\[\left\}\begin{array}{l}
		\phantom{a} \\ \phantom{a}
		\end{array}\right.\]
	\end{minipage}%
	\begin{minipage}[t!]{0.3\textwidth}
	If $X_1,\dots,X_n$ are from a \textit{normal} distribution, then for any $n$, $\bar{X}$ and $T$ are also normally distributed
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Central Limit Theorem}\end{center}
	\begin{minipage}[t]{0.65\textwidth}
		Let $X_1, X_2,\dots,X_n$ be a random sample from a distribution with mean $\mu$ and variance $\sigma^2$. If $n$ is ``sufficiently large'' ($n > 30^*$):
		\begin{enumerate}
			\item $\bar{X}$ as approximately a normal distro with $\mu_{\bar{x}} = \mu$ and $\sigma^2_{\bar{x}} = \sigma^2/n$
			\item $T$ also has approximately a normal distro with $\mu_T = n\mu$ and $\sigma_T^2 = n \sigma^2$
		\end{enumerate}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.25\textwidth}
		$^*$ 30 is a rule of thumb; some distributions require more data to be considered normal, some less.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Point Estimation}\end{center}
	\begin{itemize}
		\item[$\to$] \textit{Statistical Inference}: usually directed towards drawing some conclusion about one or more parameters based on information from sample we collect.
		\item[$\to$] \textit{Estimation}: estimates the state of population, usually characterized by some parameter $\theta$
		\item[$\to$] \textit{Hypothesis Testing}: chooses from postulated states of population
	\end{itemize}
	\begin{minipage}[t]{0.475\textwidth}
		\textbf{Point Estimate} of a parameter $\theta$ is a single number that is a sensible value for $\theta$
		\begin{itemize}
			\item Obtained by selecting a suitable statistic and computing its value from sample data
			\item $\hat{\theta}$ is the estimator of $\theta$
			\item Mean Square Error: MSE = $E[(\hat{\theta} - \theta)^2]$
			\item Typically not possible to minimize MSE unless estimator has certain desirable properties, e.g. unbiasedness
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.475\textwidth}
		$\hat{\theta}$ is an \textbf{unbiased estimator} of $\theta$ if $E[\hat{\theta}] = \theta$ for every possible value of $\theta$ ($E[\hat{\theta}] - \theta$ is the \textbf{bias} of $\theta$)
		\begin{itemize}
			\item Unbiased if sampling distribution of $\hat{\theta}$ is always centered at true value of parameter
			\item Example: $X \sim \text{Binomial}(n, p)$, then $\hat{p} = X/n$ is unbiased
			\item Standard Error: $\sigma_{\hat{\theta}} = \sqrt{V(\hat{\theta})}$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Confidence Intervals}\end{center}
	A set (depending on observed data) that we are $(100 - \alpha)$\% confident includes the true parameter of interest; If we repeat the procedure many times, in the long run about $100(1-\alpha)$\% of the confidence intervals will contain the true value
	\begin{itemize}
		\item Tighter intervals require larger sample sizes
		\item More population variability necessitates larger sample size ($n$ is increasing function of $\sigma$)
		\item As $\alpha$ increases, $z_{\alpha/2}$ increases, so interval widens
		\item Width of interval denoted $w$, ``margin of error'' is $\frac{1}{2}w$
	\end{itemize}
	Parameter $\alpha$ is equivalent to max. Type I error probability. In hypothesis testing, the confidence interval is the region in which a two-tailed test fails to reject $H_0$
\end{contentBox}

\pagebreak
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Confidence Intervals for Population Mean}\end{center}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Normal Population, $\sigma$ known}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item IID normal R.V. $X_1,\dots,X_n \sim N(\mu, \sigma)$
			\item We have $\bar{X} \sim N(\mu, \sigma/\sqrt{n})$
			\item[$\to$] CI: $\bar{x} \pm z_{\alpha/2} \frac{\sigma}{\sqrt{n}}$ where $z_{\alpha/2}$ is the $100(1 - \alpha/2)$th percentile
			\item Min sample size for width $w$ is $n = \left( 2z_{\alpha/2}\frac{\sigma}{w} \right)^2$
		\end{itemize}	
		
		\begin{center}\textbf{Any Large Population ($n > 40$), $\sigma$ Unknown}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item Let $X_1,\dots,X_n$ be IID, but not necessary normal
			\item Because $n$ is large: $\frac{\bar{X} - \mu}{s/\sqrt{n}} \approx N(0,1)$
			\item[$\to$] CI: $\bar{x} \pm z_{\alpha/2} \frac{s}{\sqrt{n}}$ (valid regardless of shape of population distribution)
		\end{itemize}
		
		\begin{center}\textbf{Small Normal Population, $\sigma$ Unknown}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item ``One-Sample $t$ Confidence Interval''
			\item[$\to$] CI: $\bar{x} \pm t_{\alpha/2,\,n-1} s/\sqrt{n}$
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Chi-Squared Distribution}\end{center}\vspace{-3mm}
		Suppose $X_1,\dots,X_n$ are IID N(0,1). Then
		\[\sum\limits_{i=1}^n X_i^2 \sim \chi_n^2 \] where $\chi_n^2$ represents the chi-squared distro with $n$ degrees of freedom.
		\begin{itemize}
			\item We denote $\chi_{\alpha, \nu}$ as the upper probability of chi-squared distro with $\nu$ DOF; this value is called a \textit{critical value}.
			\item Distribution is not symmetric; upper and lower tail probabilities are not equal.
		\end{itemize}
		
		\begin{center}\textbf{$t$ Distribution}\end{center}\vspace{-3mm}
		If $X \sim$ N(0,1), $Y \sim \chi_n^2$, and $X$ and $Y$ are independent then
		\[ T = \frac{X}{\sqrt{Y/n}} \sim t_n \] where $t_n$ represents the $t$ distribution with $n$ DOF
		\begin{itemize}
			\item $t_{\alpha, \nu}$ is the upper probability of $t$ distro with $\nu$ DOF; this value is called a \textit{critical value}
			\item Distribution is symmetric; upper and lower tail probabilities are the same.
		\end{itemize}
	\end{minipage}
\end{contentBox}
	
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Use of Chi-Squared and $t$ Distros: Theorems}\end{center}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{$\chi^2$ Distro}\end{center}\vspace{-3mm}
		Let $X_1,\dots,X_n$ be a random sample from a normal distro with parameters $\mu$ and $\sigma$. Then the R.V.
		\[ \frac{(n-1)S^2}{\sigma^2} = \frac{\sum(X_i - \bar{X})^2}{\sigma^2} \sim \chi_{n-1}^2 \]
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{$t$ Distro}\end{center}\vspace{-3mm}
		Let $\bar{X}$ be the mean of a random sample of size $n$ from a normal distribution with mean $\mu$, then
		\[ T = \frac{\bar{X} - \mu}{S/\sqrt{n}} \sim t_{n-1} \] Note that both sample mean and standard deviation ($\bar{X}$ and $S$) are denoted as R.V.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Confidence Intervals for Variance and Stand. Dev. of a Normal Population}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
		CI for $\sigma^2$ of a normal population:
		\[ \left( \frac{(n-1)s^2}{\chi^2_{\alpha/2,\,n-1}},\,\, \frac{(n-1)s^2}{\chi^2_{1-\alpha/2,\,n-1}} \right) \]
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		CI for $\sigma$ of a normal population:
		\[ \left( \sqrt{\frac{(n-1)s^2}{\chi^2_{\alpha/2,\,n-1}}},\,\, \sqrt{\frac{(n-1)s^2}{\chi^2_{1-\alpha/2,\,n-1}}} \right) \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Hypothesis and Test Procedures}\end{center}
	A \textit{statistical hypothesis} makes a claim about the value of a single parameter, several parameters, or the form of a distribution (e.g. $\mu = 0.75$, $p < 0.1$, $\sigma \neq 2$). We have two contradictory hypotheses under consideration:
	\begin{itemize}
		\item \textbf{Null Hypothesis}: $H_0$ is the claim that is initially assumed to be true (we will primarily use an equality claim: $\theta = \theta_0$
		\item \textbf{Alternate Hypothesis}: $H_1$ or $H_a$ contradicts $H_0$. Can take the form $\theta > \theta_0$, $\theta \neq \theta_0$, or $\theta < \theta_0$
		\item If we conclude $H_0$, we say ``we fail to reject $H_0$''; if we conclude $H_a$, we say ``we reject $H_0$ and conclude $H_a$''
	\end{itemize}
	
	\vspace{3mm}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Test Procedure}\end{center}\vspace{-5mm}
		\begin{enumerate}
			\item Choose $H_0$ and $H_a$
			\item Choose a test statistic, a function of the sample data on which the decision (reject or not reject $H_0$) is to be based
			\item Choose a \textit{significance level} $\alpha$, the maximum allowable Type I error probability
			\item Choose a rejection region $f(\alpha)$, the set in which we reject $H_0$ if the test statistic falls within the set
			\item Make conclusions based on test statistic and rejection region
		\end{enumerate}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Errors}\end{center}\vspace{-3mm}
		\begin{tabular}{c | c c}
			& \multicolumn{2}{c}{Truth}\\
			Conclusion & $H_0$ & $H_a$\\
			\hline
			$H_0$ & Correct & Type II Error\\
			$H_a$ & Type I Error & Correct\\
		\end{tabular}
		\begin{itemize}
			\item Type I error rate (probability):\newline P(Conclude $H_a \given$ $H_0$ is true)
			\item ``Significance Level'' $\alpha$ is maximum of Type I error probabilities
			\item Type II error rate (probability):\newline P(Conclude $H_0 \given$ $H_a$ is true)
		\end{itemize}
	\end{minipage}
\end{contentBox}

\subsection{Testing Single Populations}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Tests about Population Mean}\end{center}
	Consider $H_0: \mu = \mu_0$ and three alternate hypothesis: $H_a: \mu > \mu_0$ (upper-tailed test), $H_a: \mu < \mu_0$ (lower-tailed), $H_a: \mu \neq \mu_0$ (two-tailed); $z_{\alpha}$ corresponds to the $100(1-\alpha)$ percentile (will be a positive number) of a normal distribution
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Case I: Normal Population with Known $\sigma$}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item Test statistic: $z = \frac{\bar{x} - \mu_0}{\sigma/\sqrt{n}}$
			\item Upper-Tailed: Reject $H_0$ if $z \geq z_{\alpha}$
			\item Lower-Tailed: Reject $H_0$ if $z \leq z_{\alpha}$
			\item Two-Tailed: Reject $H_0$ if $z \geq z_{\alpha/2}$ or $z \leq -z_{\alpha/2}$
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Case II: Any Large Population}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item Test statistic: $z = \frac{\bar{x} - \mu_0}{s/\sqrt{n}}$
			\item Upper-Tailed: Reject $H_0$ if $z \geq z_{\alpha}$
			\item Lower-Tailed: Reject $H_0$ if $z \leq z_{\alpha}$
			\item Two-Tailed: Reject $H_0$ if $z \geq z_{\alpha/2}$ or $z \leq -z_{\alpha/2}$
		\end{itemize}
	\end{minipage}
	
	\vspace{3mm}
	\begin{minipage}[t]{0.45\textwidth}
		Type II error probability for case I:
		\begin{itemize}
			\item Upper-Tailed: $\Phi \left(|z_{\alpha}| + \frac{\mu_0 - \mu'}{\sigma/\sqrt{n}} \right)$
			\item Lower-Tailed: $1 - \Phi \left(-|z_{\alpha}| + \frac{\mu_0 - \mu'}{\sigma/\sqrt{n}} \right)$
			\item Two-Tailed:\newline $\Phi \left(|z_{\alpha/2}| + \frac{\mu_0 - \mu'}{\sigma/\sqrt{n}} \right) -  \Phi\left(-|z_{\alpha/2}| + \frac{\mu_0 - \mu'}{\sigma/\sqrt{n}} \right)$
		\end{itemize}
		Sample size $n$ for which a level $\alpha$ test also has $\beta(\mu') = \beta$:
		\begin{itemize}
			\item One-Tailed: $n = \left[ \frac{\sigma(z_{\alpha} + z_{\beta})}{\mu_0 - \mu'} \right]^2$
			\item Two-Tailed: $n = \left[ \frac{\sigma(z_{\alpha/2} + z_{\beta})}{\mu_0 - \mu'} \right]^2$
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Case III: Normal Population, Unknown $\sigma$}\end{center}\vspace{-5mm}
		\begin{itemize}
			\item Test statistic: $t = \frac{\bar{x} - \mu_0}{s/\sqrt{n}}$
			\item Upper-Tailed: Reject $H_0$ if $t \geq t_{\alpha,\,n-1}$
			\item Lower-Tailed: Reject $H_0$ if $t \leq t_{\alpha,\,n-1}$
			\item Two-Tailed: Reject $H_0$ if $t \geq t_{\alpha/2,\,n-1}$ or $t \leq -t_{\alpha/2,\,n-1}$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Tests about Population Proportion}\end{center}
	\begin{minipage}[t]{0.375\textwidth}
		Assumptions:
		\begin{itemize}
			\item Large $n$: $np_0 \geq 10$, $n(1-p_0) \geq 10$
			\item Observed $X \sim Bin(n,p)$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.6\textwidth}
		Let $\hat{p} = X/n$, then $\hat{p} \sim N(p, p(1-p)/n)$. Null hypothesis: $H_0: p = p_0$ Test statistic:
		\[ z = \frac{\hat{p} - p_0}{\sqrt{p_0(1-p_0)/n}} \]
	\end{minipage}
	
	\begin{minipage}[t]{0.3\textwidth}
		\[ \text{CI: } \hat{p} \pm z_{\alpha/2} \sqrt{\hat{p}(1-\hat{p})/n} \]
	\end{minipage}
	\begin{minipage}[t]{0.7\textwidth}
		\begin{align*}
			H_a: p > p_0, &\text{ reject } H_0 \text{ if } z \geq z_{\alpha}\\
			H_a: p < p_0, &\text{ reject } H_0 \text{ if } z \leq -z_{\alpha}\\
			H_a: p \neq p_0, &\text{ reject } H_0 \text{ if } z \geq z_{\alpha/2} \text{ or } z \leq -z_{\alpha/2}\\
		\end{align*}
	\end{minipage}
	
	Note, for small samples, we cannot make the normal distribution assumption and must base probabilities on the binomial distribution instead.
	
	\begin{minipage}[t]{0.475\textwidth}
		For $H_a: p > p_0$, find rejection region from probability:
		\begin{align*}
			P(\text{type I error}) = \alpha &= P(X \geq c)\quad X \sim Bin(n, p_0)\\
			&= 1 - P(X \leq c)\\
			&= 1 - B(c-1; n, p_0)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.475\textwidth}
		where $B$ is the binomial cumulative distribution function (discrete, tabulated).\newline
		
		The value critical value $c$ is the smallest $c$ for which 1 - $B(c-1;n,p_0) \leq \alpha$. We cannot typically enforce equality because the distribution is discrete.\newline
		
		Similarly, for $H_a: p < p_0$, $\alpha = B(c; n, p_0)$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{P-Value}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	
	
\end{contentBox}

\subsection{Comparing Populations}

\subsubsection{Comparing Mean}
\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Difference in Population Mean}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Two-Sample t-Test}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Pooled t-Test}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
	
	\end{minipage}
	
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Analysis of Paired Data}\end{center}
	\begin{minipage}[t]{0.6\textwidth}
		Assumptions:
		\begin{itemize}
			\item Data consists of independent pairs: $(X_1, Y_1), \dots, (X_n, Y_n)$
			\item $E(X_i) = \mu_1$, $E(Y_i) = \mu_2 \,i = 1, 2, \dots, n$
			\item $D_i \sim N(\mu_D, \sigma_D)$ where $D_i = X_i - Y_i, \mu_D = \mu_1 - \mu_2$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.4\textwidth}
		Let $\bar{D} = 1/n\sum\limits_{i=1}^n D_i$, then
		\[ T = \frac{\bar{D} - \mu_D}{s_D/\sqrt{n}} \sim t_{n-1}\]
		where $s_D$ is the sample standard deviation
	\end{minipage}
	
	\vspace{3mm}
	\begin{minipage}[t]{0.3\textwidth}
		$\bar{d}$: observed value of $\bar{D}$
		\[ \text{CI: } \bar{d} \pm t_{\alpha/2, n-1} s_D/\sqrt{n} \]
	\end{minipage}
	\begin{minipage}[t]{0.7\textwidth}
		Under $H_0: \mu_d = \Delta_0$, test statistic: $t = (\bar{d} - \Delta_0)/(s_D/\sqrt{n})$
		\begin{align*}
			H_a: \mu_d > \Delta_0 \to &\text{ reject } H_0 \text{ if } t \geq t_{\alpha, n-1}\\
			H_a: \mu_d < \Delta_0 \to &\text{ reject } H_0 \text{ if } t \leq -t_{\alpha, n-1}\\
			H_a: \mu_d \neq \Delta_0 \to &\text{ reject } H_0 \text{ if } t \geq t_{\alpha/2, n-1} \text{ or } t \leq -t_{\alpha/2,n-1}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{F-Distribution}\end{center}
	Assume $X \sim \chi^2_n$, $Y \sim \chi^2_n$. Then $F = \frac{X/m}{Y/n} \sim F_{m,n}$ where $m$ is the ``numerator DOF'' and $n$ is the ``denominator DOF.'' This distro is not symmetric; if $F \sim F_{m,n}$, then $F_{n,m} = 1/F$; i.e. $F_{\alpha,m,n} = \frac{1}{F_{1-\alpha, n, m}}$
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Single Factor ANOVA}\end{center}
	Compare the means of several groups/populations; ANOVA $\to$ ``Analysis of Variance''

	\begin{minipage}{\textwidth}
		\begin{itemize}
			\item $I$ populations, each population has $J_i$ measurements
			\item $x_{ij}$ is the $j$th measurement for population $i$
			\item $\mu_1, \mu_2, \dots, \mu_I$ are the means of the populations
			\item Assume all populations have std $\sigma$ (valid if max($\sigma_i$) $\leq$ 2min($\sigma_i$))
			\item Consider case: $H_o: \mu_1 = \mu_2 = \dots = \mu_I$ vs. $H_a$: Otherwise	
		\end{itemize}			
	\end{minipage}
	
	\begin{minipage}[t]{0.35\textwidth}
		\begin{align*}
			\text{Sample Mean: } \bar{x}_i &= \frac{1}{J_i}\sum\limits_{j=1}^{J_i} x_{ij}\\
			\text{Sample Var: } s_i^2 &= \frac{1}{J_i - 1} \sum\limits_{j=1}^{J_i}(x_{ij} - \bar{x}_i)^2\\
			\text{Grand Mean: } \bar{x} &= \frac{1}{n} \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} x_{ij}\\
			\text{Grand Total: } x_T &= \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} x_{ij}\\
			\text{Population Total: } x_{iT} &= \sum\limits_{j=1}^{J_i} x_{ij}
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.6\textwidth}
		\begin{itemize}
			\item SSE - Sum of squares; estimates $\sigma^2$
			\item MSE - Mean square error; pooled variance estimate; unbiased estimate of $\sigma^2$
			\item SSTr - Treatment sum of squares
			\item MSTr - Mean square for treatments
			\item SST - Total sum of squares
		\end{itemize}
		\begin{align*}
			SSE &= \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} (s_{ij} - \bar{x}_i)^2 = \sum\limits_{i=1}^I (J_i -1)s_i^2\\
			SSTr &= \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} (\bar{x}_i - \bar{x})^2 = \sum\limits_{i=1}^I J_i (\bar{x}_i - \bar{x})^2 = \sum\limits_{i=1}^I \frac{x_{iT}^2}{J_i} - \frac{x_T^2}{n}\\
			SST &= \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} (x_{ij} - \bar{x})^2 = SSTr + SSE = \sum\limits_{i=1}^I \sum\limits_{j=1}^{J_i} x_{ij}^2 - \frac{x_T^2}{n}
		\end{align*}
	\end{minipage}
	
	\begin{minipage}[t]{0.6\textwidth}
		\begin{center}ANOVA Table\end{center}
		\begin{tabular}{c | c | c | c | c}
			Source of & Sum of & Mean &\\
			Variation & DOF & Sqaures & Square & $f$\\
			\hline
			Treatments & $I-1$ & $SSTr$ & $MSTr$ & $MSTr/MSE$\\
			Error & $n-I$ & $SSE$ & $MSE$ &\\
			Total & $n-1$ & $SST$ & &
		\end{tabular}
	\end{minipage}
	\begin{minipage}[t]{0.35\textwidth}
		\begin{align*}
			MSE &= \frac{SSE}{n-I}\\
			MSTr &= \frac{SSTr}{I-1}
		\end{align*}
		Rejection Region: $f \geq F_{\alpha, I-1, n-1}$
	\end{minipage}
\end{contentBox}

\subsubsection{Comparing Proportion}
\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Test Differences Between Population Proportions}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
		Assumptions:
		\begin{itemize}
			\item $X \sim Bin(m, p_1)$
			\item $Y \sim Bin(n, p_2)$
			\item $X, Y$ independent
			\item Large sample size: $m\hat{p}_1$, $m(1 - \hat{p}_1)$, $n\hat{p}_2$, and $n(1-\hat{p}_2)$ all $\geq 10$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		where
		\begin{align*}
			\hat{p}_1 &= X/m,\quad \hat{p}_2 = Y/n\\
			\hat{p}_1 - \hat{p}_2 &\sim N(p_1 - p_2, \sigma^2)\\
			\sigma^2 &= \frac{p_1(1-p_1)}{m} + \frac{p_2(1-p_2)}{n}
		\end{align*}
	\end{minipage}
	
	\begin{minipage}[t]{0.4\textwidth}
		\[ \text{CI: }\hat{p}_1 - \hat{p}_2 \pm z_{\alpha/2} \sqrt{\frac{\hat{p}_1(1 - \hat{p}_1}{m} + \frac{\hat{p}_2(1 - \hat{p}_2}{n}} \]
	\end{minipage}
	\begin{minipage}[t]{0.6\textwidth}
		Under $H_0$: $p_1 - p_2 = 0$, $p_1 = p_2 = p$, natural estimator\\ of $p$ is $\hat{p} = (x+y)/(m+n)$. Test statistic is
		\[ z = \frac{\hat{p}_1 - \hat{p}_2}{\sqrt{\hat{p}(1 - \hat{p})(1/m + 1/n)}} \]
		Rejection regions are the usual.
	\end{minipage}
\end{contentBox}

\subsubsection{Comparing Variance}
\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Test Differences Between Population Variances}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
		Assumptions:
		\begin{itemize}
			\item $X_1, X_2, \dots, X_n$ are IID, $N(\mu_1, \sigma_1)$
			\item $Y_1, Y_2, \dots, Y_n$ are IID, $N(\mu_2, \sigma_2)$
			\item $X_i$ and $Y_j$ are independent $\forall \,\, i, j$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		Let $s_1^2$, $s_2^2$ be sample variances, Then
		\[ F = \frac{s_1^2/\sigma_1^2}{s_2^2/\sigma_2^2} \sim F_{m-1, n-1} \]
	\end{minipage}
	
	\begin{minipage}[t]{0.35\textwidth}
		\[ \text{CI: } \left( \frac{s_1^2/s_2^2}{F_{\alpha/2, m-1, n-1}}, \frac{s_1^2/s_2^2}{F_{1-\alpha/2, m-1, n-1}} \right) \]
	\end{minipage}
	\begin{minipage}[t]{0.65\textwidth}
		Test: $H_0: \sigma_1^2 = \sigma_2^2 \to f = s_1^2/s_2^2$
		\begin{align*}
			H_a: \sigma_1^2 > \sigma_2^2, \,\, &\text{reject } H_0 \text{ if } f \geq F_{\alpha, m-1, n-1}\\
			H_a: \sigma_1^2 < \sigma_2^2, \,\, &\text{reject } H_0 \text{ if } f \leq F_{1-\alpha, m-1, n-1}\\
			H_a: \sigma_1^2 \neq \sigma_2^2, \,\, &\text{reject } H_0 \text{ if } f \geq F_{\alpha/2, m-1, n-1} \text{ or } f \leq F_{1-\alpha/2, m-1, n-1}\\
		\end{align*}
	\end{minipage}
\end{contentBox}