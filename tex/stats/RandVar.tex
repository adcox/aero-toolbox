%!TEX root=../../Aero_Toolbox.tex
\section{Random Variables}
\begin{footnotesize}
	Maps events to a 1-D real space: $X: \epsilon \to \mathbb{R}^1$.
	\begin{itemize}
		\item[-]Assigns a value to an event; e.g. $X(\text{heads}) = 1, X(\text{tails}) = 0$ or $X$ = sum of two rolled dice
	\end{itemize}
\end{footnotesize}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Discrete Random Variables}\end{center}
	R.V. have discrete values (e.g. sum of dice, number of visitors)
	\begin{itemize}
		\item Probability Mass Function (PMF): $p(x) = P(X = x) \in [0,1]$
		\item Cumulative Distribution Function (CDF): $F_x(x) = P(X \leq x) = \sum\limits_{t\leq x} p(t)$
		\begin{itemize}
			\item[$\to$] Properties: ($i$) increasing, ($ii$) continuous on the right, ($iii$) $\lim\limits_{x\to\infty} F_x(x) = 1$, ($iv$) $\lim\limits_{x\to-\infty}F_x(x) = 0$
		\end{itemize}
		\item Expected Value: $E(X) = \mu_x = \sum x p(x)$
		\begin{itemize}
			\item[$\to$]of a function of a R.V: $E(h(X)) = \sum h(x)p(x)$
			\item[$\to$]Linear relationships: $E(kX + b) = kE(X) + b$ (holds for continuous too)
		\end{itemize}
		\item Variance: $V(X) = \sigma^2 =  E\left[ (X - \mu_x)^2 \right] = \sum (x - \mu)^2 p(x)$
		\begin{itemize}
			\item[$\to$]Shortcut formula: $\sigma^2 = E(X^2) - \left[E(X)\right]^2$ (holds for continuous too)
			\item[$\to$]Linear relationships: $V(kX + b) = k^2V(X)$ (holds for continuous too)
		\end{itemize}
	\end{itemize}
\end{contentBox}

\pagebreak
\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Continuous Random Variables}\end{center}
	R.V. can take on continuous values (e.g. body weight, position in space, time between arrivals)
	\begin{itemize}
		\item Probability Density Function (PDF): $f_x(x) = \dod{}{x}\left(F_x(x)\right)$
		\begin{itemize}
			\item[$\to$] $f_x(x) \geq 0$
			\item[$\to$] $\int\limits_{-\infty}^{\infty} f_x(x) \dif x = 1$
		\end{itemize}
		\item Cumulative Distribution Function: $F_x(x) = P(X \leq x) = \int\limits_{-\infty}^x f_x(\xi) \dif\xi$
		\begin{itemize}
			\item[$\to$] $0 \leq F_x(x) \leq 1 \, \forall x$
			\item[$\to$] $F_x(-\infty) = 0$, $F_x(\infty) = 1$
			\item[$\to$] Non-decreasing: $F_x(x_2) - F_x(x_1) = P(x_1 \leq X \leq x_2) > 0$
			\item[$\to$] $F_x(x)$ is continuous on the right: $\lim\limits_{x\to x_0^+}F_x(x) = F_x(x_0)$
		\end{itemize}
		\item Expected Value: $E(X) = \mu_x = \int\limits_{-\infty}^{\infty}x f_x(x) \dif x$
		\begin{itemize}
			\item[$\to$] If $h$ is a function of $X$, then $E(h(X)) = \int\limits_{-\infty}^{\infty}h(x) f_x(x) \dif x$
		\end{itemize}
		\item Variance: $V(X) = \sigma^2 = \int\limits_{-\infty}^{\infty}(x - \mu_x)^2 f_x(x) \dif x = E(X^2) - \mu_x^2 = \int\limits_{-\infty}^{\infty} x^2 f_x(x) \dif x - \mu_x^2$
		\item Median $\nu$: $\frac{1}{2} = \int\limits_{-\infty}^{\nu}f_x(x) \dif x$
		\begin{itemize}
			\item[$\to$] Median is not necessarily unique (can be a range of values). E.g. if $f_x(x)$ is uniformly distributed between [1,2] and [3,4], the median can be anywhere between 2 and 3.
		\end{itemize}
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Algebra of Random Variables}\end{center}
	Let $X$, $Y$, and $Z$ by random variables\\
	\begin{center}
	\begin{tabular}{l l l}
		Operation & Expected Value & Variance\\
		\hline
		$Z = X + Y = Y + X$ 		& $E(Z) = E(X) + E(Y)$ 		& $V(Z) = V(X) + 2Cov(X,Y) + V(Y)$\\
		$Z = X - Y = -Y + X$ 		& $E(Z) = E(X) - E(Y)$ 		& $V(Z) = V(X) - 2Cov(X,Y) + V(Y)$\\
		$Z = XY = YX$ 				& $E(Z) = E(XY) = E(YX)$ 	& $V(Z) = V(XY)$\\
		$Z = X/Y = X(1/Y) = (1/Y)X$ & $E(Z) = E(X/Y)$ 			& $V(Z) = V(X/Y)$\\
		$Z = X^Y = e^{Y \ln(X)}$ 	& $E(Z) = E(X^Y)$ 			& $V(Z) = V(X^Y)$
	\end{tabular}
	\end{center}

	If $X$ or $Y$ is replaced by a deterministic variable or constant value, $k$, the previous properties remain valid because $P(X = k) = 1$, $E(X) = k$, $V(X) = 0$, and $Cov(Y,k) = 0$. If $X$ and $Y$ are \textbf{independent}, some special properties apply:\\
	\begin{center}
	\begin{tabular}{l l l}
		Operation & Expected Value & Variance\\
		\hline
		$Z = X + Y$ & 						& $V(X) + V(Y)$\\
		$Z = X - Y$ & 						& $V(X) + V(Y)$\\
		$Z = XY$ 	& $E(Z) = E(X)E(Y)$ 	& $V(X)V(Y) + V(X)E(Y)^2 + V(Y)E(X)^2$\\
		$Z = X/Y$ 	& $E(Z) = E(X)E(1/Y)$ 	& $V(X)V(1/Y) + V(X)E(1/Y)^2 + V(1/Y)E(X)$\\
		$Z = X^Y$ 	& 						& 
	\end{tabular}
	\end{center}

	Covariance properties:\\
	\begin{center}
	\begin{tabular}{l l l}
		Operation & General & Independent\\
		\hline
		$Z = X + Y$ & $Cov(Z, X) = V(X) + Cov(X,Y)$ 		& $Cov(Z, X) = V(X)$\\
		$Z = X - Y$ & $Cov(Z, X) = V(X) - Cov(X,Y)$ 		& $Cov(Z, X) = V(X)$\\
		$Z = XY$ 	& $Cov(Z, X) = E(X^2 Y) - E(XY)E(X)$ 	& $Cov(Z, X) = V(X) E(Y)$\\
		$Z = X/Y$ 	& $Cov(Z, X) = E(X^2/Y) - E(X/Y)E(X)$ 	& $Cov(Z, X) = V(X) E(1/Y)$\\
		$Z = Y/X$ 	& $Cov(Z, X) = E(Y) - E(Y/X)E(X)$ 		& $Cov(Z, X) = E(Y)[1 - E(X)E(1/X)]$\\
		$Z = X^Y$ 	& $Cov(Z, X) = E(X^{Y+1}) - E(X^Y)E(X)$ & \\
		$Z = Y^X$ 	& $Cov(Z, X) = E(XY^X) - E(Y^X)E(X)$
	\end{tabular}
	\end{center}
\end{contentBox}