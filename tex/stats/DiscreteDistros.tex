\section{Distributions of Discrete Random Variables}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Bernoulli Distribution}\end{center}\vspace{-5mm}
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{itemize}
			\item Two possible outcomes: success/fail
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{align*}
			X &= \text{\# successes (0 or 1)}\\
			P(X = x) &= p^x(1 - p)^{1-x},\quad p = P(\text{success})\\
			E(X) &= p\\
			V(X) &= p(1-p)
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Binomial Distribution}\end{center}\vspace{-5mm}
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{itemize}
			\item $n$ independent trials
			\item Each trial is Bernoulli (two outcomes)
			\item $p= P($success$)$ is constant $\forall$ trials
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{align*}
			X &= \text{\# successes}\\
			P(X = x) &= {n \choose x}p^x(1 - p)^{n-x}\\
			E(X) &= np\\
			V(X) &= np(1-p)
		\end{align*}
	\end{minipage}
	
	\begin{minipage}{0.95\textwidth}
		Approximations:
		\begin{itemize}
			\item If $n > 50$, $np < 5$, Binomial can be approximated by Poisson with $\lambda = np$
			\item If $np \geq 10$, $n(1-p) \geq 10$, and Binomial prob. histogram isn't too skewed, then Binomial can be approximated by Normal($\mu = np$, $\sigma^2 = np(1-p)$). Must apply ``continuity correction'' by extending/contracting bounds by $\pm0.5$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\newpage
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Hypergeometric Distribution}\end{center}\vspace{-3mm}
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{itemize}
			\item Population of $N$ events; each is Bernoulli
			\item Total of $M$ successes out of $N$ events
			\item Sample $n$ from population without replacement; each subset $n$ of $N$ is equally likely to be chosen
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{align*}
			X &= \text{\# successes in } n\\
			P(X = x) &= \frac{{M \choose x} {{N-M} \choose {n-x}}}{{N \choose n}}
			E(X) &= np, \quad p = \frac{M}{N}\\
			V(X) &= \frac{N-n}{N-1}np(1-p)
		\end{align*}
	\end{minipage}
	
	\begin{minipage}{\textwidth}
		Approximations:
		\begin{itemize}
			\item If $n/N \leq 0.05$, we can approximate with Binomial($n$, $p = M/N$)
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Negative Binomial Distribution}\end{center}\vspace{-3mm}
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{itemize}
			\item Sequence of $n$ independent trials
			\item Each trial is Bernoulli (two outcomes)
			\item $p= P($success$)$ is constant $\forall$ trials
			\item Continue until $r$ successes observed
			\item[$\star$] If $r=1$, $X$ follows a ``geometric distribution'' where $X$ is the \# failures before the first success
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{align*}
			X &= \text{\# failures that preceed } r^{th} \text{ success}\\
			P(X = x) &= {{x + r - 1} \choose {r-1}}p^r(1 - p)^x\\
			E(X) &= \frac{r(1-p)}{p}\\
			V(X) &= \frac{r(1-p)}{p^2}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Poisson Distribution}\end{center}\vspace{-3mm}
	\begin{minipage}[t!]{0.4\textwidth}
		\begin{itemize}
			\item Characterize \# occurrences in a time interval
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.6\textwidth}
		\begin{align*}
			X &= \text{\# occurences in time interval}\\
			\lambda &= \text{Avg. \# occurrences in time interval (always } >0)\\
			P(X = x) &= \frac{e^{-\lambda} \lambda^x}{x!}\\
			E(X) &= \lambda\\
			V(X) &= \lambda
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Poisson Process}\end{center}\vspace{-3mm}
	\begin{itemize}
		\item Counting process for \# events that have occurred up to a particular time
		\item Events occur randomly and homogeneously in continuous time at avg. rate of $\lambda$/unit time
	\end{itemize}
	\begin{minipage}[t!]{0.5\textwidth}
		Let $N(t)$ be \# successes by time $t$ ($t_0 = 0$)
		\begin{itemize}
			\item[$\to$]$N(0) = 0$
			\item[$\to$]$\{N(t), t\geq 0\}$ has independent increments if the increments don't overlap
			\item[$\to$] \# successes in interval $(t_1, t_2]$ follows Poisson distribution w/ parameter $\lambda*(t_2 - t_1)$, $0\leq t_1 \leq t_2 < \infty$
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.5\textwidth}
		Let the ``waiting time'' $W_n$ denote time of occurrence of $n$th event and let the ``inter-arrival time'' $I_n$ be the elapsed time between the $(n-1)$st and $n$th event.
		\begin{itemize}
			\item[$\to$]$I_n = W_n - W_{n-1}$
			\item[$\to$]$W_n = \sum\limits_{j=1}^n I_j$
			\item[$\to$]$I_n$'s are independently and identically distributed w/ exponential R.V. w/ parameter $\lambda$ $\forall$ $n$
			\item[$\to$]If we know arrivals/interval, arrival times are uniformly distributed
		\end{itemize}
	\end{minipage}
\end{contentBox}