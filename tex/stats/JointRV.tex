\section{Jointly Distributed Random Variables}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Continuous Jointly-Distributed R.V.}\end{center}
	\begin{minipage}[t]{0.3\textwidth}
		\begin{itemize}
			\item Probability Density Function: $f_{xy}(x,y)$
			\begin{itemize}
				\item[$\to$]$f_{xy}(x,y) \geq 0$
				\item[$\to$] $\iint\limits_{-\infty}^{\infty} f_{xy}(x,y) \dif x \dif y = 1$
			\end{itemize}
			Marginal PDF from joint PDF:
			\begin{itemize}
				\item[$\to$] $f_x(x) = \int\limits_{-\infty}^{\infty} f_{xy}(x,y) \dif y$
				\item[$\to$] $f_y(y) = \int\limits_{-\infty}^{\infty} f_{xy}(x,y) \dif x$
			\end{itemize}
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.65\textwidth}
		\begin{itemize}
			\item Cumulative Distribution Function: $F_{xy}(x,y) = P(X \leq x,\, Y \leq y)$
			\begin{itemize}
				\item[$\to$] $0 \leq F_{xy}(x,y) \leq 1$
				\item[$\to$] $F_{xy}(-\infty,y) = F_{xy}(x, -\infty) = 0$
				\item[$\to$] $F_{xy}(x, \infty) = F_x(x),\quad F_{xy}(\infty, y) = F_y(y)$
			\end{itemize}
			\begin{align*}
				&P \left( [x_1 \leq X \leq x_2] \cap [y_1 \leq Y \leq y_2] \right) = \eval{\eval{F_{xy}(x,y)}_{x_1}^{x_2}}_{y_1}^{y_2}\\
				&\quad = F_{xy}(x_2, y_2) - F_{xy}(x_1, y_2) - F_{xy}(x_2, y_1) + F_{xy}(x_1, y_1)
			\end{align*}
		\end{itemize}
	\end{minipage}
	\vspace{3mm}
	\begin{itemize}
		\item If $X$ and $Y$ are independent, then $f_{xy}(x,y) = f_x(x) f_y(y)$
		\item Relating CDF and PDF: $f_{xy}(x,y) = \dmd{}{2}{x}{}{y}{}F_{xy}(x,y)$, $F_{xy}(x,y) = \int\limits_{-\infty}^{x} \int\limits_{-\infty}^{y} f_{xy}(x,y) \dif y \dif x$		
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Discrete Jointly Distributed R.V.}\end{center}
	\begin{itemize}
		\item Probability Mass Function: $p(x,y) = P(X = x,\, Y = y)$
		\item $P( (x,y) \in A) = \sum\limits_{(x,y)} \sum\limits_{\in A} p(x,y)$
		\item Marginal PMF: $p_x(x) = \sum\limits_{y} p(x,y)$, $p_y(y) = \sum\limits_{x} p(x,y)$
		\item $X$ and $Y$ are independent iff $\forall$ $(x,y)$, $p(x,y) = p_x(x) p_y(y)$
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Conditional Probability}\end{center}\vspace{-3mm}
		Discrete: $P_{x\given y}(x \given y) = \frac{p(x,y)}{p_y(y)}$\newline
		Continuous: $f_{x\given y}(x \given y) = \frac{f(x,y)}{f_y(y)}$
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Expected Value}\end{center}\vspace{-3mm}
		\[ E\left[ h(X,Y) \right] = \left\{
			\begin{array}{l}
				\sum\limits_x \sum\limits_y h(x,y) p(x,y)\\
				\int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} h(x,y) f_{xy}(x,y) \dif y \dif x
			\end{array}\right.
		\]
		If $X$ and $Y$ are independent: $E[X,Y] = E[X]E[Y]$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Covariance}\end{center}\vspace{-3mm}
		\begin{align*}
			\text{Cov}(X,Y) &= E\left[ (X - \mu_x)(Y - \mu_y) \right]\\
			&= \left\{
			\begin{array}{c}
				\sum\limits_x \sum\limits_y (x -\mu_x)(y - \mu_y)p(x,y)\\
				\int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} (x - \mu_x)(y - \mu_y)f_{xy}(x,y) \dif y \dif x
			\end{array}\right.\\
			&= E(XY) - \mu_x \mu_y
		\end{align*}
		Note: Cov($X,X$) = $V(X)$\\
		\noindent If $X$ and $Y$ are independent: $\text{Cov}(X,Y) = 0$.
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Correlation Coefficient}\end{center}
		\[ \text{Corr}(X,Y) = \rho_{xy} = \frac{\text{Cov}(X,Y)}{\sigma_x \sigma_y},\quad -1 \leq \rho_{xy} \leq 1 \]
		\begin{itemize}
			\item $X,Y$ independent $\to \, \rho = 0$ (\textit{one-way} implication only)
			\item $|\rho| \geq 0.8 \to$ ``strong'', $0.5 \leq |\rho| < 0.8 \to$ ``medium'', $|\rho| < 0.5 \to$ ``weak''
			\item Corr($aX + b,\, cY + d) = \pm\text{Corr}(X,Y)$, + iff sgn($a$) = sgn($b$)
			\item $\rho = \pm 1$ iff $Y = aX + b$ for some $a,b$ with $a \neq = 0$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{More Than Two R.V.}\end{center}
	If $X_1, X_2,\dots,X_n$ are all discrete R.V., the joint pmf of the variables is
	\[ p(x_1, x_2,\dots,x_n) = P(X_1 = x_1, X_2 = x_2,\dots,X_n = x_n) \]

	If $X_1, X_2,\dots,X_n$ are all continuous R.V., the joint pdf is the function $f(x_1,x_2,\dots,x_n)$ such that for any $n$ intervals $[a_1,b_1],\dots,[a_n,b_n]$
	\[ P(a_1 \leq X_1 \leq b_1,\dots, a_n \leq X_n \leq b_n) = \int\limits_{a_1}^{b_1} \dots \int\limits_{a_n}^{b_n} f(x_1,\dots,x_n) \dif x_n \dots \dif x_1 \]
	
	The R.V.s are said to be independent if for \textit{every} subset $X_{i_1}, X_{i_2},\dots,X_{i_k}$ of the variables (each pair, triple, etc.), the joint pmf or pdf of the subset is equal to the product of the marginal pmf's or pdf's.
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Linear Combinations of R.V.}\end{center}
	Given $y = a_1 X_1 + a_2 X_2 + \dots + a_n X_n = \sum\limits_{i=1}^n a_i X_i$, where $X_i$ has mean $\mu_i$ and variance $\sigma_i^2$. Then
	
	\begin{minipage}[t]{0.4\textwidth}
		\begin{itemize}
			\item Whether or not the $X_i$'s are independent: 
			\begin{itemize}		
				\item[$\to$] $E(Y) = a_1 \mu_1 + \dots + a_n \mu_n$
				\item[$\to$] $V(Y) = \sum\limits_{i=1}^n \sum\limits_{j=1}^n a_i a_j \text{Cov}(X_i, X_j)$
			\end{itemize}
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{itemize}
			\item If $X_1,\dots,X_n$ are independent: $V(Y) = a_1^2 \sigma_1^2 + \dots + a_n^2 \sigma_n^2$
		\end{itemize}
	\end{minipage}
	
	\vspace{3mm}
	Special case: $X_1,\dots,X_n$ are independent, normally distributed, then $Y$ follows a normal distribution.
\end{contentBox}