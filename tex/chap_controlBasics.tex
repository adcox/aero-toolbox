\chapter{Controllers}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Strategies}\end{center}
	\begin{itemize}
		\item \textit{Change the Poles}: The poles of the transfer function describe the response of the system to an input; control seeks to change the poles to achieve a desired response.
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Open-Loop Control}\end{center}
		Control inputs are independent of system state; e.g., microwave, timed washing machine.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Closed-Loop Control}\end{center}
		Control inputs depend on system state or output; e.g., HVAC, autopilot, cruise control. 
	\end{minipage}
	
\end{contentBox}

\section{Linear System Control}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Linear State Feedback Controller}\end{center}
	Consider an open-loop (OL) state-space system $\dot{\vec{x}} = \coxmat{A} \vec{x} + \coxmat{B} \vec{u}$. We design a set of gains $\coxmat{K}$ such that $(\coxmat{A} - \coxmat{BK})$ has stable eigenvalues and implement linear feedback $\vec{u} = - \coxmat{K}\vec{x}$.
	
	\includegraphics[width=0.37\textwidth]{LinearFeedbackDiagram.pdf}
	\includegraphics[width=0.57\textwidth]{LinearFeedbackDescision.pdf}
	
	\begin{itemize}
		\item System is \textit{stabilizable} if $\exists \eval[2]{\coxmat{K}} \coxmat{A} - \coxmat{BK}$ is asymptotically stable
		\item Controllability of this system is sufficient for stabilizability, but not necessary
		\item If $\coxmat{A} - \coxmat{BK}$ has uncontrollable eigenvalues, they must be stable for the system to be stabilizable
	\end{itemize}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{State Estimator}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		Consider a system
		\begin{align*}
			\dot{\vec{x}} &= \coxmat{A} \vec{x} + \coxmat{B} \vec{u}\\
			\vec{y} &= \coxmat{C} \vec{x} + \coxmat{D} \vec{u}
		\end{align*}
		with $n$ states, $m$ inputs, and $p$ outputs
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		A state estimator for this system has the form
		\begin{align*}
			\dot{\hat{x}} &= \coxmat{A} \hat{x} + \coxmat{B} \vec{u} + \coxmat{L}(\hat{y} - \vec{y})\\
			\hat{y} &= \coxmat{C} \hat{x} + \coxmat{D} \vec{u}
		\end{align*}
		where $\hat{x}$ is the estimated state vector, $\coxmat{L}$ is the observer gain matrix ($n \times p$).
	\end{minipage}
	
	We can rewrite $\dot{\hat{x}} = (\coxmat{A} - \coxmat{LC}) \hat{x} + (\coxmat{B} + \coxmat{LD}) \vec{u} - \coxmat{L} \vec{y}$. Goal is for $\hat{x} \approx \vec{x}$. To minimize error, choose $\coxmat{L}$ s.t. $\coxmat{A} + \coxmat{LC}$ is a stable matrix.
	
	\begin{center}\textbf{Detectibility}\end{center}
	\begin{itemize}
		\item The state is \textit{detectible} iff $\coxmat{A} + \coxmat{LC}$ can be made asymptotically stable
 		\item If the system described by $\coxmat{A}$ and $\coxmat{C}$ is observable, it is also detectible.
 		\item If $\lambda$ is an unobservable eigenvalue of $(\coxmat{C},\, \coxmat{A})$ then $\lambda$ is an eigenvalue of $\coxmat{A} + \coxmat{LC} \, \forall\, \coxmat{L}$
 	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Output Feedback Controller}\end{center}
		Feedback law $\vec{u} = \coxmat{K} \vec{y}$ results in following closed-loop system: $\dot{\vec{x}} = (\coxmat{A} + \coxmat{BKC}) \vec{x}$.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Deadbeat Controller}\end{center}
		\begin{center}Make all eigenvalues zero.\end{center}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Observer-Based Controllers}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{align*}
			\dot{\hat{x}} &= \coxmat{A} \hat{x} + \coxmat{B} \vec{u} + \coxmat{L}(\coxmat{C}\hat{x} + \coxmat{D} \vec{u} - \vec{y})\\
			&= (\coxmat{A} + \coxmat{BK} + \coxmat{LC})\hat{x} - \coxmat{L}\vec{y}\\
			\vec{u} &= \coxmat{K}\hat{x}
		\end{align*}
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item Need to have $\coxmat{A} + \coxmat{BK}$ asymptotically stable (stabilizable; i.e., $(\coxmat{A},\coxmat{B})$ controllable)
			\item Need to have $\coxmat{A} + \coxmat{LC}$ asymptotically stable (detectible; i.e., $(\coxmat{A},\coxmat{C})$ observable)
		\end{itemize}
		
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Optimal Feedback Controller - Linear Quadratic Regulator (LQR)}\end{center}
	\begin{itemize}
		\item Optimizes cost function $J(\vec{x}_0, \vec{u}) = \int\limits_0^{\infty} \left( \vec{x}^T(t) \coxmat{Q} \vec{x}(t) + \vec{u}^T(t) \coxmat{R} \vec{u}(t) \right) \dif t$
		\item Optimal controller given by $\vec{u}^*(t) = -\coxmat{R}^{-1} \coxmat{B}^T \coxmat{P} \vec{x}(t)$ where $\coxmat{P} > 0$ satisfies the algebraic Riccati equation:
		\[\coxmat{PA} + \coxmat{A}^T \coxmat{P} - \coxmat{PBR}^{-1} \coxmat{B}^T \coxmat{P} + \coxmat{Q} = 0\]
		\item The stabilizing solution has a hermitian $\coxmat{P}$ with the property that the matrix $\coxmat{A} - \coxmat{BR}^{-1}\coxmat{B}^T \coxmat{P}$ is asymptotically stable.
	\end{itemize}
\end{contentBox}