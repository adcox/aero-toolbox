%!TEX root=../Aero_Toolbox.tex

% Stochastic Processes: AAE 567
\chapter{Stochastic Processes}


\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Discrete Time Kalman Filter}\end{center}
	\begin{minipage}[t!]{0.35\textwidth}
		\begin{align*}
			x(n+1) &= Ax(n) + Bu(n)\\
			y(n) &= Cx(n) + Dv(n)
		\end{align*}
	\end{minipage}%
	\hspace{0.025\textwidth}
	\begin{minipage}[t]{0.5\textwidth}
		Assume $u(n)$, $v(n)$ independent, white noise\\
		$x(0) \perp u(n) \perp v(m)\, \forall\, n,m$, $x(0)$ is a R.V.
	\end{minipage}
	
	\begin{align*}
		\hat{x}(n+1) &= A\hat{x}(n) + AQ_n C^* (CQ_nC^* + DD^*)^{-1} (y(n) - C\hat{x}(n))\\
		&= (A - L_n)\hat{x}(n) + L_ny(n),\quad L_n = AQ_nC^*(CQ_nC^* + DD^*)^{-1}\\
		Q_{n+1} &= AQ_nA^* + BB^* - AQ_nC^* (CQ_nC^* + DD^*)^{-1} CQ_nA^*
	\end{align*}
	Steady State: $P = APA^* + BB^* - APC^*(CPC^* + DD^*)^{-1}CPA^*$, $P > 0$ (iff $\{A,B,C\}$ controllable and observable)\\
	Adding a forcing function $w(n)$ (e.g. state feedback) ($Q_n$, $L_n$ unchanged):
	\begin{align*}
		x(n+1) &= Ax(n) + Bu(n) + B_1 w(n)\\
		y(n) &= Cx(n) + Dv(n)\\
		\hat{x}(n+1) &= (A-L_n C)\hat{x}(n) + L_n y(n) + B_1 w(n)
	\end{align*}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Continuous Time Kalman Filter}\end{center}
	\begin{minipage}[t!]{0.35\textwidth}
		\begin{align*}
			\dot{x} &= Ax + Bu\\
			y &= Cx + Dv
		\end{align*}
	\end{minipage}%
	\hspace{0.025\textwidth}
	\begin{minipage}[t]{0.5\textwidth}
		Assume $u(t)$, $v(t)$ independent, white noise\\
		$x(0) \perp u(t_1) \perp v(t_2)\, \forall\, t_1, t_2$, $x(0)$ is a R.V.
	\end{minipage}
	
	\begin{align*}
		\dot{\hat{x}} &= A\hat{x} + Q C^* (DD^*)^{-1} (y(n) - C\hat{x})\\
		&= (A - QC^*(DD^*)^{-1}C)\hat{x} + QC^*(DD^*)^{-1}y\\
		\dot{Q} &= AQ + QA^* + BB^* - QC^*(DD^*)^{-1}CQ
	\end{align*}
	Steady State: $\dot{Q}=0 \to 0 = AP + PA^* +BB^* - PC^*(DD^*)^{-1}CP$ ``Algebraic Riccati Equation''.\\In steady state, $P(t) = \lim\limits_{t\to\infty} Q(t)$\\
	Conditions:
	\begin{enumerate}
		\item $Q(0) = 0$
		\item $\{C,A\}$ observable $\to Q(t) \leq \gamma I < \infty$ (bounded, limit exists)
		\item $\{A,B,C\}$ controllable, observable $\to P > 0$ and is unique positive solution to ARE. Also implies $A - PC(DD^*)^{-1}$ is stable.
	\end{enumerate}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}{\textwidth}
	\begin{center}\textbf{Batch Algorithm}\end{center}

	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\item Gather a priori information and static quantities
			\vspace{-1em}
			\begin{align*}
				\coxmat{P_0}^- \quad & \text{A priori epoch state covariance}\\
				\vec{q}_0^- \quad & \text{A priori epoch state}\\
				\vec{x}_0^- \quad & \text{A priori epoch state variations}\\
				\vec{y}_i \quad & \text{Observations at time } t_i\\
				\coxmat{R} \quad & \text{Observation covariance}
			\end{align*}
			\item Initialize the process:
			\vspace{-1em}
			\begin{align*}
				\vec{x}_0 &= \vec{x}_0^- \qquad \text{(Epoch State Variations)}\\
				\hat{\vec{q}}_0 &= \vec{q}_0 \qquad \text{(Epoch State Estimate)}\\
				\coxmat{\Lambda} &= \left( \coxmat{P}_0^- \right)^{-1} \qquad \text{(Information Matrix)}\\
				\coxmat{N} &= \coxmat{\Lambda} \vec{x}_0
			\end{align*}
			If no a priori information, set $\coxmat{\Lambda} = \coxmat{N} = 0$
			\item For each set of observations $\vec{y}_i$ at time $t_i$ with covariances $R_i$:
			\label{step:batchIterateOverObs}		
			\begin{enumerate}
				\item Compute a reference solution from the epoch state estimate \[\hat{\vec{q}} = \vec{f}(\hat{\vec{q}}_0)\]
				\item Compute observations from the estimated solution \[\hat{\vec{y}}_i = \vec{g}(t_i, \hat{\vec{q}})\]
				\item Compute observation variations \[ \vec{z}_i = \vec{y}_i - \hat{\vec{y}}_i \]
			\end{enumerate}
		\end{enumerate}
	\end{minipage}%
	\hspace{\columnsep}%
	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\item[] Step \ref{step:batchIterateOverObs}, continued:
			\begin{enumerate}
				\setcounter{enumii}{3}
				\item Construct observation-state relationship matrix and STM
				\vspace{-1em}
				\begin{align*}
					\coxmat{\tilde{H}}_i &= \dpd{\vec{y}_i}{\vec{q}}\\
					\coxmat{H}_i &= \coxmat{\tilde{H}_i \phi}(t_i, t_0)
				\end{align*}
				\item Accumulate matrices
				\vspace{-1em}
				\begin{align*}
					\coxmat{\Lambda} &= \coxmat{\Lambda} + \coxmat{H}_i^T \coxmat{R}_i^{-1} \coxmat{H}_i\\
					\coxmat{N} &= \coxmat{N} + \coxmat{H}_i^T \coxmat{R}_i^{-1} \vec{z}_i
				\end{align*}
			\end{enumerate}
			\setcounter{enumi}{3}
			\item Solve the normal equations \[ \hat{\vec{x}}_0 = \coxmat{\Lambda}^{-1} \coxmat{N} \] The covariance is $\coxmat{P_0} = \coxmat{\Lambda}^{-1}$
			\item If the process has converged, exit! Otherwise, proceed to the next step.
			\item Begin a new iteration
			\begin{enumerate}
				\item Update the reference solution \[ \hat{\vec{q}}_0 = \hat{\vec{q}}_0 + \hat{\vec{x}}_0 \]
				\item Shift $\vec{x}_0$ with respect to the updated $\hat{\vec{q}}_0$ as follows: \[ \vec{x}_0 = \vec{x}_0 - \hat{\vec{x}}_0 \]
				\item Use the original value of $\coxmat{P}_0$
				\item Reset $\coxmat{\Lambda} = \coxmat{P}_0^{-1}$
				\item Update $\coxmat{N} = \coxmat{P}_0^{-1} \vec{x}_0$
				\item Return to step \ref{step:batchIterateOverObs} with the updated values
			\end{enumerate}
		\end{enumerate}
	\end{minipage}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}{\textwidth}
	\begin{center}\textbf{Linear Sequential Estimator (a.k.a. Kalman Filter) Algorithm}\end{center}

	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\item Gather a priori information and static quantities
			\vspace{-1em}
			\begin{align*}
				\coxmat{P_0}^- \quad & \text{A priori epoch state covariance}\\
				\vec{q}_0 \quad & \text{Reference Epoch State}\\
				\vec{x}_0^- \quad & \text{A priori epoch state variations}\\
				y_i \quad & \text{Observation at time } t_i\\
				\coxmat{R} \quad & \text{Observation covariances}
			\end{align*}
			%
			\item Initialize process
			\vspace{-1em}
			\begin{align*}
				i &= 1\\
				\hat{\vec{x}}_{i-1} &= \hat{\vec{x}}_0^-\\
				\coxmat{P}_{i-1} &= \coxmat{P}_0^-
			\end{align*}
			\item Read next set of observations, $\vec{y}_i$, at time $t_i$ with covariances $\coxmat{R}_i$ \label{step:p17_kalman_next}
			%
			\item Propagate reference trajectory and state transition matrix from $t_{i-1}$ to $t_i$, yielding $\vec{q}(t_i)$, $\coxmat{\phi}(t_i, t_{i-1})$
			%
			\item Predict state variations:
			\[ \hat{\vec{x}}_i^- = \coxmat{\phi}(t_i, t_{i-1}) \hat{\vec{x}}_{i-1} \]
			%
			\item Predict state covariance matrix: 
			\[ \coxmat{P}_i^- = \coxmat{\phi}(t_i, t_{i-1}) \coxmat{P}_{i-1} \coxmat{\phi}(t_i, t_{i-1})^T \]
		\end{enumerate}
	\end{minipage}%
	\hspace{\columnsep}%
	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\setcounter{enumi}{6}
			\item Compute observations from reference solution
			\[ \hat{\vec{y}}_i = \vec{g}\left( t_i, \vec{q}(t_i) \right) \]
			%
			\item Compute observation variations
			\[ \vec{z}_i = \vec{y}_i - \hat{\vec{y}}_i \]
			%
			\item Compute observation-state relationship matrix
			\[ \coxmat{\tilde{H}}_i = \dpd{\vec{g} \left( t_i, \vec{q}(t_i) \right)}{\vec{q}} \]
			%
			\item Compute gain matrix
			\[ \coxmat{K}_i = \coxmat{P}_i \coxmat{\tilde{H}}_i^T \left( \coxmat{\tilde{H}}_i \coxmat{P}_i \coxmat{\tilde{H}}_i^T \right)^{-1} \]
			%
			\item Update state variation estimate and covariance
			\vspace{-1em}
			\begin{align*}
				\hat{\vec{x}}_i &= \hat{\vec{x}}_i^- + \coxmat{K}_i \left( \vec{z}_i - \coxmat{\tilde{H}}_i \hat{\vec{x}}_i^- \right)\\
				\coxmat{P}_i &= \left(\coxmat{I} - \coxmat{K}_i \coxmat{\tilde{H}}_i \right) \coxmat{P}_i^-
			\end{align*}
			\item If all observations have been read, exit. Otherwise, return to Step \ref{step:p17_kalman_next}
		\end{enumerate}
	\end{minipage}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{\textwidth}
	\begin{center}\textbf{Extended Kalman Filter Algorithm}\end{center}

	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\item Gather a priori information and static quantities
			\vspace{-1em}
			\begin{align*}
				\coxmat{P_0}^- \quad & \text{A priori epoch state covariance}\\
				\vec{q}_0 \quad & \text{Reference Epoch State}\\
				\vec{x}_0^- \quad & \text{A priori epoch state variations}\\
				y_i \quad & \text{Observation at time } t_i\\
				\coxmat{R} \quad & \text{Observation covariances}
			\end{align*}
			\item Initialize process
			\vspace{-1em}
			\begin{align*}
				i &= 1\\
				\vec{q}(t_{i-1}) &= \vec{q}_0\\
				\hat{\vec{x}}_{i-1} &= \hat{\vec{x}}_0^-\\
				\coxmat{P}_{i-1} &= \coxmat{P}_0^-
			\end{align*}
			\item Read next set of observations, $\vec{y}_i$, at time $t_i$ with covariances $\coxmat{R}_i$ \label{step:p21_ekf_next}
			\item Propagate reference trajectory and state transition matrix from $t_{i-1}$ to $t_i$ (initial state at $\vec{q}(t_{i-1})$), yielding $\vec{q}(t_i)$, $\coxmat{\phi}(t_i, t_{i-1})$
			\item Predict state covariance matrix: 
			\[ \coxmat{P}_i^- = \coxmat{\phi}(t_i, t_{i-1}) \coxmat{P}_{i-1} \coxmat{\phi}(t_i, t_{i-1})^T \]
		\end{enumerate}
	\end{minipage}%
	\hspace{\columnsep}%
	\begin{minipage}[t]{\dimexpr.5\textwidth-.5\columnsep}
		\begin{enumerate}
			\setcounter{enumi}{5}
			\item Compute observations from reference solution
			\[ \hat{\vec{y}}_i = \vec{g}\left( t_i, \vec{q}(t_i) \right) \]
			\item Compute observation variations
			\[ \vec{z}_i = \vec{y}_i - \hat{\vec{y}}_i \]
			\item Compute observation-state relationship matrix
			\[ \coxmat{\tilde{H}}_i = \dpd{\vec{g} \left( t_i, \vec{q}(t_i) \right)}{\vec{q}} \]
			\item Compute gain matrix
			\[ \coxmat{K}_i = \coxmat{P}_i \coxmat{\tilde{H}}_i^T \left( \coxmat{\tilde{H}}_i \coxmat{P}_i \coxmat{\tilde{H}}_i^T \right)^{-1} \]
			\item Update state variation estimate and covariance
			\vspace{-1em}
			\begin{align*}
				\hat{\vec{x}}_i &= \coxmat{K}_i \vec{z}_i\\
				\coxmat{P}_i &= \left(\coxmat{I} - \coxmat{K}_i \coxmat{\tilde{H}}_i \right) \coxmat{P}_i^-
			\end{align*}
			\item Update the reference solution
			\vspace{-1em}
			\begin{align*}
				\vec{q}(t_i) = \vec{q}(t_i) + \hat{\vec{x}}_i
			\end{align*}
			\item If all observations have been read, exit. Otherwise, return to Step \ref{step:p21_ekf_next}
		\end{enumerate}
	\end{minipage}
	\vspace{1em}
	\end{minipage}
\end{contentBox}