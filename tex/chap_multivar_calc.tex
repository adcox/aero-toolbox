\chapter{Multivariate Calculus}

\input{tex/calc/vectorBasics.tex}

\section{Geometric Definitions and Applications}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Domain}\end{center}
	A ``solution space'' of sorts. A domain $D$ is
	\begin{itemize}
		\item \textit{simply connected} if every closed curve in $D$ can be continuously shrunk to any point in $D$ without leaving $D$ (i.e., a rectangular prism domain is simply connected, but a torus is not)
	\end{itemize}
\end{contentBox}


\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Curve}\end{center}
	\begin{minipage}[t]{0.46\textwidth}
		A curve $C$ is
		\begin{itemize}
			\item \textit{smooth} if it has at each point a unique tangent whose direction varies continuously along $C$
			\item \textit{piecewise smooth} if it consists of finitely many smooth curves
			\item \textit{closed} if the origin and terminus points are collocated
		\end{itemize}				
		It is frequently useful to parameterize a curve as a function of one variable, i.e.,
		\[C: \vec{r}(\sigma) = \begin{Bmatrix} x_1(\sigma) & x_2(\sigma) & \dots & x_n(\sigma) \end{Bmatrix} \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item Tangent vector to $C$ is $\vec{t} = \dod{\vec{r}}{\sigma}$
			\item Arclength along $C$ is
			\[s(\sigma) = \int\limits_0^\sigma \sqrt{\vec{r}'(\tau) \dotp \vec{r}'(\tau)} \dif \tau \]
			\vspace{-1em}
			\item Normal vector to $C$ is $\vec{n} = \dod{\hat{t}}{s} = \dod{\hat{t}}{\sigma}\dod{\sigma}{s} = k\hat{n}$ where $k$ is the \textit{curvature} of $C$
			\item Binormal vector is $\vec{b} = \vec{t} \times \vec{n}$
			\item Torsion of $C$ is $\nu$ from $\dod{\hat{b}}{s} = \dod{\hat{b}}{\sigma}\dod{\sigma}{s} = \nu \hat{n}$
			\item Radius of curvature of $C$ is $\rho = 1/k$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Surface}\end{center}
	\begin{minipage}[t]{0.46\textwidth}
		A surface $S$ is
		\begin{itemize}
			\item \textit{smooth} if its surface normal depends continuously on the points of $S$
			\item \textit{piecewise smooth} if it consists of finitely many smooth portions (sphere is smooth, cube is piecewise smooth)
			\item A smooth surface is \textit{orientable} if the positive normal direction at arbitrary point $P$ on $S$ can be continued in a unique and continuous way over the entire surface (mobius strip is nonorientable)
			\item A piecewise smooth surface is \textit{orientable} on more stringent conditions; see Kreyszig 10.6.
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		It is frequently useful to parameterize a surface as a function of two variables, i.e.,
		\[S: \vec{r}(u,v) = \begin{Bmatrix} x_1(u,v) & x_2(u,v) & \dots & x_n(u,v) \end{Bmatrix} \]
		\begin{itemize}
			\item Tangent plane to $S$ is spanned by $\pd{\vec{r}}{u}$ and $\pd{\vec{r}}{v}$
			\item Normal vector to plane is along $\pd{\vec{r}}{u} \times \pd{\vec{r}}{v}$ assuming that $u$ and $v$ are independent. Also, if $S$ is represented by $g(x,y,z)=0$, then
			\[ \vec{n} = \grad g \]
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Spatial Plane}\end{center}
		Let a plane be defined by
		\[ a_1 x + a_2 y + a_3 z = c \]
		Then the vector $\vec{a} = \{a_1,\, a_2,\, a_3 \}$ is normal to the plane.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Spatial Surface}\end{center}
		Let a surface S be defined by $f(x,y,z) = c =$ const. If $\grad f \neq 0$ at point P, then $\grad f$ is normal to S at P.
	\end{minipage}
\end{contentBox}

\input{tex/calc/multivar_differentiation.tex}
\input{tex/calc/multivar_integration.tex}

%\begin{contentBox}
%	\footnotesize%
%	\begin{center}\textbf{Title}\end{center}
%	
%	\begin{minipage}[t]{0.45\textwidth}
%	
%	\end{minipage}
%	\begin{minipage}[t]{0.45\textwidth}
%	
%	\end{minipage}
%\end{contentBox}




\section{Misc - Put elsewhere?}
\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Solving PDEs with Laplace Transforms}\end{center}
	\index{Laplace Transform!multivariable PDEs}
	Solve a partial differential equation of multi-variable function $w(x, t)$
	
	\begin{minipage}[t]{0.4\textwidth}
		Strategy:
		\begin{itemize}
			\item Take $\Lapl$ w.r.t. one variable, usually $t$
			\item Solve resulting ODE of transformed function
			\item Invert Laplace transform to yield solution
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.5\textwidth}
		Useful derivatives:
		\begin{align*}
			\Lapl \left\{ \dpd{w(x,t)}{t} \right\} &= s W(x,s) - w(x,0)\quad \text{normal deriv. transform}\\
			\Lapl \left\{ \dpd{w(x,t)}{x} \right\} &= \dpd{W(x,s)}{x} \quad \text{deriv. of transform}
		\end{align*}
	\end{minipage}
\end{contentBox}