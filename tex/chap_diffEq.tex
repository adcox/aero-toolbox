% Differential Equations
\chapter{Ordinary Differential Equations}
\index{Ordinary Differential Equations}

\section{1st Order Scalar ODEs}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Solution Methods}\end{center}
	\index{Ordinary Differential Equations!Solutions!1st order}
	\begin{minipage}[t]{\textwidth}
		\begin{center}\textbf{Logs}\end{center}\vspace{-3mm}
		\[ \frac{\dif y / \dif t}{y} = k \quad \to \quad \dod{}{t} \ln(y) = k \, \to \text{integrate} \]
		or
		\[ \dot{y} = ky - b \quad \to \quad \dot{y} = k(y - \frac{b}{k}) \quad \to \quad \frac{\dif y/ \dif t}{y - \frac{b}{k}} = k \quad \to \quad \frac{b}{k} + (y_0 - \frac{b}{k})e^{kt} \]
	\end{minipage}
	
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Integrating Factors}\end{center}\vspace{-3mm}
		\begin{align*}
			\text{Given} \,\,& \dot{y} + py = g \qquad \text{Let} \,\, \mu = \exp \left( \int p \dif t \right)\\
			\text{Integrate} \,\, & \dod{}{t} (y \mu) = \mu g \quad \to \quad y(t) \mu(t) + c = \int \mu(\tau) g(\tau) \dif \tau \\
		\end{align*}
%		Given $\dot{y} + py = g$, let $\mu = \exp \left( \int p \dif t \right)$; \newline	
%		then $\dod{}{t}(y \mu) = \mu g \to$ integrate
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Separable}\end{center}\vspace{-3mm}
		\[ \dod{y}{x} = \frac{f(x)}{g(y)} \quad \to \quad g(y) \dif y = f(x) \dif x \quad \to \text{integrate} \]
		Take care to preserve integration constant for I.C.s, if applicable!
	\end{minipage}
\end{contentBox}

\section{$n$th Order ODEs}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Solution Strategies}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Ansatz, Homogeneous/Particular}\end{center}
		\begin{enumerate}
			\item Find a homogeneous solution by
			\begin{itemize}
				\item Writing the characteristic polynomial, finding eigenvalues, write sum of exponentials, expand via Euler's formula, etc.
				\item Find the eigenvalues and eigenvectors of the constant coefficient matrix, write the solution as a superposition of the eigenvectors scaled by constant coefficients and the exponential of the corresponding eigenvalue
			\end{itemize}
			\item Obtain a particular solution by
			\begin{itemize}
				\item Variation of Parameters
				\item Undetermined Coefficients
				\item Inspection (i.e., brains, luck, and intuition)
			\end{itemize}
			\item Sub solution into I.C.s and B.C.s to solve for coefficients.
		\end{enumerate}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Other Strategies}\end{center}
		\begin{itemize}
			\item Take the Laplace transform of the ODE, solve for the Laplace transform of the desired function, and then take the inverse Laplace transform; may plug in I.C.s initially or assume I.C.s are zero and solve for coefficients after solution form is obtained (may only work if system is homogeneous).
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{$n$th Order to 1st Order Vector Eq}\end{center}
	\index{Ordinary Differential Equations!System Representation}
	\begin{minipage}[t]{0.35\textwidth}
		\begin{align*}
			\text{Eq: } & a_n \dod[n]{y}{t} + \dots + a_1 \dod{y}{t} + a_0 y(t) = g(t)\\
			\text{Given } \dod[n]{y}{t} &= F \left( t,\, y,\, \dot{y},\, \dots,\, y^{(n)} \right)\\
			\text{Choose } y_1 &= y,\,\, y_2 = \dot{y},\,\, \dots,\,\, y_n = y^{(n-1)}
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.55\textwidth}
		\begin{align*}
			\dod{\vec{y}}{t} = \begin{Bmatrix}\dot{y}_1\\ \dot{y}_2\\ \vdots \\ \dot{y}_n\end{Bmatrix} = 
			\begin{bmatrix}
				0 & 1 & 0 & \dots & 0\\
				0 & 0 & 1 & & 0\\
				\vdots & & & \ddots & 0\\
				\frac{-a_0}{a_n} & \frac{-a_1}{a_n} & \frac{-a_2}{a_n} & \dots & \frac{-a_{n-1}}{a_n}
			\end{bmatrix}
			\begin{Bmatrix}y_1 \\ y_2 \\ \vdots \\ y_n \end{Bmatrix}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Scalar $nth$ Order Homogeneous System}\end{center}
	\index{Ordinary Differential Equations!Solutions!homogeneous,scalar}
	\[ g(t) = 0 \qquad \text{find } y_h(t)\]
	\begin{itemize}
		\item Try Ansatz $y(t) = e^{rt} \quad \to \quad \left(a_n r^n + a_{n-1}r^{n-1} + \dots + a_1 r + a_0 \right)e^{rt} = 0$.
		\item ``Characteristic Polynomial'' is $\sum\limits_{i=0}^n a_i r^i$
		\item Solution is linear combo of sol'ns $y(t) = c_1 f_1(t) + \dots + c_n f_n(t)$, set of $f_i$ form a basis for a vector space of solutions
		\begin{itemize}
			\item $r_i$ repeated $l$ times: $\left\{ f_i, \,\, \dots, \,\, f_{i+l}  \right\} = \left\{ e^{r_i t},\,\, te^{r_i t},\,\, \dots, \,\, t^{l-1}e^{r_i t} \right\}$
			\item $r_{i, i+1} = \alpha \pm i\beta$ generates solutions $f_{i, i+1} = e^{(\alpha \pm i\beta)t} \quad \to \quad e^{\alpha t} \cos \beta t \,\,, e^{\alpha t} \sin \beta t$
			\item $r \in \mathbb{C}$ repeated $l$ times: try $e^{\alpha t} \left\{ \cos \beta t,\,\, \sin \beta t,\,\, t\cos \beta t, \,\, t\sin \beta t,\,\, \dots, \,\, t^{l-1} \cos\beta t,\,\, t^{l-1} \sin \beta t \right\}$
		\end{itemize}
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Vector 1st Order Homogeneous Solution}\end{center}
	\index{Ordinary Differential Equations!Solutions!homogeneous, vector}
	\begin{minipage}[t]{0.45\textwidth}
		\[ \dod{\vec{y}}{t} = \coxmat{A} \vec{y} \]
		\begin{itemize}
			\item Constant coefficients in $\coxmat{A}$
			\item Eigenvalues $\lambda_1$, $\dots$, $\lambda_n$
			\item Corresponding eigenvectors $\vec{v}_1$, $\dots$, $\vec{v}_n$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		Solution form
		\[ \vec{y}(t) = c_1 e^{\lambda_1 t} \vec{v}_1 + \dots + c_n e^{\lambda_n t}\vec{v}_n \]
	\end{minipage}		
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Vector 1st Order Nonhomogeneous Solution}\end{center}
	\[ \vec{g}(t) \neq 0 \qquad \text{general solution: } \vec{y}(t) = \vec{y}_h(t) +\vec{y}_p(t) \]
	
	\begin{minipage}[t]{\textwidth}
		\begin{center}\textbf{Variation of Parameters}\end{center}
		\index{Ordinary Differential Equations!Solutions!variation of parameters}
		Form ``fundamental matrix'': $\coxmat{Y} = \begin{bmatrix} \vec{f}_1 & \dots & \vec{f}_n \end{bmatrix}$, then $\vec{y}_p = \coxmat{Y} \int\limits_0^t \coxmat{Y}^{-1}(\tau) \vec{g}(\tau) \dif \tau$
	\end{minipage}
	
	\begin{center}\textbf{Undetermined Coefficients}\end{center}
	\index{Ordinary Differential Equations!Solutions!undetermined coefficients}
	\begin{minipage}[t]{0.5\textwidth}		
		Choose $\vec{y}_p$ to have ``similar form'' to $\vec{g}(t)$:
		\begin{tabular}{c|c}
			Term in $g(t)$ & $y_p$ guess\\
			\hline
			$k e^{\gamma t}$ & $\vec{c} e^{\gamma t}$\\
			$k t^n$ & $\vec{k}_n t^n + \dots + \vec{k}_1 t + \vec{k}_0$\\
			$k\cos(\omega t)$, $k\sin(\omega t)$ & $\vec{a} \cos(\omega t) + \vec{b} \sin(\omega t)$\\
			$k e^{\alpha t} \cos(\omega t)$, $k e^{\alpha t} \sin(\omega t)$ & $e^{\alpha t} \left[ \vec{a} \cos(\omega t) + \vec{b} \sin(\omega t) \right]$ 
		\end{tabular}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.4\textwidth}
		If $\vec{g}(t)$ is already in the form of $\vec{y}_h(t)$, find additional sol'ns by multiplying by successive multiples of $t$. E.g.
		\[ y_h(t) = a e^t \quad g(t) = b e^t \quad \text{try } g(t) = c_1 e^t + c_2 t e^t \]
		If there are $n$ similar solution forms, must add $n$ powers of $t$	
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{2D Solution Subspace Characterizations}\end{center}
	\index{Ordinary Differential Equations!Solutions!characterization}
	Consider a subset of the solution space that includes $y_i$ and $y_j$ (i.e., constants chosen to isolate these solutions) with solutions characterized by the eigenvalues $\lambda_{i,j}$ and corresponding eigenvectors $\vec{v}_{i,j}$. The solution flow in ($y_i$, $y_j$) has several types:
	
	\begin{tabular}{llll}
	Type & Eigenvalues & Stability & Solution Form\\
	\hline
	Proper Node		& $\lambda_i = \lambda_j \in \mathbb{R}$ & Stable: $\lambda_{i,j} \leq 0$; Else unstable & $\vec{y} = c_i \vec{v}_i e^{\lambda t} + c_j \vec{v}_j e^{\lambda t}$\\
	\hline	
	Improper Node	& $\lambda_i \neq \lambda_j$, $\mathbb{R},$ same sign & Stable: $\lambda_i < \lambda_j \leq 0$; Else unstable & $\vec{y} = c_i \vec{v}_i e^{\lambda_i t} + c_j \vec{v}_j e^{\lambda_j t}$\\
	\hline	
	Saddle Point		& $\lambda_i < 0 < \lambda_j$, $\mathbb{R}$ & Unstable & $\vec{y} = c_i \vec{v}_i e^{\lambda_i t} + c_j \vec{v}_j e^{\lambda_j t}$\\
	\hline	
	\multirow{3}{*}{Center} & $\lambda_1 = i \omega$ & \multirow{3}{*}{Neutrally Stable} & $\vec{y} = c_1 \left[ \vec{u} \cos \omega t - \vec{w} \sin \omega t \right] +$ \\
	& $\lambda_2 = -i \omega$ & & $\quad c_2 \left[ \vec{w} \cos \omega t + \vec{u} \sin \omega t \right]$\\
	& & & $\vec{u} = \real(\vec{v}_1)$, $\vec{w} = \imag(\vec{v}_1)$\\
	\hline	
	\multirow{3}{*}{Spiral Point} & $\lambda_1 = \alpha + i \omega$ & \multirow{3}{*}{Stable: $\alpha < 0$, Unstable: $\alpha > 0$} & $\vec{y} = c_1 e^{\alpha t} \left[ \vec{u} \cos \omega t - \vec{w} \sin \omega t \right] +$\\
	& $\lambda_2 = \alpha - i \omega$ & & $\quad c_2 e^{\alpha t}  \left[ \vec{w} \cos \omega t + \vec{u} \sin \omega t \right]$\\
	& & & $\vec{u} = \real(\vec{v}_1)$, $\vec{w} = \imag(\vec{v}_1)$\\
	\hline
	\\[-1em]% A little extra space before the next line
	\multirow{2}{*}{Degenerate Node}	& $\lambda_i = \lambda_j$, & \multirow{3}{*}{Same as Proper Node} & $\vec{y} = c_1 e^{\lambda t} \vec{v} + c_2 \left[ e^{\lambda t} \vec{\mu} + t e^{\lambda t} \vec{v} \right]$\\
	& only one eigenvector & & $\vec{\mu}$ solves $(\coxmat{A} - \lambda\coxmat{I})\vec{\mu} = \vec{v}$
	\end{tabular}	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Wronskian}\end{center}
	\index{Ordinary Differential Equations!Wronskian}
	\begin{minipage}[t!]{0.45\textwidth}
		\[ \coxmat{W} = \begin{bmatrix} &&&\\ \vec{f}_1 & \vec{f}_2 & \dots & \vec{f}_n\\ &&&	\end{bmatrix} \]
	\end{minipage}
	\begin{minipage}[t!]{0.45\textwidth}
		Solutions, $\vec{f}_1,\,\, \dots,\,\, \vec{f}_n$, to system of $n$ 1st order diff. eq. are lin. indep. iff $\det \coxmat{W} \neq 0$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Critical Points}\end{center}
	\index{Ordinary Differential Equations!critical points}
	\begin{itemize}
		\item Set all time derivatives to zero, solve for state variables
		\item Represent fixed points in phase space (see 2D Solution Subspace Characterizations)
	\end{itemize}
\end{contentBox}

\chapter{Partial Differential Equations}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{General Approach - Separable Equations}\end{center}
	\vspace{0.5em}
	
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Solution Form}\end{center}
		\begin{align*}
			f(x, t) &= F(x) G(t)\\
			\text{or}&\\
			f(x, y) &= F(x) G(y)
		\end{align*}
		or similar (not limited to 2D). \textbf{Assume} $F$ and $G$ are independent and separable.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Plug In}\end{center}
		Substitute the solution form into the PDE and separate variables, e.g.
		\[ \frac{F''}{F} = \frac{-\ddot{G}}{G} = \pm k^2 \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Separate Eqs}\end{center}
		Write ODE for each separable equation:
		\begin{align*}
			F'' \mp k^2 F &= 0\\
			\ddot{G} \pm k^2 G &= 0
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	Next, write initial and boundary conditions in terms of the separable functions:
	
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Dirichlet}\end{center}
		\vspace{-1em}
		\begin{align*}
			f(0, t) &= F(0)G(t) = 0\\
			f(L, t) &= F(L)G(t) = 0
		\end{align*}
		and other similar B.C.s
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Neumann}\end{center}
		\vspace{-1em}
		\begin{align*}
			\dpd{f(0,t)}{t} &= F(0)\dot{G}(t) = 0\\
			\dpd{f(L,t)}{t} &= F(L)\dot{G}(t) =  0
		\end{align*}
		and other similar B.C.s
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Initial}\end{center}
		\vspace{-1em}
		\begin{align*}
			f(x, 0) &= h(x)
		\end{align*}
		and other similar I.C.s
	\end{minipage}%
	
	\vspace{1em}
	\textbf{Strategy:}
	\begin{enumerate}
		\item If possible, identify two B.C.s that specify zero for one of the functions and its derivative, e.g. $F(0) = 0$ and $F'(L) = 0$, and set the sign of $k^2$ such that the separable equation in that function has an oscillatory solution (imaginary roots of the characteristic eq.).
		\item Solve the first ODE (e.g., the one in $F$ that we identified in the previous step); the goal is to solve for $F$, up to a constant coefficient, and $k^2$ using only two of the B.C.s. The result is usually a set of eigenfunctions indexed by $n$. For example, $k = n\pi/L$ and $F = \sum F_n$ for $n = 1, \dots, \infty$.
		\item Apply further B.C.s to simplify the form of the solution to the second ODE (e.g., in $G$); usually it is possible to eliminate terms as coefficients evaluate to zero; the solution will also take the form of a sum of eigenfunctions since $k$ is a function of $n$.
		\item Combine independent solutions, e.g.,
		\[ f(x,t) = \sum\limits_{n=1}^{\infty} A_n F_n(x) G_n(t)\,, \]
		and evaluate the initial condition. The result is an infinite sum equal to a function; The unknown coefficients $A_n$ are, thus, determined from this equation and are frequently obtained by arranging the equation so that $A_n$ are the Fourier coefficients of $h(x)$, or the coefficients of some other well-known orthogonal decomposition of $h(x)$.
	\end{enumerate}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Wave Equation}\end{center}
	
	\begin{minipage}[t]{0.35\textwidth}
		\[ \dpd[2]{u}{t} = c^2 \dpd[2]{u}{x}, \quad c = \sqrt{\frac{T}{\rho}} \]
		where $T$ is tension, $rho$ is material density, and string has length $L$. Solution Steps:
		\begin{enumerate}[label=\roman*]
			\item Separation of vars: $u(x,t) = F(x)G(t)$ $\to$ $F\ddot{G} = c^2 F'' G$
			\[ \frac{\ddot{G}}{G} \frac{1}{c^2} = \frac{F''}{F} = k \]
			Each side is independent, so the expression is constant; two single-variable ODEs:
			\[ F'' - kF = 0,\quad \ddot{G} - kc^2 G = 0 \]
			\item Satisfy boundary conditions and initial conditions
			\item Use Fourier Series to find solution
		\end{enumerate}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.55\textwidth}
		Case 1: Attached ends: $u(0,t) = u(L,t) = 0$. I.C.: $u(x,0) = f(x)$, $\dpd{u(x,0)}{t} = g(t)$. Only $k = -\nu^2 < 0$ yields ``interesting'' solutions.
		\[
			u(x,t) = \sum\limits_{n=1}^{\infty} \left[ B_n \cos(\lambda_n t) + B_n^* \sin(\lambda_n t) \right] \sin \left( \frac{n \pi x}{L} \right), \quad \lambda_n = \frac{c n \pi}{L}
		\]
		$B_n$ and $B_n^*$ are the Fourier Sine Series coefficients of $f(x)$ and $g(x)$:
		\begin{align*}
			B_n &= \frac{2}{L} \int\limits_0^L f(x) \sin \left( \frac{n \pi x}{L} \right) \dif x\\
			\lambda_n B_n^* &= \frac{2}{L} \int\limits_0^L g(x) \sin \left( \frac{n \pi x}{L} \right) \dif x
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t!]{0.25\textwidth}
		Case 2: D'Alembert's solution (Applies to infinite string [no boundary conditions] \textit{and} bounded string)
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.65\textwidth}
		\[ u(x,t) = \frac{1}{2} \left[ f^*(x - ct) + f^*(x + ct) \right] + \frac{1}{2c} \int\limits_{x-ct}^{x+ct} g(\sigma) \dif \sigma \]
		Solution takes form of two moving waves. If boundary conditions are imposed, $f^*$ must be odd periodic extension of $f$ with period $2L$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Heat Equation}\end{center}
	Model some temperature distribution, $u(x,y,z,t)$ in a body: specific heat $\sigma$, density $\rho$, thermal conductivity $\kappa$:
	\[ \dpd{u}{t} = \frac{\kappa}{\sigma \rho} \nabla^2 u = c^2 \nabla^2 u \qquad \text{1D case: } \dpd{u}{t} = c^2 \dpd[2]{u}{x}\]
	Same solution strategy as Wave equation. It is assumed that bar is only capable of losing heat through ends (no radiation or convection)
	
	\vspace{1em}
	\noindent\textit{Case 1: Perfectly insulated, ends maintain temperature 0}
	
	\begin{minipage}[t]{0.25\textwidth}
		\begin{align*}
			u(0,t) &= u(L,t) = 0\\
			u(x,0) &= f(x)
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.7\textwidth}
		\vspace{-2em}
		\begin{align*}
			u(x,t) &= \sum\limits_{n=1}^{\infty} B_n \sin \left( \frac{n \pi x}{L} \right) e^{-\lambda_n^2 t}, \quad \lambda_n = \frac{c n \pi}{L}, \quad c= \sqrt{\frac{\kappa}{\sigma \rho}}\\
			B_n &= \frac{2}{L} \int\limits_0^L f(x) \sin \left( \frac{n \pi x}{L} \right) \dif x
		\end{align*}
		$B_n$ are the Fourier Sine Series coefficients of $f(x)$
	\end{minipage}
	
	\vspace{1em}
	\noindent\textit{Case 2: Insulated Ends}
	
	\begin{minipage}[t]{0.25\textwidth}
		\begin{align*}
			\dpd{u(0,t)}{x} &= \dpd{u(L,t)}{x} = 0\\
			u(x,0) &= f(x)
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.7\textwidth}
		\vspace{-2em}
		\begin{align*}
			u(x,t) &= \sum\limits_{n=0}^{\infty} A_n \cos \left( \frac{n \pi x}{L} \right) e^{-\lambda_n^2 t}, \quad A_0 = \frac{1}{L} \int\limits_0^L f(x) \dif x\\
			A_n &= \frac{2}{L} \int\limits_0^L f(x) \cos \left( \frac{n \pi x}{L} \right) \dif x
		\end{align*}
		$A_n$ are the Fourier Cosine Series coefficients of $f(x)$
	\end{minipage}
	
	\vspace{1em}
	\noindent\textit{Case 3: Infinitely Long Bar}
	
	\begin{minipage}[t]{0.2\textwidth}
		\begin{align*}
			u(x,0) &= f(x)
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.75\textwidth}
		\vspace{-2em}
		\begin{align*}
			u(x,t) &= \int\limits_0^{\infty} \left[ A(\omega) \cos(\omega x) + B(\omega) \sin(\omega x) \right] e^{-c^2 \omega^2 t} \dif \omega = \frac{1}{2c \sqrt{\pi t}} \int\limits_{-\infty}^{\infty} f(v) \sin \left( \frac{-(x-v)^2}{4ct} \right) \dif v\\
			A(\omega) &= \frac{1}{\pi} \int\limits_{-\infty}^{\infty} f(v) \cos(\omega v) \dif v, \quad B(\omega) = \frac{1}{\pi} \int\limits_{-\infty}^{\infty} f(v) \sin(\omega v) \dif v
		\end{align*}
		The temperature response is essentially a scaled Fourier integral; $A(\omega)$ and $B(\omega)$ are the Fourier Integral coefficients of $f(x)$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Steady-State Heat Equation (Laplace Eq)}\end{center}
	Set heat equation $\pd{u}{t} = 0$ for steady state behavior. Several boundary condition types:
	
	\vspace{1em}
	\begin{minipage}[t]{0.3\textwidth}
		\textit{Dirichlet} B.C.s: $u$ is prescribed on boundary
	\end{minipage}%
	\hspace{0.02\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\textit{Neumann} B.C.s: $\pd{u}{n}$ (normal derivative) is prescribed on boundary
	\end{minipage}%
	\hspace{0.02\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}
		\textit{Mixed} or \textit{Robin} B.C.s: combo of Dirichlet and Neumann
	\end{minipage}
	
	\vspace{1em}
	\noindent\textit{Case 1: 2D Rectangular Region $0 \leq x \leq a$, $0 \leq y \leq b$}
	
	\begin{minipage}[t]{0.45\textwidth}
		\[ \dpd[2]{u}{x} + \dpd[2]{u}{y} = 0 \]
		Boundary conditions: on top, $u(x,b) = f(x)$; other sides, $u(x,0) = u(y,0) = u(y,a) = 0$
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			u(x,y) = \sum\limits_{n=1}^{\infty} A_n^* \sinh \left(\frac{n \pi y}{a} \right) \sin \left( \frac{n \pi x}{a} \right)\\
			A_n^* = \frac{2}{a \sinh \left(\frac{n \pi b}{a} \right) } \int\limits_0^a f(x) \sin \left( \frac{n \pi x}{a} \right) \dif x
		\end{align*}
	\end{minipage}
	
	\noindent\textit{Case 2: 2D Circular Region $0 \leq r \leq R$, $-\pi \leq \theta \leq \pi$}
	
	\begin{minipage}[t]{0.45\textwidth}
		\[ \dpd[2]{u}{r} + \frac{1}{r} \dpd{u}{r} + \frac{1}{r^2} \dpd[2]{u}{\theta} = 0 \]
		Boundary condition: on edge, $u(R,\theta) = f(\theta)$
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			u(r, \theta) &= a_0 + \sum\limits_{n=1}^{\infty} \left[ a_n \left(\frac{r}{R}\right)^n \cos(n \theta) + b_n \left(\frac{r}{R}\right)^n \sin(n \theta) \right]\\
			a_0 &= \frac{1}{2\pi} \int\limits_{-\pi}^{\pi} f(\theta) \dif \theta \qquad a_n = \frac{1}{\pi} \int\limits_{-\pi}^{\pi} f(\theta) \cos(n\theta) \dif \theta\\
			b_n &= \frac{1}{\pi} \int\limits_{-\pi}^{\pi} f(\theta) \sin(n\theta) \dif \theta
		\end{align*}
	\end{minipage}
\end{contentBox}
