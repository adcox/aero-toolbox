\chapter{Optimization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Parameter Optimization}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Extremum}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		$f$ is a function of $n$ independent variables, $f(x_1,\dots,x_n)$
		\begin{itemize}
			\item $f(\vec{x}_0)$ is a local maximum if $f(\vec{x}) \leq f(\vec{x}_0)$ at all points $\vec{x}$ in the neighborhood of $\vec{x}_0$
			\item $f(\vec{x}_0)$ is a local minimum if $f(\vec{x}) \geq f(\vec{x}_0)$ at all points $\vec{x}$ in the neighborhood of $\vec{x}_0$
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\textit{Vanishing Partial Derivatives for Local Extrema}: $f$ has a local extremum at $\vec{x}_0$, where $f$ is $\mathcal{C}^1$ in some neighborhood of $\vec{x}_0$, if
		\[ \dpd{f}{x_1} = 0, \quad \dots, \quad \dpd{f}{x_n} = 0 \]
		at $\vec{x}_0$
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.45\textwidth}
		Let $f$ be $\mathcal{C}^2$ in some neighborhood of $\vec{x}_0$ and define the Hessian matrix
		\[ \coxmat{H} = \begin{bmatrix}
			f_{x_1 x_1}(\vec{x}_0) & f_{x_1 x_2}(\vec{x}_0) & \dots & f_{x_1 x_n}(\vec{x}_0)\\
			f_{x_2 x_1}(\vec{x}_0) & & & f_{x_2 x_n}(\vec{x}_0)\\
			\vdots & & & \vdots\\
			f_{x_n x_1}(\vec{x}_0) & f_{x_n x_2}(\vec{x}_0) & \dots & f_{x_n x_n}(\vec{x}_0)
		\end{bmatrix}
		\]
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Let $\det \coxmat{H} \neq 0$. If $\coxmat{H} > 0$ (positive definite) then $f$ has a local minimum at $\vec{x}_0$, if $\coxmat{H} < 0$ (negative definite) then $f$ has a local maximum at $\vec{x}_0$, and if $\coxmat{H}$ has at least one positive and one negative eigenvalue then $f$ has a saddle at $\vec{x}_0$.
		
		if $\det\coxmat{H} = 0$, $\vec{x}_0$ is a \textit{higher order critical point}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Constrained Optimization: Method of Elimination}\end{center}
	
	\begin{minipage}[t]{0.35\textwidth}
		Given
		\begin{align*}
			f &= f(x_1, \dots, x_n)\\
			g_1 &= g_1(x_1, \dots, x_n) = c_1\\
			\vdots & \qquad \vdots\\
			g_m &= g_m(x_1, \dots, x_n) = c_m \quad m \leq n\\
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.55\textwidth}
		\begin{enumerate}
			\item Solve constraint functions $g_i$ for control variables $x_j$
			\item Substitute expressions for $x_j$ in terms of $h_i$ into objective function, $f$
			\item Find critical points of modified $f$ function as usual
		\end{enumerate}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Constrained Optimization: Lagrange Multiplier Method}\end{center}
	
	\begin{minipage}[t]{0.35\textwidth}
		Given
		\begin{align*}
			f &= f(x_1, \dots, x_n)\\
			g_1 &= g_1(x_1, \dots, x_n) = c_1\\
			\vdots & \qquad \vdots\\
			g_m &= g_m(x_1, \dots, x_n) = c_m \quad m \leq n\\
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.55\textwidth}
		\begin{enumerate}
			\item Form a new objective function $F \equiv f - \sum\limits_{i=1}^m \lambda_i g_i(x_1, \dots, x_n)$
			\item Form system of equations
			\begin{alignat*}{3}
				\dpd{F}{x_1} &= 0 \qquad \qquad &\dpd{F}{\lambda_1} &= g_1 = c_1\\
				\vdots \quad & \quad \vdots \qquad  \qquad &\vdots \quad & \quad \vdots\\
				\dpd{F}{x_n} &= 0 \qquad \qquad &\dpd{F}{\lambda_m} &= g_m = c_m
			\end{alignat*}
			\item Solve the system for $x_1, \dots, x_n$ and $\lambda_1,\dots,\lambda_m$ to locate extremum
		\end{enumerate}
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Functional Optimization}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Background}\end{center}
	Bernoulli's investigation of the Brachistochrone problem led to the discovery that dynamical motion obeys Lagrange's equations, i.e., the functional
	\[ J = \int\limits_{t_0}^{t_f} L \dif t \]
	has a stationary value; $L$ is the \textit{Lagrangian}, i.e., the difference between the kinetic and potential energies of the dynamical system. This idea is known as Hamilton's Principle.
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Bolza Problem}\end{center}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{align*}
		\text{Minimize } J &= \phi(t_f, \vec{x}_f) + \int\limits_{t_0}^{t_f} L(t, \vec{x}, \vec{u}) \dif t\\
		\text{Subject to: } \dot{\vec{x}} &= \vec{f}(t, \vec{x}, \vec{u}) \qquad \text{(Process Eqs, i.e., EOMs)}\\
		\vec{x}(t_0) &= \vec{x}_0 \qquad \text{(Initial conditions)}\\
		\vec{\psi}(t_f, \vec{x}_f) &= \vec{0} \qquad \text{(Terminal boundary conditions)}\\
	\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		where
		\begin{itemize}
			\item $J$ is the \textit{cost index}, \textit{performance index}, or a functional
			\item $L$ is the Lagrangian (more general than Lagrangian in dynamics context)
			\item $\phi(t_f, \vec{x}_f)$ is the \textit{endpoint cost} or \textit{terminal cost}
			\item $\int L \dif t$ is the \textit{path cost}
			\item $t$ is time
			\item $\vec{x}$ is state ($n$-vector)
			\item $\vec{u}$ is control ($m$-vector) $\in U$, where $U$ is set of admissible controls
			\item $\vec{\psi}$ is terminal constraint ($p$-vector, $p \leq n+1$)
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Lagrange Problem}\end{center}
		\[ \text{Minimize } J = \int\limits_{t_0}^{t_f} L(t, \vec{x}, \vec{u}) \dif t \]		
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Meyer Problem}\end{center}
		\[ \text{Minimize } J = \phi(t_f, \vec{x}_f) \]
	\end{minipage}
	
	With both problems, the rest is the same as the Bolza formulation. It is always possible to change from one form (Bolza, Lagrange, Meyer) to another
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{1st Order Necessary Conditions for Optimal Solution}\end{center}
	The solutions $\vec{x}^*(t)$ and $\vec{u}^*(t)$ optimize the cost function iff the following necessary conditions are satisfied over the class of admissible functions:
	\begin{enumerate}
		\item Euler-Lagrange Equations: $\dot{\vec{\lambda}}^T = -H_{\vec{x}}$, $H_u = 0$, transversality
		\item Legendre-Clebsch Conditions: $H_{uu} \geq 0$
		\item Weierstrass Condition: $H(t, x^*, \lambda, u^*) \leq H(t, x^*, \lambda, u)$
	\end{enumerate}
	
	\begin{center}\textbf{2nd Order Necessary Conditions for Optimal Solution}\end{center}
	\begin{enumerate}
		\setcounter{enumi}{3}
		\item Jacobi Condition (Nonexistence of conjugate point in [$t_0$, $t_f$])
	\end{enumerate}
	
	\begin{center}\textbf{Necessary Conditions at Corners of Optimal Solution}\end{center}
	Stuff about Weierstrass-Erdman Corner Conditions
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Euler-Lagrange Theorem, Adjoined Method (Necessary Condition I)}\end{center}
	
	Given the Bolza Problem (except we now say $\vec{\psi}$ is a $q$-vector) and assuming $\phi$, $L$, $\vec{f}$, $\vec{\psi}$ $\in \mathcal{C}^1$ on their respective domains; If $u^*(t) \in \mathcal{C}^0 [t_0,\, t_f]$ minimizes $J$ then $\exists$ a time-varying multiplier vector $\vec{\lambda}(t)$ and a constant multiplier vector $\vec{\nu}$ such that, with the \textit{Hamiltonian} and \textit{Terminal Function}
	\[ H(t, \vec{x}, \vec{u}, \vec{\lambda}) \equiv L(t, \vec{x}, \vec{u}) + \vec{\lambda}^T \vec{f}(t, \vec{x}, \vec{u})\,, \qquad \Phi(t_f, \vec{x}_f) \equiv \phi(t_f, \vec{x}_f) + \vec{\nu}^T \vec{\psi}(t_f, \vec{x}_f) \,, \]
	the following equations must hold,
	
	\begin{minipage}[t]{0.22\textwidth}
		\begin{center}\textit{Costate Equations}\end{center}
		\vspace{-2em}
		\begin{center}($n$ differential equations)\end{center}	
		\begin{align*}
			\dot{\lambda}_i &= - \eval[2]{\dpd{H}{x_i}}_{*}\\
			i &= 1,\dots,n
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.22\textwidth}		
		\begin{center}\textit{Costate Boundary Values}\end{center}
		\vspace{-2em}
		\begin{center}($n$ algebraic equations)\end{center}
		\[ \vec{\lambda}(t_f) = \dpd{\Phi^*}{\vec{x}_f} \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.22\textwidth}		
		\begin{center}\textit{Control Law}\end{center}
		\vspace{-2em}
		\begin{center}($m$ algebraic equations)\end{center}
		\[ \dpd{H}{\vec{u}} = \vec{0} \]
		Yields control law, only valid for unconstrained control
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.22\textwidth}
		\begin{center}\textit{State Equations}\end{center}
		\vspace{-2em}
		\begin{center}($n$ differential equations)\end{center}
		\[ \dot{\vec{x}} = \vec{f} = \dpd{H}{\vec{\lambda}} \]
	\end{minipage}

	\vspace{1em}	
	\begin{minipage}[t]{0.46\textwidth}
		These equations define a set of $2n$ differential equations that govern the states, $\vec{x}$, and costates, $\vec{\lambda}$. $2n$ B.C.s are available from the costate boundary value equations (above) and the initial conditions, $\vec{x}(t_0) = \vec{x}_0$.	
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Transversality Condition}\end{center}
		\vspace{-2em}
		\begin{center}(Valid \textit{only} if $\dif t_f \neq 0$)\end{center}
		\vspace{-1em}
		\[ \Omega = L_f + \dod{\Phi}{t_f} = 0 \]		
		
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Euler-Lagrange Theorem, Unadjoined Method (Necessary Condition I)}\end{center}
	Given the Bolza Problem and assuming $\phi$, $L$, $\vec{f}$, $\vec{\psi}$ $\in \mathcal{C}^1$ on their respective domains; If $u^*(t) \in \mathcal{C}^0 [t_0,\, t_f]$ minimizes $J$ then $\exists$ multipliers $\lambda_1(t),\dots, \lambda_m(t)$ such that with the \textit{Hamiltonian},
	\[ H(t, \vec{x}, \vec{u}, \vec{\lambda}) \equiv L(t, \vec{x}, \vec{u}) + \vec{\lambda}^T \vec{f}(t, \vec{x}, \vec{u})\,, \]
	the following equations must hold,
	
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Costate Equations}\end{center}
		\vspace{-2em}
		\begin{center}($n$ differential equations)\end{center}		
		\[ \dot{\lambda}_i = - \eval[3]{\dpd{H}{x_i}}_{\vec{x}^*},\quad i = 1,\dots,n \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}		
		\begin{center}\textit{Control Law}\end{center}
		\vspace{-2em}
		\begin{center}($m$ algebraic equations)\end{center}
		\[ \dpd{H}{\vec{u}} = \vec{0} \]
		Yields control law, only valid for unconstrained control
	\end{minipage}%	
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.3\textwidth}		
		\begin{center}\textit{State Equations}\end{center}
		\vspace{-2em}
		\begin{center}($n$ differential equations)\end{center}
		\[ \dot{\vec{x}} = \vec{f} = \dpd{H}{\vec{\lambda}} \]
	\end{minipage}
	
	\vspace{1em}
	These equations define a set of $2n$ differential equations that govern the states, $\vec{x}$, and costates, $\vec{\lambda}$. The addition of the costate equations necessitates more boundary conditions to create a well-defined TPBVP (total of $2n + 2$ B.C.s).
	
	\vspace{1em}
	\begin{minipage}[t]{0.3\textwidth}
		\begin{center}\textit{Predefined B.C.s}\end{center}
		\vspace{-2em}
		\begin{center}($n + 1 + p$ boundary conditions)\end{center}
		\vspace{-2em}
		\begin{align*}
			t_0 & \quad (1)\\
			\vec{x}_0 &= \vec{x}(t_0) \quad (n)\\
			\vec{\psi}(t_f, \vec{x}_f) &= \vec{0} \quad (p)
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.65\textwidth}
		\begin{center}\textit{Transversality Condition}\end{center}
		\vspace{-2em}
		\begin{center}($n + 1 - p$ boundary conditions)\end{center}
		\vspace{-1em}		
		$H^*_f \dif t_f - \vec{\lambda}^T_f \dif \vec{x}_f + \dif \phi = 0$ subject to $\dif \psi = 0$ where
		
		\begin{minipage}[t!]{0.35\textwidth}			
			\begin{align*}
				H^*_f &= H(t_f^*)\\
				\vec{\lambda}_f &= \vec{\lambda}(t_f)
			\end{align*}
		\end{minipage}%
		\hspace{0.03\textwidth}
		\begin{minipage}[t!]{0.57\textwidth}
			\begin{align*}
				\dif \phi &= \dpd{\phi}{\vec{x}_f} \dif \vec{x}_f + \dpd{\phi}{t_f} \dif t_f\\
				\dif \psi_i &= \dpd{\psi_i}{\vec{x}_f} \dif \vec{x}_f + \dpd{\psi_i}{t_f} \dif t_f = 0 \quad i = 1,\dots p
			\end{align*}
		\end{minipage}
	\end{minipage}
	
	Some additional notes:
	\begin{itemize}
		\item Differential terms quantify the variation in a quantity from some reference. If that quantity is fixed, there is no variation. I.e., if $t_f$ is fixed, then $\dif t_f = 0$. Similarly, if $x_{i,f} = c = $ fixed, then $\dif x_{i,f} = 0$.
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Solving Bolza: A Cookbook}\end{center}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{enumerate}
			\item Form the Hamiltonian: $H = L + \vec{\lambda}^T \vec{f}$
			\item Write the Euler-Lagrange Equations:
				\begin{align*}
					\dot{\lambda}_i &= -\dpd{H}{x_i}, \quad i=1,\dots,n\\
					H_u &= \dpd{H}{u} = 0
				\end{align*}
				When possible, use $H_u$ to find the control, $\vec{u}$ ($m$-vector). When a definitive control is not obtained from this eq., use the Minimum Principle. In either case, substitute the resulting control into the state equation, $\dot{\vec{x}} = \vec{f}(t, \vec{x}, \vec{u})$, and the costate equation (above).
			
			\item Apply the transversality condition. Use either the adjoined or un-adjoined method.
			\begin{enumerate}[label=\roman*]
				\item Adjoined Method: Use the algebraic form of the transversality condition, $\Omega$ (only valid if $t_f$ is free, i.e., $\dif t_f \neq 0$):
				\begin{align*}
					\Phi(t_f, \vec{x}_f) &\equiv \phi(t_f, \vec{x}_f) + \vec{\nu}^T \vec{\psi}(t_f, \vec{x}_f)\\
					\Omega &= L_f + \dod{\Phi}{t} = 0
				\end{align*}
				Solve for $\vec{\nu}$ ($q$-vector), which appears in $\Phi$, using the boundary conditions, $\vec{\psi}$. Treat the components of $\vec{\lambda}(t_f)$ as independent.
			\end{enumerate}
		\end{enumerate}
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{enumerate}
			\setcounter{enumi}{2}
			\item (Continued...)
			\begin{enumerate}[label=\roman*]
				\setcounter{enumii}{1}
				\item Un-Adjoined Method:
				\begin{itemize}
					\item Write $\vec{\psi}(t_f, \vec{x}_f)$ as a $p$-vector and use $\dif \vec{\Psi} = 0$ to solve for $p$ differentials in terms of the remaining $n+1-p$ differentials
					\item Sub these $p$ differentials into the transversality condition
					\item Obtain an expression involving $n+1-p$ ``independent'' differentials and equate their coefficients to zero
				\end{itemize}
			\end{enumerate}
			
			\item Apply the Minimum Principle to supply additional conditions on the control law so that a specific law may be found and leveraged in step 2.
		\end{enumerate}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Legendre-Clebsch Conditions (Necessary Condition II)}\end{center}
	If $H$ is differentiable in $u$ to 2nd order, $u^*$ is interior (not on control boundary) at $t$, and $u(t)$ is ``infinitesimally close'' to $u^*(t)$ then
	\[ \dpd{H^*(t)}{u} = 0, \qquad \dpd[2]{H^*(t)}{u} \geq 0 \quad \text{(pos, semi-def)} \]
	If $u^*$ is on the boundary, it is not necessary that $\pd{H^*(t)}{u} = 0$
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Weierstrass Condition (Necessary Condition III)}\end{center}
	Let $\vec{x}^*(t) \in \mathcal{C}^1 [t_0,\, t_f]$ and $\vec{u}^*(t) \in \mathcal{C}^0[t_0, \, t_f]$ minimize $J$ in the Bolza formulation. Then at each $t \in [t_0,\, t_f]$ it is necessary that
	\[ H \left( t,\, \vec{x}^*(t),\, \vec{u}^*(t),\, \vec{\lambda}(t) \right) \leq H \left( t,\, \vec{x}^*(t),\, \vec{u}^(t),\, \vec{\lambda}(t) \right) \]
	for all admissible (unbounded control) $\vec{u}(t)$ (Weierstrass does not hold if $\vec{u}(t)$ is bounded)
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Minimum Principle (Necessary Condition III)}\end{center}
	A more general form of Weierstrass; ``$H^*$ must be minimized over the set of all possible $u$''
	
	\begin{minipage}[t]{0.46\textwidth}
		Consider the optimal control problem to minimize
		\[ J = \lambda_0 \phi(t_0, \vec{x}_0, t_f, \vec{x}_f) + \lambda_0 \int\limits_{t_0}^{t_f} L(t,\vec{x},\vec{u}) \dif t \]
		subject to
		\[ \dot{\vec{x}} = \vec{f}(t, \vec{x}, \vec{u}) \qquad \vec{x}(t_0) = \vec{x}_0 \]
		where $\vec{x}$ is an $n$-vector and $\vec{u}$ is an $m$-vector, and where the terminal constraints are $\vec{\psi}(t_f, \vec{x}_f) = \vec{0}$ and $\vec{u} \in U$, the set of admissible controls.
		
		The set $U$ is an arbitrary set in $m$-dimensional Euclidean space and $\vec{u}(t)$ may be a ``measurable'' piecewise continuous function on $[t_0,\, t_f]$ whose values lie in $U$;
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		Suppose $\vec{u}^*(t) \in U$ minimizes $J$. Then the Hamiltonian is
		\[ H(t, \vec{x}, \vec{u}, \vec{\lambda}) \equiv \lambda_0 L(t, \vec{x}, \vec{u}) + \vec{\lambda}^T \vec{f}(t, \vec{x}, \vec{u}) \]
		According to the multiplier rule, $\exists$ continuous costate functions $\lambda_i(t)$ on $[t_0,t_f]$ for $i=0,1,\dots,n$ s.t.
		\[\dot{\lambda}_i = -\dpd{H^*}{x_i} \quad i=1,\dots,n \]
		at each $t \in [t_0,t_f]$ on arcs where $\vec{u}(t)$ is continuous, and $\lambda_0$ is a constant s.t.
		\[ \begin{Bmatrix}\lambda_0 & \lambda_1(t) & \dots & \lambda_n(t) \end{Bmatrix}^T \neq \vec{0} \]
		at each $t \in [t_0,t_f]$
	\end{minipage}
	
	\vspace{0.5em}
	For the conditions stated above, we have Pontryagin's Minimum Principle: $H \left( t,\, \vec{x}^*(t),\, \vec{u}^*(t),\, \vec{\lambda}(t) \right)$ is continuous on $[t_0,t_f]$ and
	\[H \left( t,\, \vec{x}^*(t),\, \vec{u}^*(t),\, \vec{\lambda}(t) \right) \leq H \left( t,\, \vec{x}^*(t),\, \vec{u}^(t),\, \vec{\lambda}(t) \right) \]
	for $\vec{u} \in U$. A problem is said to be \textit{abnormal} if $\lambda_0 = 0$ and the Minimum Principle conditions are satisfied; Otherwise, $\lambda_0 = 1$ and the problem is termed \textit{normal} (the usual case). Additionally, the differential form of the transversality condition becomes
	\[ \eval{\left[ H^* \dif t - \vec{\lambda}^T \dif \vec{x} \right]}_{t_0}^{t_f} + \lambda_0 \dif \phi^* \]
	subject to $\dif \vec{\psi} = \vec{0}$
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Jacobi (Conjugate Point) Condition (Necessary Condition IV)}\end{center}
	An extremal that contains a conjugate point is not a minimum; $\exists$ a neighboring solution of lower cost. To determine whether a conjugate point exists see the work of Prussing and Sandrik (2005, ``Second-orer necessary conditions and sufficient conditions applied to continuous-thrust trajectories,`` J. Guid. Control Dyn.) Colloquially, a \textit{conjugate point} is like a non-unique solution (e.g., infinitely many shortest paths from North pole to South pole)
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Weierstrass-Erdman Corner Conditions}\end{center}
	
	\begin{minipage}[t]{0.2\textwidth}
		Let $u$ be a scalar with $|u| \leq 1$ for the standard Bolza problem. If $x^*(t) \in \mathcal{C}^0 [t_0, t_f]$ and $u^*(t) \in \text{P.C. }[t_0, t_f]$ minimize $J$
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.72\textwidth}
	Then
	\begin{enumerate}[label=(\roman*)]
		\item $\dot{\vec{\lambda}} = -\dpd{H^*}{x}$ on each subarc between corners (i.e., points where control is discontinuous and $\vec{x}^*$ has a corner)
		\item $\vec{\lambda}(t)$ and $H(t)$ are continuous on the entire trajectory and across corners
		\item \[ H^*_u(t) \left\{
		\begin{array}{l l}
			\leq 0 & \text{if } u^*(t) = +1\\
			= 0 & \text{if }-1 < u^*(t) < +1\\
			\geq 0 & \text{if } u^*(t) = -1			
		\end{array}
		\right.
		\]
	\end{enumerate}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Weak and Strong Extremals}\end{center}
	Neighborhoods:
		\begin{align*}
			N_1(\vec{x}^*, \vec{u}^*) &= \left\{ \eval[2]{(\vec{x}, \vec{u})\,\,} \,\,\norm{\vec{x} - \vec{x}^*} < \epsilon, \epsilon > 0 \text{ and small} \right\}\\
			N_2(\vec{x}^*, \vec{u}^*) &= \left\{ \eval[2]{(\vec{x}, \vec{u})\,\,} \,\,\norm{\vec{x} - \vec{x}^*} < \epsilon, \norm{\vec{u} - \vec{u}^*} < \delta, \epsilon,\delta > 0 \text{ and small} \right\}
		\end{align*}
		
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item $\vec{x}^*(t)$ is a \textit{weak extremal} if $J(\vec{x}^*) \leq J(\vec{x})$ for each $(\vec{x}, \vec{u}) \in N_2(\vec{x}^*, \vec{u}^*)$ that satisfies the constraints
			\item $\vec{x}^*(t)$ is a \textit{strong extremal} if $J(\vec{x}^*) \leq J(\vec{x})$ for each $(\vec{x}, \vec{u}) \in N_1(\vec{x}^*, \vec{u}^*)$ that satisfies the constraints
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item All strong extremals are also weak extremals
			\item $\vec{x}^*(t)$ is a weak extremal if it satisfies Legendre-Clebsch and is strong if it also satisfies the Minimum Principle/Weierstrass Conditions.
		\end{itemize}
		
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Inequality Constraints}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		So far, only end point equality constraints have been considered; now consider equality and/or inequality path constraints
		
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{enumerate}
			\item Consider the control constraint $G(\vec{u}) \leq \vec{0}$; then the minimum principle applies and we have a ``pure'' control constraint
			\item Consider the state constraint $S(t, \vec{x}) \leq \vec{0}$; modifications to the minimum principle are required; ``State Variable Inequality Constraint (SVIC) Problem,`` see Bryson and Ho, Ch.3, for more info; Main result is that the Lagrange multipliers and Hamiltonian may be discontinuous at the entry times to the boundary, $S(t, \vec{x}) = \vec{0}$
		\end{enumerate}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Simple Bounded Control - Switching Function}\end{center}
	Suppose $u$ is a scalar and bounded such that $|u| \leq 1$ and the Hamiltonian is
	\[ H = H_0(t,\vec{x}, \vec{\lambda}) + H_1(t, \vec{x}, \vec{\lambda}) u \]
	
	\begin{minipage}[t]{0.46\textwidth}
		Application of the minimum principle is different since $u$ is bounded. To minimize $H$, choose $u$ as a function of the sign of $H_1$ ($H_1$ is called the \textit{switching function}). In the case where $H_1 = 0$ on $[t_1,\, t_2] \leq [t_0,\, t_f]$, the subarc $[t_1,\, t_2]$ is called a \textit{singular subarc}.
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\[ u = \left\{ \begin{array}{l c l}
			-1 & \text{if} & H_1 > 0\\
			? & \text{if} & H_1 = 0\\
			+1 & \text{if} & H_1 < 0
		\end{array} \right. \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Singular Arcs}\end{center}
	An extremal arc, $\pd{H}{\vec{u}} = \vec{0}$, on which $\pd[2]{H}{\vec{u}}$ is singular is a \textit{singular arc}. In the singular problem, neither the Minimum Principle nor classical C.o.V. provides adequate tests for optimality.
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		Assume $u$ is scalar and bounded, $|u| \leq 1$, and $H$ is linear in $u$, i.e., $H = H_0(t,\vec{x}, \vec{\lambda}) + H_1(t, \vec{x}, \vec{\lambda}) u$. From the Minimum Principle,
		\[ u = \left\{ \begin{array}{l c l}
			-1 & \text{if} & H_1(t, \vec{x}, \vec{\lambda}) > 0\\
			+1 & \text{if} & H_1(t, \vec{x}, \vec{\lambda}) < 0
		\end{array} \right. \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		Then, if $u^*(t)$ is a scalar singular optimal control, the \textit{Generalized Legendre-Clebsch Condition} stipulates that
		\[ (-1)^q \dpd{}{u} \left( \dod[2q]{H^*_u}{t} \right) \leq 0 \]
		where the $2q$-th derivative if the first even derivative of $H_u$ that contains $u$ explicitly. This eq. also known as ``Kelley-Contensou Condition''
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{General Theory of Optimal Rocket Trajectories}\end{center}
	To-Be-Documented; May be more appropriate in a different section as it is application-specific
\end{contentBox}

%\begin{contentBox}
%	\footnotesize%
%	\begin{center}\textbf{Title}\end{center}
%\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linear Programming}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Simplex Method}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
	
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
	
	\end{minipage}
\end{contentBox}