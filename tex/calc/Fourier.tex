\section{Fourier Series}
\index{Fourier Series}

\begin{contentBox}
	\footnotesize%
	Special case of eigenfunctions that leverage trigonometric polynomials as an orthogonal basis. Functions are approximated by projecting into the trigonometric polynomial space.
	
	\begin{center}\textbf{Definition}\end{center}
	Periodic, piecewise continuous function $f(x)$ with period $p = 2L$ on interval $[-L,\,L]$
	
	\vspace{-1em}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			f(x) &= a_0 + \sum\limits_{n=1}^{\infty} \left[ a_n \cos\left( \frac{n\pi x}{L} \right) + b_n \sin \left( \frac{n\pi x}{L} \right) \right]\\
			a_0 &= \frac{1}{2L} \int\limits_{-L}^L f(x) \dif x
		\end{align*}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			a_n &= \frac{1}{L} \int\limits_{-L}^L f(x) \cos \left( \frac{n\pi x}{L} \right) \dif t\\
			b_n &= \frac{1}{L} \int\limits_{-L}^L f(x) \sin \left( \frac{n\pi x}{L} \right) \dif t
		\end{align*}
	\end{minipage}
	
	\begin{center}\textbf{Properties}\end{center}
	\vspace{-1em}
	\begin{itemize}
		\item Fourier coefficients of a sum $f(x) + g(x)$ are the sums of the Fourier coefficients of $f(x)$ and $g(x)$
		\item Fourier coefficients of $c f(x)$ are $c$ times coefficients of $f(x)$
	\end{itemize}
	
	\begin{center}\textbf{Orthogonality of Trigonometric System}\end{center}
	\vspace{-1em}
	The trig. system $\cos(nx)$, $\sin(nx)$ is orthogonal on $[-\pi,\,\pi]$, i.e., the inner product $\langle f, g \rangle = \int\limits_{-\pi}^{\pi}f(x)g(x)\dif x$ is
	
	\begin{minipage}[t]{0.45\textwidth}
		\[ \int\limits_{\pi}^{\pi} \cos(nx) \cos(mx)\dif x = 0 \quad \text{for } m \neq n \]
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.45\textwidth}
		\[ \int\limits_{\pi}^{\pi} \sin(nx) \sin(mx)\dif x = 0 \quad \text{for } m \neq n \]
	\end{minipage}
	
	\begin{minipage}[t]{0.45\textwidth}
		\[ \int\limits_{\pi}^{\pi} \cos(nx) \sin(mx)\dif x = 0 \quad \forall \,\, m, \, n \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Convergence and Sum}\end{center}
	\index{Fourier Series!convergence}
	$f$ is periodic and piecewise continuous on [$-\pi$, $\pi$]. Let $f(x)$ have a left- and right-hand derivative at each $x$. Then the Fourier Series of $f$ converges and its sum is $f(x)$ except at points $x_0$ where $f(x)$ is discontinuous; at these points the sum is
	\[ \frac{f(x_0)^- + f(x_0)^+}{2} \]
\end{contentBox}

\begin{contentBox}
	\footnotesize
	
	\begin{minipage}[t]{0.5\textwidth}
		\begin{center}\textbf{Error}\end{center}
		\index{Fourier Series!error}
		\vspace{-1em}
		$f(x)$ is periodic on [$-\pi$, $\pi$]. Let $F(x)$ be the $N$th partial sum (terms up to $n = N$ of the Fourier Series). The error between the function and its approximation is
		\[ E = \int\limits_{-\pi}^{\pi} (f(x) - F(x))^2 \dif x = \int\limits_{-\pi}^{\pi} f(x)^2 \dif x - \pi \left[ 2a_0^2 + \sum\limits_{n=1}^N (a_n^2 + b_n^2) \right] \]
	\end{minipage}		
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.4\textwidth}
		\begin{center}\textbf{Parseval's Identity}\end{center}
		\index{Parseval's Identity}
		\vspace{-1em}
		As an extension of the error equation,
		\[ 2a_0^2 + \sum\limits_{n=1}^{\infty} (a_n^2 + b_n^2) = \frac{1}{\pi} \int\limits_{-\pi}^{\pi} f(x)^2 \dif x \]
		This is useful to prove the convergence of sequences and series (series on the left, convergence value on the right).
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Applications}\end{center}
	\begin{itemize}
		\item Diff EQ Solutions: approximate piecewise continuous forcing functions with Fourier Series for easier analyses (e.g., Laplace transforms)
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Even and Odd Function Shortcuts}\end{center}
	\index{Fourier Series!cosine series}
	\index{Fourier Series!sine series}
	If $f(x)$ is an even periodic function with period $2L$,
	
	\begin{minipage}[t]{0.45\textwidth}
		the Fourier series is a \textit{Fourier Cosine Series}:
		\begin{align*}
			f(x) &= a_0 + \sum\limits_{n=1}^{\infty} a_n \cos \left( \frac{n\pi x}{L} \right)\\
			a_0 &= \frac{1}{L} \int\limits_0^L f(x) \dif x\\
			a_n &= \frac{2}{L} \int\limits_0^L f(x) \cos \left( \frac{n \pi x}{L} \right) \dif x
		\end{align*}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		the Fourier series is a \textit{Fourier Sine Series}:
		\begin{align*}
			g(x) &= \sum\limits_{n=1}^{\infty} b_n \sin \left( \frac{n\pi x}{L} \right)\\
			b_n &= \frac{2}{L} \int\limits_0^L f(x) \sin \left( \frac{n \pi x}{L} \right) \dif x
		\end{align*}
		(Both take advantage of even/odd function integral properties)
	\end{minipage}		
	
	Fourier Cosine and Sine Series
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Even and Odd Extensions/Expansions}\end{center}
	\index{Fourier Series!extensions; even, odd}
	\begin{minipage}[t!]{0.45\textwidth}
		\includegraphics[width=\textwidth]{Fourier_EvenOddExtensions/Slide1.png}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.45\textwidth}
		If $f(x)$ is defined on [0, $L$], we can define odd periodic extension and develop a sine series, or an even periodic extension and develop a cosine series, both with period $2L$
	\end{minipage}
	
	\vspace{0.5em}
	\begin{minipage}[t]{0.47\textwidth}
		\begin{center}\textit{Even Extension}\end{center}
		\includegraphics[width=\textwidth]{Fourier_EvenOddExtensions/Slide2.png}
	\end{minipage}%
	\begin{minipage}[t]{0.47\textwidth}
		\begin{center}\textit{Odd Extension}\end{center}
		\includegraphics[width=\textwidth]{Fourier_EvenOddExtensions/Slide3.png}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Generalized Fourier Series - Orthogonal Eigenfunction Expansions}\end{center}
	\index{Fourier Series!generalized}
	
	\begin{minipage}[t]{0.45\textwidth}
		The solutions $y_0$, $y_1$, $y_2$ are orthogonal functions w.r.t weight function $r(x)$ on $[a,b]$ if
		\[ \left\langle y_m,\, y_n \right\rangle = \int\limits_a^b r(x) y_m(x) y_n(x) =
		\left\{ \begin{array}{c l}
			0 & m \neq n\\
			\norm{y_m} & m = n
		\end{array} \right. \]
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		A function $f(x)$ (need not be continuous) can be projected into the space of $y_m$ solutions in form of ``orthogonal expansion'' or ``generalized Fourier series'' as
		\[ f(x) = \sum\limits_{m=0}^{\infty} \quad \text{on} [a,b] \]
		$a_i$ are ``generalized Fourier constants'', available from inner product
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Example: Periodic Sturm-Liouville Eq}\end{center}
		\vspace{-1em}
		\begin{align*}
			y'' + \lambda y = 0 \quad \text{on } [-\pi, \pi]\\
			y(-\pi) = y(\pi), \quad y'(-\pi) = y'(\pi)\\
			p(x) = 1,\quad q(x) = 0,\quad r(x) = 1
		\end{align*}
		For $\lambda > 0$, eigenfunctions are $\sin(mx)$, $\cos(mx)$ and
		\[ f(x) = a_0 + \sum\limits_{m=1}^{\infty} \left[ a_m\cos(mx) + b_m \sin(mx) \right] \]
		For $\lambda = 0$, $y$ is constant and for $\lambda < 0$, $y = 0$
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Example: Bessel Fcn of 1st Kind}\end{center}
		\vspace{-1em}
		\begin{align*}
			x^2 J_n''(kx) + xJ_n'(kx) + (k^2x^2 - n^2)J_n(kx) = 0 \quad 0 \leq x \leq R\\
			r(x) = x
		\end{align*}
		Can form Fourier-Bessel series from Bessel functions, $J_n(k_{n,m}, x)$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Fourier Integral}\end{center}
	\index{Integral!Fourier}
	$f(x)$ is piecewise continuous (need \textbf{not} be continuous) on every finite interval, has left- and right-derivatives, and $\int\limits_{-\infty}^{\infty} |f(x)| \dif x < \infty$, then $f(x)$ may be represented by the Fourier Integral:
	
	\vspace{-1.5em}
	\[ f(x) = \int\limits_0^{\infty} \left[ A(\omega) \cos(\omega x) + B(\omega) \sin(\omega x) \right] \dif \omega \qquad A(\omega) = \frac{1}{\pi} \int\limits_{-\infty}^{\infty} f(v) \cos(\omega v) \dif v \qquad B(\omega) = \frac{1}{\pi} \int\limits_{-\infty}^{\infty} f(v) \sin(\omega v) \dif v \]
	
	\begin{minipage}[t]{0.45\textwidth}
		If $f(x)$ is even
		\vspace{-1em}
		\[ B(\omega) = 0 \quad A(\omega) = \frac{2}{\pi} \int\limits_0^{\infty} f(v) \cos(\omega v) \dif v \]
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		If $f(x)$ is odd
		\vspace{-1em}
		\[ A(\omega) = 0 \quad B(\omega) = \frac{2}{\pi} \int\limits_0^{\infty} f(v) \sin(\omega v) \dif v \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Fourier Transform}\end{center}
	\index{Fourier Transform}
	If $f(x)$ is absolutely integrable on $x$-axis and piecewise-continuous on every finite interval:
	\[ f(x) = \frac{1}{\sqrt{2\pi}} \int\limits_{-\infty}^{\infty} \hat{f}(x) e^{i \omega x} \dif \omega \qquad \hat{f}(x) = \mathcal{F}\left( f(x) \right) = \frac{1}{\sqrt{2\pi}} \int\limits_{-\infty}^{\infty} f(x) e^{-i \omega x} \dif x \]
	
	\begin{minipage}[t]{0.45\textwidth}
		\vspace{-1em}
		\begin{align*}
			\text{Linearity: } & \mathcal{F} \left( a f(x) + b g(x) \right) = a \mathcal{F}(f)+ b \mathcal{F}(g)\\
			\text{Convolution: } & \mathcal{F} \left( f * g \right) = \sqrt{2\pi} \mathcal{F}(f) \mathcal{F}(g)
		\end{align*}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\vspace{-1em}
		\begin{align*}
			\text{Derivatives: } & \mathcal{F} \left( f'(x) \right) = i \omega \mathcal{F}\left( f(x) \right)\\
			& \mathcal{F} \left( f''(x) \right) = -\omega^2 \mathcal{F} \left( f(x) \right)
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Fourier Sine and Cosine Transforms}\end{center}
	\index{Fourier Transform!sine and cosine}
	If $f(x)$ is absolutely integrable, piecewise-continuous on every finite interval, $\int\limits_{-\infty}^{\infty} |f(x)| \dif x < \infty$, then Fourier transforms exist; $f$ does \textbf{not} have to be periodic!
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Fourier Cosine Transform}\end{center}
		\vspace{-1.5em}
		\begin{align*}
			f(x) & \text{ is even}\\
			f(x) &= \sqrt{\frac{2}{\pi}} \int\limits_0^{\infty} \hat{f}_c(\omega) \cos(\omega x) \dif \omega\\
			\hat{f}_c(\omega) &= \mathcal{F}_c(f) = \sqrt{\frac{2}{\pi}} \int\limits_0^{\infty} f(x) \cos(\omega x) \dif x
		\end{align*}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textit{Fourier Sine Transform}\end{center}
		\vspace{-1.5em}
		\begin{align*}
			f(x) & \text{ is odd}\\
			f(x) &= \sqrt{\frac{2}{\pi}} \int\limits_0^{\infty} \hat{f}_s(\omega) \sin(\omega x) \dif \omega\\
			\hat{f}_s(\omega) &= \mathcal{F}_s(f) = \sqrt{\frac{2}{\pi}} \int\limits_0^{\infty} f(x) \sin(\omega x) \dif x
		\end{align*}
	\end{minipage}
	
	Transforms are linear operations: $\mathcal{F}_{c/s}\left( a f(x) + b g(x) \right) = a \mathcal{F}_{c/s}\left[ f(x) \right] + \mathcal{F}_{c/s}\left( g(x) \right)$. Some derivative properties:
		
	\begin{minipage}[t]{0.45\textwidth}
		\begin{align*}
			\mathcal{F}_c\left( f'(x) \right) = \omega \mathcal{F}_s \left( f(x) \right) - \sqrt{\frac{2}{\pi}} f(0)\\
			\mathcal{F}_c\left( f''(x) \right) = -\omega^2 \mathcal{F}_c \left( f(x) \right) - \sqrt{\frac{2}{\pi}} f'(0)\\
		\end{align*}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\vspace{-1em}
		\begin{align*}
			\mathcal{F}_s\left( f'(x) \right) = -\omega \mathcal{F}_c \left( f(x) \right)\\
			\mathcal{F}_s\left( f''(x) \right) = -\omega^2 \mathcal{F}_s \left( f(x) \right) + \sqrt{\frac{2}{\pi}} \omega f(0)\\
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Fourier Series - Frequency Analysis Approach}\end{center}
	\index{Fourier Series!frequency analysis}
	Let $f(t)$ be periodic over [0, $\tau$], period $\tau$. The $f$ is approximated by
	
	\begin{minipage}[t!]{0.45\textwidth}
		\[ p(t) = \sum\limits_{k = -\infty}^{\infty} a_k e^{-i k \omega_0 t}, \quad k = 0,1,2,\dots \]	
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.45\textwidth}
		\begin{align*}
			\omega_0 &= \frac{2\pi}{\tau} \quad \text{fundamental frequency}\\
			a_k &= \frac{1}{\tau} \int \limits_0^{\tau} f(t) e^{-i k \omega_0 t} \dif f, \quad a_{-k} = \bar{a}_k
		\end{align*}
	\end{minipage}
	
	or, alternatively
	
	\begin{minipage}[t!]{0.45\textwidth}
		\[ p(t) = a_0 + \sum\limits_{k = 1}^{\infty} \alpha_k \cos(\omega_0 k t) + \beta_k \sin(\omega_0 k t) \]	
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t!]{0.45\textwidth}
		\begin{align*}
			a_0 &= \frac{1}{\tau} \int\limits_0^{\tau} f(t) \dif t\\
			\alpha_k &= 2 \real(a_k) \qquad \beta_k = 2 \imag(a_k)
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Power}\end{center}
		\vspace{-1em}
		Total power of $f(t)$ is
		\[ \frac{1}{\tau} \int\limits_0^{\tau} |f(t)|^2 \dif t = |a_0|^2 + 2\sum\limits_{k=1}^{\infty} |a_k|^2 \]
		Power of one specific frequency band, $k$, is $|a_k|^2$
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Error}\end{center}
		\vspace{-1em}
		Error in approximation $p(t)$ is
		\[ \frac{1}{\tau} \int\limits_0^{\tau} |f(t) - p(t)|^2 \dif t = 2\sum\limits_{k \geq q}^{\infty} |a_k|^2 \]
		RMS Error = $\sqrt{\text{Error}}$
	\end{minipage}
\end{contentBox}