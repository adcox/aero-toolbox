\section{Differentiation}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Partial Differentiation}\end{center}
	Let $f$ be a function of two independent variables $x$ and $y$. The partial of $f$ w.r.t. $x$ is computed by treating $y$ as a constant and taking a normal derivative.
	
	\begin{minipage}[t]{0.45\textwidth}
		Notation:
		\begin{itemize}
			\item $f_x = \pd{f}{x}$
			\item $f_{x_1 x_2} = \pd{}{x_2} \left( \pd{f}{x_1} \right)$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Chain Rule}\end{center}
	\begin{minipage}[t]{0.5\textwidth}
		Given $f(x,y)$ where $x = g(t)$, $y = h(t)$:
		\[\dpd{f}{t} = \dpd{f}{x}\dod{x}{t} + \dpd{f}{y}\dod{y}{t}\]	
	\end{minipage}%
	\begin{minipage}[t]{0.5\textwidth}
		Given $f(x,y)$ where $y = g(x)$:
		\[\dpd{f}{x} = \dpd{f}{x}\dod{x}{x} + \dpd{f}{y}\dod{y}{x} = \dpd{f}{x} + \dpd{f}{y}\dod{y}{x}\]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Equality of Mixed Partials}\end{center}
	If $f_x$, $f_y$, $f_{xy}$, $f_{yx}$ are continuous in some neighborhood of ($x_0$, $y_0$) then $f_{xy} = f_{yx}$ at ($x_0$, $y_0$); generally holds for higher order partials if $f$ is ``well-behaved''
\end{contentBox}

\begin{contentBox}
	\footnotesize%	
	\begin{center}\textbf{Gradient}\end{center}
	\begin{minipage}[t]{0.46\textwidth}
		Given a scalar function $f(x_1,\dots,x_n)$, the gradient is
		\[ \grad f = \vec{\nabla} f = \dpd{f}{x_1}\hat{x}_1 + \dots + \dpd{f}{x_n} \hat{x}_n \]
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		Properties:
		\begin{align*}
			\vec{\nabla}(f^n) &= n f^{n-1} \vec{\nabla} f\\
			\vec{\nabla}(fg) &= f \vec{\nabla} g + g \vec{\nabla} f\\
			\vec{\nabla}(f/g) &= (1/g^2) \left(\vec{\nabla} f - f \vec{\nabla} g \right)
		\end{align*}
	\end{minipage}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Physical Context}\end{center}
		The gradient of $f$ at point $\vec{X}$ gives the direction of maximum increase at $\vec{X}$. The direction of maximum decrease is the opposite direction.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%	
	\begin{center}\textbf{Directional Derivative}\end{center}
	The rate of change of $f$ along an arbitrary direction, $\hat{b}$
	\[ \text{D}_b f = \hat{b} \dotp \vec{\nabla} f \]
\end{contentBox}

\begin{contentBox}
	\footnotesize%	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Divergence}\end{center}
		The divergence of a vector field, $\vec{v} = \vec{v}(x, y, z)$, is
		\begin{align*}
			\dvg \vec{v} &= \vec{\nabla} \dotp \vec{v} = \left[ \dpd{}{x},\, \dpd{}{y},\, \dpd{}{z} \right] \dotp \left[ v_1,\, v_2,\, v_3 \right]\\
			&= \dpd{v_1}{x} + \dpd{v_2}{y} + \dpd{v_3}{z}
		\end{align*}
		Properties:
		\vspace{-1em}
		\begin{align*}
			\dvg (f \vec{v} ) &= f \dvg \vec{v} + \vec{v} \dotp \vec{\nabla} f\\
			\dvg (f \vec{\nabla} g) &= f \vec{\nabla}^2 g + \vec{\nabla}f \dotp \vec{\nabla} g
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Curl}\end{center}
		The curl of a vector field, $\vec{v} = \vec{v}(x, y, z)$, is
		\begin{align*}
			\curl \vec{v} &= \vec{\nabla} \times \vec{v} =
			\begin{vmatrix}
				\hat{i} & \hat{j} & \hat{k}\\
				\pd{}{x} & \pd{}{y} & \pd{}{z}\\
				v_1 & v_2 & v_3
			\end{vmatrix}\\
			&= \left( \dpd{v_3}{y} - \dpd{v_2}{z} \right) \hat{i} + \left( \dpd{v_1}{z} - \dpd{v_3}{x} \right) \hat{j} + \left( \dpd{v_2}{x} - \dpd{v_1}{y} \right) \hat{k}
		\end{align*}
		Properties:
		\vspace{-1em}
		\begin{align*}
			\curl (f \vec{v}) &= \vec{\nabla}f \times \vec{v} + f \curl \vec{v}\\
			\dvg(\vec{u} \times \vec{v}) &= \vec{v} \dotp \curl \vec{u} - \vec{u} \dotp \curl \vec{v}
		\end{align*}
		If $\curl \vec{v} = \vec{0}$, the field is termed ``irrotational''
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%	
	\begin{center}\textbf{Gradient - Divergence - Curl Relationships}\end{center}
	
	\begin{minipage}[t]{0.32\textwidth}
		\begin{align*}
			\vec{\nabla}^2 f &= \dvg \left( \vec{\nabla} f \right) \quad \text{``Laplacian of $f$''}\\
			\vec{\nabla}^2 (fg) &= g \vec{\nabla}^2 f + 2 \vec{\nabla} f \dotp \vec{\nabla} g + f \vec{\nabla}^2 g
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{align*}
			\curl ( \vec{\nabla} f) &= \vec{0}\\
			\dvg( \curl \vec{v} ) = 0
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
	
	\end{minipage}
\end{contentBox}