\section{Integration}

\begin{contentBox}
	\footnotesize%	
	\begin{center}\textbf{Line Integral}\end{center}
	Similar to regular integral, but integrate over variable curve $C$ instead of 1D axis (i.e., $t$, $x$, etc.). It is necessary that the curve be parameterized by some variable, $\sigma$, and represented by some function, $C: \vec{r}(\sigma) = \{ x(\sigma),\, y(\sigma),\, z(\sigma) \}$
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		Define $\vec{F}(\vec{r})$ be a multivariate vector function and $C$ a curve defined by $\vec{r}(\sigma)$. The line integral of $\vec{F}$ over $C$ is
		\begin{align*}
			\int\limits_C \vec{F}(\vec{r}) \dotp \dif \vec{r} &= \int\limits_a^b  \vec{F}\left( \vec{r}(\sigma) \right) \dotp \dod{\vec{r}(\sigma)}{\sigma} \dif \sigma\\
			&= \int\limits_a^b \left( F_1 x' + F_2 y' + F_3 z' \right) \dif \sigma
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		If $C$ is closed, we also write
		\[ \oint\limits_C \vec{F}(\vec{r}) \dotp \dif \vec{r} = \int\limits_C \vec{F}(\vec{r}) \dotp \dif \vec{r} \]
	\end{minipage}
	
	\begin{minipage}[t]{0.46\textwidth}
		Properties
		\vspace{-1em}
		\begin{align*}
			\int\limits_C k \vec{F} \dotp \dif \vec{r} &= k \int\limits_C \vec{F} \dotp \dif \vec{r}\\
			\int\limits_C (\vec{F} + \vec{G}) \dotp \dif \vec{r} &= \int\limits_C \vec{F} \dotp \dif \vec{r} + \int\limits_C \vec{G} \dotp \dif \vec{r}
		\end{align*}
	\end{minipage}%
	\begin{minipage}[t]{0.46\textwidth}
		If $C$ can be subdivided into two arcs, $C_1$ and $C_2$, then
		\[ \int\limits_C \vec{F} \dotp \dif \vec{r} = \int\limits_{C_1} \vec{F} \dotp \dif \vec{r} + \int\limits_{C_2} \vec{F} \dotp \dif \vec{r} \]
		Any representations of $C$ that give the same positive direction (direction of increasing $\sigma$) on $C$ also yield the same value of the line integral.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Path Independence}\end{center}
	The line integral $\int\limits_C \vec{F}(\vec{r}) \dotp \dif \vec{r}$ is \textit{path independent} in D if, for every pair of endpoints $A$ and $B$ in domain $D$, the integral has the same value for all paths in $D$ that begin at $A$ and end at $B$. More formally,
	\begin{enumerate}
		\item A line integral of $\vec{F}$ with continuous components, $F_i$, in a domain $D$ is path independent iff $\vec{F}$ is the gradient of some function $f$ in $D$, i.e.,
		\[ \vec{F} = \grad f \quad \to \quad F_i = \dpd{f}{x_i} \qquad \text{which results in: } \int\limits_A^B \vec{F}(\vec{r}) \dotp \dif \vec{r} = f(B) - f(A)\]
		\item Similarly, the line integral is path independent in $D$ iff its value around every closed path in $D$ is zero.
		\item The line integral is path independent in $D$ iff the differential form, $\vec{F} \dotp \dif \vec{r} = \sum\limits_{i=1}^n F_i \dif x_i$, has continuous coefficient functions, $F_i$ and is \textit{exact} in $D$, i.e., $D$ is simply connected and $\curl \vec{F} = \vec{0}$. In 3D this amounts to
		\[ \dpd{F_3}{y} = \dpd{F_2}{z}, \quad \dpd{F_1}{z} = \dpd{F_3}{x}, \quad \dpd{F_2}{x} = \dpd{F_1}{y} \]
	\end{enumerate}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Green's Theorem - Planar Functions}\end{center}
	Consider the function $\vec{F}(x,y) = F_1 \hat{x} + F_2 \hat{y}$. Let $R$ be a closed, bounded region in the $xy$-plane whose boundary $C$ consists of finitely many smooth curves. Let $F_1(x,y)$ and $F_2(x,y)$ be continuous and have continuous partial derivatives $\pd{F_1}{y}$ and $\pd{F_2}{x}$ everywhere in some domain containing $R$. Then
	\[ \iint\limits_R \left( \dpd{F_2}{x} - \dpd{F_1}{y} \right) \dif x \dif y = \oint\limits_C (F_1 \dif x + F_2 \dif y), \qquad \text{or,} \qquad \iint\limits_R (\curl \vec{F}) \dotp \hat{z} \dif x \dif y = \oint \limits_C \vec{F} \dotp \dif \vec{r}\]
	The integration of $C$ proceeds such that $R$ is on the left as we advance in the direction of integration
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Application of Green's Thm to Planar Areas}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Cartesian}\end{center}
		\vspace{-1em}
		\[\text{Area $A$ of region $R$ is} \quad A = \frac{1}{2} \oint\limits_C (x \dif y - y \dif x) \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Polar}\end{center}
		\vspace{-1em}
		\[ \text{Area $A$ of region $R$ is} \quad A = \frac{1}{2} \oint\limits_C r^2 \dif \theta \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Application of Green's Thm to Planar Laplacian Double Integral}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		Let the function $w(x,y)$ be continuous and have continuous first and second partial derivatives in a domain of the $xy$-plane containing a region $R$ of the type indicated in Green's Thm. Then
	\[ \iint\limits_R \nabla^2 w \dif x \dif y = \oint\limits_C \dpd{w}{n} \dif s \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		where $\pd{w}{n}$ is the \textit{normal derivative} of $w$,
		\[ \dpd{w}{n} = (\grad w) \dotp \hat{n} = \dpd{w}{x} \dod{y}{s} - \dpd{w}{y} \dod{x}{s} \]
	\end{minipage}
	
	Implication: Any function that satisfies Laplace's Equation, $\nabla^2 w = 0$, yields a zero derivative, both of the double areal integral and the closed curve integral.
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Surface Integral}\end{center}
	Consider a piecewise smooth surface $S$ represented by the parameterized function $\vec{r}(u,v)$ where ($u$, $v$) vary over the region $R$; the normal vector of $S$ is $\vec{n} = \vec{r}_u \times \vec{r}_v$. Then the surface integral of a vector function $\vec{F}$ over $S$ is
	\[ \iint\limits_S \vec{F} \dotp \hat{n} \dif A = \iint\limits_R \vec{F}\left( \vec{r}(u,v) \right) \dotp \vec{n}(u,v) \dif u \dif v = \iint\limits_R (F_1 n_1 + F_2 n_2 + F_3 n_3) \dif u \dif v = \iint\limits_S (F_1 \dif y \dif z + F_2 \dif z \dif x + F_3 \dif x \dif y) \]
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Change of Orientation}\end{center}
		\vspace{-1em}
		The replacement of $\hat{n}$ with $-\hat{n}$ corresponds to the multiplication of the integral by -1
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Physical Applications}\end{center}
		\vspace{-1em}
		\begin{itemize}
			\item If $\vec{F}$ represents a fluid flow, the surface integral gives the \textit{flux} through $S$
		\end{itemize}
		
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Nonoriented Surface Integral}\end{center}
	\[ \iint\limits_S G(\vec{r}) \dif A = \iint\limits_R G\left( \vec{r}(u,v) \right) | \vec{n}(u,v) | \dif u \dif v \]
	\begin{itemize}
		\item This integral ignores orientation by taking the absolute value of the normal vector
		\item If $G(\vec{r})$ is a mass density distribution, then the integral yields the mass of $S$
		\item Set $G = 1$ and the integral yields the area of $S$:
		\[ A(S) = \iint\limits_S \dif A = \iint\limits_R | \vec{r}_u \times \vec{r}_v | \dif u \dif v \]
	\end{itemize}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Gauss's Divergence Theorem}\end{center}
	Let $T$ be a closed, bounded region whose boundary is a piecewise smooth, orientable surface $S$. Let $\vec{F}(x,y,z)$ be a vector function that is continuous and has continuous first partial derivatives in some domain containing $T$. Then
	\begin{align*}
		\iiint\limits_T \dvg \vec{F} \dif V &= \iint\limits_S \vec{F} \dotp \hat{n} \dif A\\
		\iiint\limits_T \left( \dpd{F_1}{x} + \dpd{F_2}{y} + \dpd{F_3}{z} \right) \dif x \dif y \dif z &= \iint\limits_S \left( F_1 \dif y \dif z + F_2 \dif z \dif x + F_3 \dif x \dif y \right)
	\end{align*}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Stoke's Theorem}\end{center}
	Generalizes Green's Thm in the plane. Let $S$ be a piecewise smooth oriented surface and let the boundary of $S$ be a piecewise smooth simple closed curve $C$. Let $\vec{F}(x,y,z)$ be continuous with continuous first partial derivatives in a domain containing $S$. Then
	\[ \iint\limits_S (\curl \vec{F}) \dotp \hat{n} \dif A = \oint\limits_C \vec{F} \dotp \vec{r}\,'(s) \dif s \]
	where $\hat{n}$ is the unit normal vector of $S$, and $\vec{r}\,'$ is $\od{\vec{r}}{s}$ ($\vec{r}$ is unit tangent vector and $s$ is arc length of $C$). The cyclic integral is completed in the direction of the right-hand rotation about $\hat{n}$.
\end{contentBox}