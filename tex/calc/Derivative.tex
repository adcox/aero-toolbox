\section{Differentiation}
\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Limits}\end{center}
		\begin{itemize}
			\item If the limit exists, then $\lim\limits_{x\to c} = L$ is defined as follows: $\exists$ a $\delta > 0$ such that if $|x - c| < \delta$ and $x \neq c$, then $|f(x) - L| < \epsilon$ for any small $\epsilon > 0$
			\item The limit exists if the limit is the same from either side, i.e. $\lim\limits_{x \to c^-}f(x) = \lim\limits_{x \to c^+}f(x)$
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Derivative Definition}\end{center}
		Let $y = f(x)$, then the average rate of change of $f$ over $[a, a+h]$ is
		\[ \frac{\Delta y}{\Delta x} = \frac{f(a + h) - f(a)}{h} \]
		and the instantaneous rate of change of $f$ at $a$ is
		\[ f'(a) = \lim\limits_{h\to 0} \frac{f(a + h) - f(a)}{h} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Notation}\end{center}
		\begin{itemize}
			\item Newtonian Notation: $f(x)$, $f''(x)$, $f^{(7)}(x)$
			\item Leibniz Notation: $\dod{f}{x}$, $\dod[2]{f}{x}$
		\end{itemize}
	\end{minipage}%
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Linear Operator}\end{center}
		The derivative is a linear operator:
		\begin{itemize}
			\item $\dod{}{x}\left[c f(x) \right] = c f'(x)$
			\item $\dod{}{x}\left[ f(x) + g(x) \right] = f'(x) + g'(x)$
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Power Rule}\end{center}
		$f(x) = x^n \quad \to \quad f'(x) = n x^{n-1}$
	\end{minipage}
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Exponential Functions}\end{center}
		\begin{align*}
			f(x) &= a^x \quad \to \quad f'(x) = \ln(a) a^x\\
			f(x) &= e^x \quad \to \quad f'(x) = e^x
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Product Rule}\end{center}
		\[\dod{}{x}\left[ f(x)g(x) \right] = f'(x)g(x) + f(x)g'(x)\]
	\end{minipage}
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Quotient Rule}\end{center}
		\[\dod{}{x}\left[ \frac{f(x)}{g(x)} \right] = \frac{f'(x)g(x) - f(x)g'(x)}{g(x)^2}\]
	\end{minipage}
	\begin{minipage}[t]{0.33\textwidth}
		\begin{center}\textbf{Chain Rule}\end{center}
		\[ y = f(g(x)) \quad \to \quad y' = f'(g(x)) g'(x) \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Implicit Differentiation}\end{center}
	??????????
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Parametric Equations}\end{center}
		A set of equations that describe the coordinates of a curve as a function of a variable, or ``parameter'' (e.g. time). For example $\sin(t)$ and $\cos(t)$ parameterize the unit circle as a function of time $t$.
	\end{minipage}%
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.475\textwidth}
		\begin{center}\textbf{Local Linearization}\end{center}
		Local linearization of $f$ near $x = a$: $y \approx f(a) + f'(a)(x - a)$. The error in this estimate is $E = f(x) - [f(a) + f'(a)(x-a)]$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{L'Hopital's Rule}\end{center}
		\[ \lim\limits_{x \to a} \left( \frac{f(x)}{g(x)} \right) = \lim\limits_{x \to a} \left( \frac{f'(x)}{g'(x)} \right) \]
		This rule can be applied recursively and applies when one of the following conditions is met:
		\begin{itemize}
			\item $f(a) = f(b) = 0$
			\item $\lim\limits_{x\to a}f(x) = \pm \infty$ AND $\lim\limits_{x \to a} g(x) = \pm \infty$
			\item $a = \pm \infty$ AND $\lim\limits_{x\to a}f(x) = \lim\limits_{x\to a}g(x) = 0$
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Optimization}\end{center}
		\begin{itemize}
			\item Critical Point: a point $p$ where $f'(p) = 0$ or $f'(p)$ is undefined
			\item Critical Value: $f(p)$ at the critical point
			\item Global Maximum - $f(x_0)$ if $f(x) \leq f(x_0)$ for all $x$ in the domain of $f$
			\item Global Minimum - $f(x_0)$ if $f(x) \geq f(x_0)$ for all $x$ in the domain of $f$
			\item Local Maximum - $f(x_0)$ if $f(x) \leq f(x_0)$ in some neighborhood of $x_0$
			\item Local Minimum - $f(x_0)$ if $f(x) \geq f(x_0)$ in some neighborhood of $x_0$
			\item Extremum - either a minimum or maximum
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Locating Extrema (or Optimal Values)}\end{center}
	All extremum are located at critical points (end points on interval may be local min/max but are not technically extremum)
	\begin{minipage}[t]{0.5\textwidth}
		On closed interval $I = [x_1, x_2]$:
		\begin{itemize}
			\item Find critical points, evaluate
			\item Check end points
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.5\textwidth}
		On open interval $I = (x_1, x_2)$:
		\begin{itemize}
			\item Find critical points, evaluate
			\item Check end behavior
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Increasing Function Theorem}\end{center}\vspace{-3mm}
		If $f$ is continuous on $[a,b]$ and differentiable on $(a,b)$ then:
		\begin{itemize}
			\item $f$ is increasing on $[a,b]$ if $f'(x) > 0$ on $(a,b)$
			\item $f$ is non-decreasing on $[a,b]$ if $f'(x) \geq 0$ on $(a,b)$
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Racetrack Principle}\end{center}\vspace{-3mm}
		Let $g$ and $h$ be continuous on $[a,b]$ and differentiable on $(a,b)$, and $g'(x) \leq h'(x)$ for $a < x < b$
		\begin{itemize}
			\item If $g(a) = h(a)$ then $g(x) \leq h(x)$ for $a \leq x \leq b$
			\item If $g(b) = h(b)$ then $g(x) \geq h(x)$ for $a \leq x \leq b$
		\end{itemize}
	\end{minipage}
	
	\vspace{2mm}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Mean Value Theorem}\end{center}\vspace{-3mm}
		If $f$ is continuous on $[a,b]$ and differentiable on $(a,b)$ then there exists a point $c$ with $a < c < b$ such that $f'(c) = \text{avg. slope} = [f(b) - f(a)]/(b-a)$
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}\textbf{Extreme Value Theorem}\end{center}\vspace{-3mm}
		If $f$ is continuous on $[a,b]$ then $f$ has a global max and min on $[a,b]$
	\end{minipage}
\end{contentBox}