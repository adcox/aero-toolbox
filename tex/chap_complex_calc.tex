\chapter{Complex Calculus}
Complex numbers are a generalization of real numbers, i.e., the analysis in this chapter applies equally to real numbers represented as complex numbers with zero imaginary part.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Definitions}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Complex Numbers}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		An ordered pair $z = (x,y)$ with a real part, $x$, and imaginary part, $y$
		\begin{align*}
			x &= \real z\\
			y &= \imag z
		\end{align*}
		If $x = 0$, $z$ is termed \textit{pure imaginary}. The \textit{imaginary unit} is defined
		\begin{align*}
			i &= (0,1)\\
			&= \sqrt{-1}
		\end{align*}
		($i$ is sometimes replaced by $j$)
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		In practice $z = (x,y)$ is typically written $z = x + iy$. Properties of two complex numbers $z_1 = x_1 + i y_1)$ and $z_2 = x_2 + i y_2)$ are:
		\begin{itemize}
			\item Addition: $z_1 + z_2 = (x_1 + x_2) + i (y_1 + y_2)$
			\item Multiplication: $z_1 z_2 = (x_1 x_2 - y_1 y_2) + i (x_1 y_2 + x_2 y_1)$ (foil the expression to arrive at this result)
			\item Complex Conjugate: $\bar{z}_1 = x_1 - i y_1$
			\item Division:
			\begin{align*}
				z &= \frac{z_1}{z_2} = \frac{z_1 \bar{z}_2}{z_2 \bar{z_2}}
				 &= \frac{x_1 x_2 + y_1 y_2}{x_2^2 + y_2^2} + i \frac{x_2 y_1 - x_1 y_2}{x_2^2 + y_2^2}
			\end{align*}
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Geometric Representation}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Cartesian Form}\end{center}
		Define the horizontal axis to be the \textit{real axis} and the vertical axis to be the \textit{imaginary axis}. The plane spanned by these axis is the \textit{complex plane}. Complex numbers are 2D vectors in this basis; addition and scalar multiplication are equivalent to vector operations (i.e., draw complex numbers as vectors for further insight).
		\begin{center}
			\includegraphics[width=0.6\textwidth]{ComplexPlane.pdf}
		\end{center}
		
		\begin{center}\textit{Triangle Inequality}\end{center}
		\[ | z_1 + \dots + z_n | \leq |z_1| + \dots + |z_n| \]
		
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Polar Form}\end{center}
		Complex numbers may be transformed to polar coords. just as regular Cartesian coordinates:
		\begin{align*}
			x &= r\cos\theta\\
			y &= r\sin\theta\\
			z &= r(\cos\theta + i\sin\theta) \quad \text{``polar form''}\\
			|z| &= r = \sqrt{x^2 + y^2} = \sqrt{z \bar{z}} \quad \text{``absolute value'' or ``modulus''}\\
			\theta &= \arctan(y/x) = \arg z
		\end{align*}
		The principal value is $-\pi < \text{Arg} z \leq \pi$. Consider the properties of complex operations, in polar form, on $z_1 = r_1(\cos\theta_1 + i\sin\theta_1)$ and $z_2 = r_2(\cos\theta_2 + i\sin\theta_2)$:
		\begin{itemize}
			\item Multiplication:
			\begin{align*}
				z_1 z_2 &= r_1 r_2 \left[ \cos(\theta_1 + \theta_2) + i\sin(\theta_1 + \theta_2) \right]\\
				|z_1 z_2| &= |z_1| |z_2|\\
				\arg(z_1 z_2) &= \arg z_1 + \arg z_2
			\end{align*}
			(Trig identities to combine products of sin and cos)
			\item Division:
			\begin{align*}
				z_1/z_2 &= r_1/r_2 \left[ \cos(\theta_1 - \theta_2) + i\sin(\theta_1 - \theta_2) \right]\\
				\begin{vmatrix}\frac{z_1}{z_2}\end{vmatrix} &= \frac{|z_1|}{|z_2|}\\
				\arg(z_1/z_2) &= \arg z_1 - \arg z_2
			\end{align*}
		\end{itemize}
	\end{minipage}	
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Powers and Roots of $z$}\end{center}
	Given $z = r \left( \cos \theta + i \sin \theta \right)$:		

	\begin{minipage}[t]{0.46\textwidth}
		\vspace{-1em}
		\begin{align*}
			z^n &= r^n e^{i n \theta}\\
				&= r^n \left( \cos(n\theta) + i\sin(n\theta) \right) \quad n = 1,2,\dots\\
				&= r^n \left( \cos(n\theta) - i\sin(n\theta) \right) \quad n = -1,-2,\dots
		\end{align*}
		Example: $(1+i)^{12} = \left( \sqrt{2} e^{i \pi/4} \right)^{12} = 2^6 e^{3\pi i} = -2^6$
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\vspace{-1em}
		\begin{align*}
			\sqrt[n]{z} &= \sqrt[n]{r} \left[ \cos \left( \frac{\theta + 2k\pi}{n} \right) + i \sin \left( \frac{\theta + 2k\pi}{n} \right) \right]\\
			\sqrt[n]{1} &= \cos \left( \frac{2k\pi}{n} \right) + i \sin \left( \frac{2k\pi}{n} \right) \quad k = \pm 1, \pm 2,\dots, \pm (n-1)
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Sets in the Complex Plain}\end{center}
	Let $S$ be a set of complex points
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Point Set}: any collection of points (e.g., solutions to quadratic equation, points of a line, points interior a circle, etc.)
			\item \textit{Open Set}: $S$ is open if every point of $S$ has a neighborhood consisting entirely of points that belong to $S$
			\item \textit{Connected Set}: $S$ is connected if any two of its points can be joined by a chain of finitely many straight-line segments all of whose points belong to $S$
			\item \textit{Domain}: An open and connected set
			\item \textit{Complement}: The complement of $S$ is the set of all points that do not belong to $S$
			\item \textit{Bounded Domain}: A domain that lies entirely in some circle about the origin
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Simply Connected Set}: $S$ is simply connected if every simple closed curve (closed curve without self-intersections) encloses only points of $D$ (circular disk is simply connected but an annulus is not)
			\item \textit{Simply Connected Domain}: Every simple closed path in $D$ encloses only points in $D$
			\item \textit{Multiply Connected Domain}: A domain that is not simply connected (e.g., annulus)
			\item \textit{$p$-fold Connected Domain}: A domain with a boundary that consists of $p$ closed connected sets without common points; these sets can be curves, segments, or single points; $D$ has $p$ ``holes''
			\item \textit{Closed Set}: $S$ is closed if its complement is open
			\item \textit{Boundary Point}: a point with every neighborhood that contains both points that belong and don't belong to $S$ (e.g., points on the bounding circles of an open annulus)
			\item \textit{Boundary}: Set of all boundary points of $S$
			\item \textit{Region}: A set consisting of a domain and perhaps some or all of its boundary points (this definition is a bit flexible based on the author).
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Common Complex Point Sets}\end{center}
	Define: $z$, $a \in \mathbb{C}$, and $\rho \in \mathbb{R} > 0$		
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Unit Circle}: Points in the complex plane s.t. $|z| = 1$
			\item \textit{Open Circular Disk}: All points $z$ near point $a$ s.t. $|z - a| < \rho$
			\item \textit{Closed Circular Disk}: All points $z$ near point $a$ s.t. $|z - a| \leq \rho$
			\item \textit{Neighborhood} (or \textit{$\rho$-neighborhood}): the open circular disk with radius $\rho$ about the point of interest
		\end{itemize}
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Open Annulus}: Allpoints $z$ near point $a$ s.t. $\rho_1 < |z - a| < \rho_2$
			\item \textit{Closed Annulus}: Allpoints $z$ near point $a$ s.t. $\rho_1 \leq |z - a| \leq \rho_2$
			\item \textit{Upper Half-Plane}: All points $z$ with $\imag z > 0$
			\item \textit{Lower Half-Plane}: All points $z$ with $\imag z < 0$
			\item \textit{Right Half-Plane}: All points $z$ with $\real z > 0$
			\item \textit{Left Half-Plane}: All points $z$ with $\real z < 0$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Tips and Tricks}\end{center}
	
	\begin{itemize}
		\item To remove a complex number from the denominator of a fraction, multiply top and bottom by complex conjugate of denominator.
		\item Large exponents: Transform to polar form, e.g. $(1+i)^{12} = \left( \sqrt{2} e^{i \pi/4} \right)^12 = 2^6 e^{3\pi i} = -2^6$
	\end{itemize}
	\begin{minipage}[t]{0.46\textwidth}
	
	\end{minipage}
\end{contentBox}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Complex Functions}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Complex Function}\end{center}
		A complex function $f$ maps points $z$ from the set $S$ to another set of values
		\[ w = f(z)\,, \]
		then $S$ is the \textit{domain} of $f$ (usually open and connected), and the set of all $w$ points is the \textit{range} of $f$; $z$ is a \textit{complex variable}. If $w = u + iv$, which depends on $z = x + iy$, then we can write
		\[ w = f(z) = u(x,y) + iv(x,y)\,, \]
		which demonstrates that the complex function $f$ is equivalent to a pair of real funcions $u(x,y)$ and $v(x,y)$.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Limit}\end{center}
		A function $f(z)$ has the limit $l$ as $z$ approaches $z_0$, $\underset{z \to z_0}{\lim} f(z) = l$, if $f$ is defined in a neighborhood of $z_0$ (though not necessarily at $z_0$) and if the values of $f$ are ``close'' to $l$. Very similar to calculus definition, but in the real case $x$ must approach $x_0$ on the real line while in the complex case $z$ may approach $z_0$ from any direction.
		
		\begin{center}\textbf{Continuity}\end{center}
		\vspace{-1em}
		$f(z)$ is \textit{continuous} at $z = z_0$ if $f(z_0)$ is defined and 
		\[ \underset{z \to z_0}{\lim} f(z) = f(z_0) \]
		$f$ is continuous in a domain if it is continuous at each point of the domain.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Derivative}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		The derivative of a complex function $f$ at point $z_0$ is written $f'(z_0)$ and is defined by
		\[ f'(z_0) = \underset{\Delta z \to 0}{\lim} \frac{f(z_0 + \Delta z) - f(z_0)}{\delta z} \]
		provided the limit exists. Then $f$ is said to be \textit{differentiable} at $z_0$. Note that the limit may take any path to $z_0$; the function $f(z) = \bar{z}$ is not differentiable because the limit takes a different value depending on the direction of the path to $z_0$.
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		Complex differentiation rules are identical to real differentiation (i.e. chain rule, product rule, quotient rule, etc.)
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Analyticity}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item A function $f(z)$ is \textit{analytic in a domain $D$} (also called \textit{holomorphic in $D$}) if $f(z)$ is defined and differentiable at all points of $D$.
			\item $f(z)$ is \textit{analytic at a point $z = z_0$} in $D$ if $f$ is analytic in a neighborhood of $z_0$
			\item $f(z)$ is an \textit{entire function} if $f$ is analytic $\forall\,\, z$
		\end{itemize}
		 
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
	Complex analysis requires that a function be analytic on some domain, i.e. differentiable in that domain. The analyticity of a function may be determined from the Cauchy-Riemann equations
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Cauchy-Riemann Equations}\end{center}
	Let $w = f(z) = u(x,y) + i v(x,y)$ be defined and continuous in some neighborhood of a point $z = x + iy$ and differentiable at $z$ itself. $f$ is analytic in a domain $D$ iff the following equations apply everywhere in $D$:
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Cartesian Form}\end{center}
		\vspace{-0.5em}
		\[ \dpd{u}{x} = \dpd{v}{y}, \qquad \dpd{u}{y} = -\dpd{v}{x} \]
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Polar Form}\end{center}
		\vspace{-0.5em}
		\[ \dpd{u}{r} = \frac{1}{r}\dpd{v}{\theta}, \qquad \dpd{v}{r} = -\frac{1}{r}\dpd{u}{\theta} \]	
	\end{minipage}
	
	These equations are the result of equating two limits taken along the real and imaginary axes (a condition for differentiability).
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Laplace's Equation}\end{center}
		If $f(z) = u(x,y) + iv(x,y)$ is analytic in a domain $D$, then both $u$ and $v$ satisfy Laplace's equation
		\[ \nabla^2 u = u_{xx} + u_{yy} = 0\] and
		\[ \nabla^2 v = v_{xx} + v_{yy} = 0\] in $D$ and have continuous second partial derivatives in $D$.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Harmonic Functions}\end{center}
		\begin{itemize}
			\item Solutions of Laplace's Eqs that have continuous second-order partial derivatives are called \textit{harmonic functions} and their theory is called potential theory (gravitational, eletro-static, etc.). 
			\item If two harmonic functions $u$ and $v$ satisfy the Cauchy-Riemann Eqs in a domain $D$ they are the real and imaginary parts of an analytic function $f$ in $D$. Then $v$ is said to be a \textit{harmonic conjugate function} of $u$ in $D$ (not related to complex conjugate).
		\end{itemize}
	\end{minipage}
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Exponential Function}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\[ e^z = e^x ( \cos y + i \sin y) \]
		This function simply extends the real exponential $e^x$ and has the following properties:
		\begin{itemize}
			\item $e^z$ is analytic for all $z$
			\item The derivative, $(e^z)'$ is $e^z$
			\item $e^{z_1 + z_2} = e^{z_1} e^{z_2}$
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.46\textwidth}
		If $x = 0$ and $z$ is purely imaginary, we have Euler's formula:
		\[ e^{iy} = \cos y + i \sin y \qquad \text{note: } |e^{iy}| = 1 \, \forall \, y, \quad e^{z + 2\pi i} = e^z \, \forall\, z \]
		Hence the polar form of a complex number may be written
		\[ z = re^{i\theta} \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Trigonometric Functions}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Real Functions}\end{center}
		\vspace{-1em}
		\begin{align*}
			\cos x &= \frac{1}{2} \left( e^{ix} + e^{-ix} \right)\\
			\sin x &= \frac{1}{2i} \left( e^{ix} - e^{-ix} \right)\\
			\cosh x &= \frac{1}{2} \left( e^x + e^{-x} \right)\\
			\sinh x &= \frac{1}{2} \left( e^x - e^{-x} \right)
		\end{align*}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textit{Complex Functions}\end{center}
		\vspace{-1em}
		\begin{align*}
			\cos z &= \frac{1}{2} \left( e^{iz} + e^{-iz} \right)\\
			\sin z &= \frac{1}{2i} \left( e^{iz} - e^{-iz} \right)\\
			\cosh z &= \frac{1}{2} \left( e^z + e^{-z} \right)\\
			\sinh z &= \frac{1}{2} \left( e^z - e^{-z} \right)
		\end{align*}
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.46\textwidth}
		Euler's Formula is valid in complex:
		\[ e^{iz} = \cos z + i \sin z \]
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		Complex trig and hyperbolic functions are related:
		\begin{align*}
			\cosh iz &= \cos z, \qquad \sinh iz = i \sin z\\
			\cos iz &= \cosh z, \qquad \sin iz = i \sinh z
		\end{align*}
	\end{minipage}
	
	The usual trig identities ($\tan$, $\sec$, $\csc$, sum, product, quotient, etc.) still hold with complex arguments in place of real arguments. The basic trig functions ($\cos$ $\sin$, $\cosh$, and $\sinh$) are \textit{entire} (analytic everywhere); others ($\tan$, $\tanh$, etc.) are entire except for specific singularity points, as with the real functions.
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Complex Powers and Logarithms}\end{center}
	
	\begin{minipage}[t!]{0.46\textwidth}
		\begin{align*}
			\ln z &= \ln r + i\theta \qquad (r = |z| > 0, \,\, \theta = \arg z)\\
			\text{Ln} z &= \ln |z| + i \text{Arg} z \qquad \text{(principal value of } \ln z)\\
			\ln z &= \text{Ln} z \pm 2n\pi i, \quad n = 0,1,2,\dots
		\end{align*}
		Each of the infinitely many solutions of $\ln z$ is called a \textit{branch} of the logarithm. The negative real axis is known as a \textit{branch cut} and the branch for $n = 0$ is called the \textit{principal branch} of $\ln z$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t!]{0.46\textwidth}
		For every $n = 0, \pm 1, \pm 2, \dots$, $\ln z = \text{Ln} z \pm 2n\pi i$ defines a function that is analytic except at 0 and on the negative real axis and has the derivative
		\[ \left( \ln z \right)' = \frac{1}{z} \qquad (z \text{ not 0 or negative real}) \]
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t!]{0.46\textwidth}
		\begin{align*}
			z^c &= e^{c \ln z} \qquad (\text{multivalued, } c \in \mathbb{C}, z \neq 0)\\
			z^c &= e^{c \text{Ln} z} \qquad \text{(principal value)}
		\end{align*}
		
	\end{minipage}%
	\hspace{0.03\textwidth}
	\begin{minipage}[t!]{0.46\textwidth}
		It is convention that, for real positive $z = x$, the expression $z^c$ takes the principal value
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Inverse Trigonometric and Hyperbolic Functions}\end{center}
	
	\begin{minipage}[t!]{0.33\textwidth}
		\begin{align*}
			\arccos z &= -i \ln \left( z + \sqrt{z^2 - 1} \right)\\
			\arcsin z &= -i \ln \left( iz + \sqrt{1 - z^2} \right)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t!]{0.33\textwidth}
		\begin{align*}
			\arccosh z &= \ln \left( z + \sqrt{z^2 - 1} \right)\\
			\arcsinh z &= \ln \left( z + \sqrt{z^2 + 1} \right)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t!]{0.33\textwidth}
		\begin{align*}
			\arctan z &= \frac{i}{2} \ln \left( \frac{i + z}{i - z} \right)\\
			\arctanh z &= \frac{1}{2} \ln \left( \frac{1 + z}{1 - z} \right)
		\end{align*}
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Integration}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Complex Line Integral}\end{center}
	Defined identically to the real line integral, but the curve of integration, $C: z(t) = x(t) + i y(t)$, is defined in the complex plane rather than the real plane. We assume that $C$ is piecewise smooth, i.e., has a continuous nonzero derivative (tangent vector), $\dot{z}(t) = \od{z}{t} = \dot{x}(t) + i \dot{y}(t)$. The line integral (and closed curve integral) are denoted
	\[ \int\limits_C f(z) \dif z \qquad \text{and} \qquad \oint\limits_C f(z) \dif z \]
	The usual linearity properties hold, as well as integration direction reversal (flip the sign), and curve partitioning. The complex line integral exists if $f(z)$ is continuous and $C$ is piecewise smooth.
\end{contentBox}

\begin{contentBox}
	\footnotesize
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Indefinite Integration of Analytic Functions}\end{center}
		Let $f(z)$ be analytic in a simply connected domain $D$. Then $\exists$ an indefinite integral of $f(z)$ in $D$, i.e., an analytic function $F(z)$ s.t. $F'(z) = f(z)$ in $D$ and $\forall$ paths in $D$ joining two points $z_0$ and $z_1$ in $D$ we have
		\[ \int\limits_{z_0}^{z_1} f(z) \dif z = F(z_1) - F(z_0) \]	
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Path Integration of Analytic Functions}\end{center}
		Let $C$ be a piecewise smooth path, represented by $z = z(t)$, where $a \leq t \leq b$. Let $f(z)$ be a continuous function on $C$. Then
		\[ \int\limits_C f(z) \dif z = \int\limits_a^b f[z(t)] \dot{z}(t) \dif t \qquad \left( \dot{z} = \dod{z}{t} \right)\]
		Steps to Apply:
		\begin{enumerate}
			\item Represent $C$ in form $z(t)$ on $[a,\, b]$
			\item Calculate the derivative $\dot{z}(t) = \dif z/ \dif t$
			\item Sub $z(t)$ for every $z$ in $f(z)$ (i.e., $x(t)$ for $x$, etc.)
			\item Integrate $f[z(t)] \dot{z}(t)$ over $t$ from $a$ to $b$
		\end{enumerate}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{$ML$ Inequality}\end{center}
		To estimate the upper bound of an integral:
		\[ \begin{vmatrix} \int\limits_C f(z) \dif z \end{vmatrix} \leq ML \]
		where $L$ is the length of $C$ and $M$ is the maximum absolute value of $f(z)$ on $C$, i.e., $|f(z)| \leq M$ everywhere on $C$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Cauchy's Integral Theorem}\end{center}
		If $f(z)$ is analytic in a simply connected domain $D$, then for every simple closed path $C$ in $D$,
		\[ \oint\limits_C f(z) \dif z = 0 \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Path Independence}\end{center}
		If $f(z)$ is analytic in a simply connected domain $D$, then the integral of $f(z)$ is independent of path in $D$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Cauchy's Integral Formula}\end{center}
		Let $f(z)$ be analytic in a simply connected domain $D$. Then for any point $z_0$ in $D$ and any piecewise smooth simple closed path $C$ in $D$ that encloses $z_0$,
		\[ \oint\limits_C \frac{f(z)}{z - z_0} \dif z = 2 \pi i f(z_0) \]
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Derivatives of an Analytic Function}\\ \textbf{(Generalized Cauchy Integral Formula)}\end{center}
		\vspace{-0.5em}
		If $f(z)$ is analytic in a domain $D$, then it has derivatives of all orders in $D$, which are then also analytic functions in $D$. The values of these derivatives at a point $z_0$ in $D$ are given by,
		\[ \oint\limits_C \frac{f(z)}{(z - z_0)^{n+1}} \dif z = \frac{2 \pi i}{n!} f^{(n)}(z_0)\qquad (n = 1,2,\dots) \]
		where $C$ is any simple closed path in $D$ that encloses $z_0$ and whose full interior belongs to $D$; integrate counterclockwise around $C$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Cauchy's Inequality}\end{center}
		Apply the $ML$ inequality to Cauchy's integral formula to find
		\[ | f^{(n)}(z_0) | \leq \frac{n! M}{r^n} \]
		where $M$ is the maximum of the absolute value of $f(z)$ on $C$ (i.e., $|f(z)| \leq M$ on $C$) and $C$ is a circle of radius $r$ centered on $z_0$.
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Liouville's Theorem}\end{center}
		If an entire function (the function is \textit{entire}) is bounded in absolute value in the whole complex plain, then this function must be a constant, i.e., $f = u + iv = $ const $\forall \, \, z \in \mathbb{C}$.
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Morera's Theorem (Converse of Cauchy's Integral Thm)}\end{center}
		If $f(z)$ is continuous in a simply connected domain $D$ and if
		\[ \oint\limits_C f(z) \dif z = 0 \] for every closed path in $D$, then $f(z)$ is analytic in $D$.
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Power Series}
Notes: Skipped Kreyszig Ch 15.1, 15.3, 15.4, 15.5; I would like to read and include these at some later point. Greenberg also has a few chapters for additional reading.

\vspace{1em}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Power Series Definition}\end{center}
		A power series in $z$ about $z_0$ (the \textit{center}) takes the form
		\[ \sum\limits_{n=0}^{\infty} a_n(z - z_0)^n \]
		where $a_n$ are complex constants called \textit{coefficients}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Power Series Convergence}\end{center}
		\begin{itemize}
			\item Every power series converges at the center $z_0$
			\item If a power series converges at $z = z_1 \neq z_0$, it converges absolutely for every $z$ closer to $z_0$ than $z_1$
			\item If a power series diverges at $z = z_2$, it diverges for every $z$ farther away from $z_0$ than $z_2$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Radius of Convergence}\end{center}
		The \textit{circle of convergence} is described by the \textit{radius of convergence}, $R = |z - z_0|$ inside which all points of a given power series converge.
		\begin{itemize}
			\item All points within the circle ($|z - z_0| < R$) converge
			\item All points outside the circle ($|z - z_0| > R$) diverge
			\item No general statements can be made about the convergence of the power series for points \textit{on} the circle ($|z - z_0| = R$)
			\item Notation $R = \infty$: the power series converges $\forall \, \, z$
			\item Notation $R = 0$: the power series converges only at the center, $z = z_0$
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Cauchy-Hadamard Formula}\end{center}
		Suppose that the sequence $|a_{n+1}/a_n|$ for $n = 1,2,\dots$ converges with limi $L^*$. If $L^* = 0$, then $R = \infty$. If $L^* \neq 0$, then
		\[ R = \frac{1}{L^*} = \underset{n \to \infty}{\lim} \begin{vmatrix} \frac{a_n}{a_{n+1}} \end{vmatrix} \]
		If $|a_{n+1}/a_n| \to \infty$, then $R = 0$.
	\end{minipage}
\end{contentBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Residue Integration}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Definitions}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Singular Point} - Point $z$ at which $f(z)$ ceases to be analytic
			\item \textit{Isolated Singular Point} - $z_0$ s.t. $f(z)$ is analytic in an annulus $ 0 < |z - z_0| < \rho$ for some $\rho > 0$.
			\item \textit{Nonisolated Singular Point} - A singular point that is not isolated
			\item \textit{Zero} - Point $z$ at which $f(z) = 0$
		\end{itemize}
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{itemize}
			\item \textit{Pole} - Point $z$ at which $f(z)$ tends to infinity (e.g., zero of the denominator). \textit{Need more research here - See Kreyszig Ch 16}
			\item \textit{Simple Pole} - A pole with order 1, e.g., $f = 1/(1-z)$ has a simple pole at $z = 1$.
			\item \textit{Higher-Order Pole} - A pole with order $m$, e.g., $f = 1/(1-z)^m$ has an $m$-order pole at $z = 1$.
			\item \textit{Essential Singularity} - A pole that has infinite order, i.e., the Laurent Series expansion continues with infinite negative powers of the form $(z - z_0)$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Residue at Simple Pole}\end{center}
		\[ \underset{z = z_0}{\text{Res}} f(z) = b_1 = \underset{z \to z_0}{\lim} (z - z_0) f(z) \]
		where $z_0$ is a simple pole of $f(z)$ and $b_1$ is the coefficient of the first negative power $1/(z-z_0)$ in the Laurent Series of $f(z)$. Alternatively,
		\[ \underset{z = z_0}{\text{Res}} f(z) = \underset{z = z_0}{\text{Res}} \frac{p(z)}{q(z)} = \frac{p(z_0)}{q'(z_0)} \]
		where the $'$ indicates a derivative w.r.t. $z$
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		\begin{center}\textbf{Residue at $m$-order Pole}\end{center}	
		\[ \underset{z = z_0}{\text{Res}} f(z) = \frac{1}{(m-1)!} \underset{z \to z_0}{\lim} \left\{ \dod[m-1]{}{z} \left[ (z - z_0)^m f(z) \right] \right\} \]
		This does not apply if the pole is an essential singularity (infinite order).
	\end{minipage}\
	
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Residue Theorem}\end{center}
	Let $f(z)$ be an analytic function within and on a piecewise smooth simple closed path $C$ except for finitely many isolated singular points $z_1, z_2,\dots$ that lie \textit{inside} $C$. Then the contour integral of $f(z)$ taken CCW around $C$ is
	\[ \oint\limits_C f(z) \dif z = 2\pi i \sum\limits_{j=1}^k \underset{z = z_j}{\text{Res}} f(z) \]
	Note: Residue Thm does not apply if $f(z)$ has singularities \textit{on} $C$
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Application of Residue Thm - Trig Integrals}\end{center}
	
	\begin{minipage}[t]{0.25\textwidth}
		For integrals of the type
		\[ J = \int\limits_0^{2 \pi} F( \cos \theta,\, \sin \theta) \dif \theta \]
		(We require limits [$0$, $2\pi$] for a closed contour $C$; i.e., [$0$, $\pi$] does not result in a closed contour and the Residue Thm does not apply)
	\end{minipage}
	\begin{minipage}[t]{0.4\textwidth}
		Substitute
		\begin{align*}
			\cos \theta &= \frac{1}{2} \left( e^{i \theta} + e^{-i \theta} \right) = \frac{1}{2} \left( z + \frac{1}{z} \right) \\
			\sin \theta &= \frac{1}{2i} \left( e^{i \theta} - e^{-i \theta} \right) = \frac{1}{2i} \left( z - \frac{1}{z} \right)
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.3\textwidth}
		To arrive at
		\begin{align*}
			J &= \oint\limits_C f(z) \frac{\dif z}{i z}\\
			C &: |z| = 1
		\end{align*}
		Solve with the Residue Theorem
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Application of Residue Thm - Improper Integrals}\end{center}
	The principal value of the improper integral is given by a sum of residues:
	\[ \int\limits_{- \infty}^{\infty} f(x) \dif x = 2\pi i \sum \text{Res} f(z) + \pi i \sum \text{Res} f(z) \]
	
	\begin{minipage}[t]{0.46\textwidth}
		The first sum includes all poles in the upper half-plane that are not on the real axis
	\end{minipage}%
	\hspace{0.03\textwidth}%
	\begin{minipage}[t]{0.46\textwidth}
		The second sum includes all poles on the real axis
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize
	\begin{center}\textbf{Application of Residue Thm - Fourier Integrals}\end{center}
	
	\begin{minipage}[t]{0.46\textwidth}
		\[ \int\limits_{-\infty}^{\infty} f(x) \cos (s x) \dif x = -2 \pi \sum \left[ \imag \text{Res} \left( f(z) e^{isz} \right) \right] \]
	\end{minipage}
	\begin{minipage}[t]{0.46\textwidth}
		\[ \int\limits_{-\infty}^{\infty} f(x) \sin (s x) \dif x = -2 \pi \sum \left[ \real \text{Res} \left( f(z) e^{isz} \right) \right] \]
	\end{minipage}
	
	Only consider poles of $f(z) e^{isz}$ in the upper half-plane
\end{contentBox}
%\begin{contentBox}
%	\footnotesize
%	\begin{center}\textbf{Title}\end{center}
%\end{contentBox}