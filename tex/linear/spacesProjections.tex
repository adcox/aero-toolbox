\section{Vector Spaces, Transformations, Projections}
\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Vector Spaces}\end{center}
	\index{Vector Space}
	\begin{itemize}
		\item Examples: $\mathbb{R}^2$ (real, two-vectors), $\mathbb{R}^{2\times 2}$ (real, 2x2 matrices)
	\end{itemize}
	
	\begin{minipage}[t!]{0.45\textwidth}
		A vector space $V$ is
		\begin{itemize}
			\item Nonempty set of vectors w/ same \# components
			\item Closed under linear combinations, i.e.,
			\[\vec{a} \in V,\, \vec{b} \in V,\, (\alpha\vec{a} + \beta\vec{b}) \in V\]
			\item Includes the zero vector, $\vec{0}$
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		Terms
		\begin{itemize}
			\item Dimension: $\dim V = $ max \# lin. indep. vectors
			\item Span: A set of vectors that can be linearly combined to form any $\vec{v} \in V$
			\item Basis: A set of linearly indp. vectors that span $V$
			\item Subspace: Nonempty subset of $V$ that forms itself a vector space and is closed under linear combinations
		\end{itemize}
	\end{minipage}
	
	\begin{center}\textbf{Four Fundamental Subspaces of} $\coxmat{A} \in \mathbb{R}^{m \times n}$\end{center}
	
	\begin{minipage}[t!]{0.22\textwidth}
		\begin{center}\textit{Column Space}\end{center}
		\index{Vector Space!column space}
		\[ C(\coxmat{A}) \in \mathbb{R}^{m}\]
		\begin{itemize}
			\item dim = rank$\coxmat{A} = r$
			\item Lin. indep. set of columns that span $\coxmat{A}$
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.22\textwidth}
		\begin{center}\textit{Row Space}\end{center}
		\index{Vector Space!row space}
		\[ C(\coxmat{A}^T) \in \mathbb{R}^{n}\]
		\begin{itemize}
			\item dim = rank$\coxmat{A} = r$
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.26\textwidth}
		\begin{center}\textit{Null Space}\end{center}
		\index{Vector Space!null space}
		\[ N(\coxmat{A}) = \left\{ \vec{x} \eval[1]{} \coxmat{A}\vec{x} = \vec{0},\, \vec{x} \neq \vec{0} \right\} \in \mathbb{R}^n\]
		\begin{itemize}
			\item dim = rank$\coxmat{A} = n - r$
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t!]{0.23\textwidth}
		\begin{center}\textit{Left Null Space}\end{center}
		\index{Vector Space!left null space}
		\[ N(\coxmat{A}^T) \in \mathbb{R}^m\]
		\begin{itemize}
			\item dim = rank$\coxmat{A} = m - r$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Solutions of Linear Systems: Existence and Uniqueness}\end{center}
	\index{Linear System!solution}
	\[ \coxmat{A} ~:~ m \times n \qquad \coxmat{A} \vec{x} = \vec{b} \qquad \coxmat{\tilde{A}} = \begin{augmat}{1} \coxmat{A} & \vec{b} \end{augmat} \]
	\begin{minipage}[t]{0.45\textwidth}
		\begin{itemize}
			\item \textit{Existence}: System is \textit{consistent} (has a sol'n) iff $\coxmat{A}$ and $\coxmat{\tilde{A}}$ have the same rank
			\item \textit{Uniqueness}: System has
			\begin{itemize}
				\item[*] 1 sol'n iff $\rank\coxmat{A} = \rank\coxmat{\tilde{A}} = n$
				\item[*] $\infty$ sol'ns otherwise
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		Sol'n may be written $\vec{x} = \vec{x}_p + c\vec{x}_h$
		\begin{itemize}
			\item $x_p$ is a single, ``particular'' solution to system
			\item $x_h$ solves the homogenous system $\coxmat{A}\vec{x} = \vec{0}$, i.e., $\vec{x} \in N(\coxmat{A})$
			\item $c$ is a scalar
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Linear Transformations}\end{center}
	\index{Transformation!linear}
	Given $\coxmat{A} \in \mathbb{R}^{m \times n}$, $\coxmat{A}$ defines a linear transformation from $\mathbb{R}^n \to \mathbb{R}^m$, i.e., it maps $\vec{x} \to \coxmat{A}\vec{x} = \vec{b}$
	\begin{itemize}
		\item $\coxmat{A}$ always maps $\vec{0} \in \mathbb{R}^n \to \vec{0} \in \mathbb{R}^m$
		\item if $\vec{x}, ~\vec{y} \in \mathbb{R}^n$, $\alpha,~\beta \in \mathbb{R}$, then $\coxmat{A}(\alpha \vec{x} + \beta \vec{y}) = \alpha \coxmat{A} \vec{x} + \beta \coxmat{A} \vec{y}$
		\item $\coxmat{A}$ is defined $\forall \mathbb{R}^n$ but its image (column space) may not be all of $\mathbb{R}^m$
	\end{itemize}

	\begin{center}Examples\end{center}
	
	\begin{minipage}[t]{0.25\textwidth}
		\[\begin{bmatrix}c & 0\\0 & c\end{bmatrix}\]
		\begin{center}scales by $c$\end{center}
	\end{minipage}%
	\begin{minipage}[t]{0.25\textwidth}
		\[\begin{bmatrix}C_{\theta} & -S_{\theta}\\S_{\theta} & C_{\theta}\end{bmatrix}\]
		\begin{center}rotates by $\theta$\end{center}
	\end{minipage}
	\begin{minipage}[t]{0.25\textwidth}
		\[\begin{bmatrix}0 & 1\\1 & 0\end{bmatrix}\]
		\begin{center}reflection about $x = y$\end{center}
	\end{minipage}
	\begin{minipage}[t]{0.25\textwidth}
		\[\begin{bmatrix}1 & 0\\0 & 0\end{bmatrix}\]
		\begin{center}projection onto $x$-axis\end{center}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Projections Onto Lines}\end{center}
	\index{Transformation!Projection!line}
	\begin{minipage}[t!]{0.2\textwidth}
		\includegraphics[width=0.95\textwidth]{linearProjection.pdf}
	\end{minipage}
	\hspace{0.02\textwidth}
	\begin{minipage}[t!]{0.35\textwidth}
		Want to choose $x$ s.t. $\vec{e} \perp \vec{a} ~\to~ (\vec{b} - x\vec{a}) \perp \vec{a} ~\to~ \vec{a}^T(\vec{b} - x\vec{a}) = 0$. Solve for $x$:
		\[ \vec{a}^T x \vec{a} = \vec{a}^T \vec{b} \qquad x = \frac{\vec{a}^T \vec{b}}{\vec{a}^T \vec{a}} = \frac{\vec{a} \dotp \vec{b}}{a^2} \]
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t!]{0.35\textwidth}
		Can define matrix to project any vector onto $\vec{a}$:
		\[ \coxmat{P} = \frac{1}{\vec{a}^T \vec{a}} \left[ \vec{a}\vec{a}^T \right] \]
		$\coxmat{P}\vec{b}$ projects $\vec{b}$ onto $\vec{a}$
	\end{minipage}
	\[ \coxmat{P} = \coxmat{P}^2 \qquad \rank \coxmat{P} = 1 \qquad C(\coxmat{P}) = \text{ line through } \vec{a} \qquad N(\coxmat{P}) = \text{ plane } \perp \vec{a} \qquad \trace \coxmat{A} = 1 \]
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Projections into Spaces}\end{center}
	\index{Transformation!Projection!vector space}
	Projection matrix $\coxmat{P} = \coxmat{A}(\coxmat{A}^T\coxmat{A})^{-1} \coxmat{A}^T$ projects any vector $\vec{b}$ into $C(\coxmat{A})$
	\[\coxmat{P} = \coxmat{P}^2 \qquad \coxmat{P} = \coxmat{P}^T \qquad \vec{b} - \coxmat{P}\vec{b} \text{ is the orthogonal complement of } \coxmat{P}\vec{b} \]
	Least squares: $\coxmat{A}\vec{x} = \vec{b}$, $\coxmat{A}$ is $m\times n$ with $m > n$, likely 0 sol'ns. The least squares sol'n, via left inverse, $\vec{x} = (\coxmat{A}^T\coxmat{A})^{-1} \coxmat{A}^T \vec{b}$.
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Intersections and Sums of Subspaces}\end{center}
	\index{Vector Space!subspace}
	\index{Subspace|seealso{Vector Space}}
	$V$ \& $W$ are subspaces of $\mathbb{R}^n$. $V \cap W = \left\{ \vec{x} \in \mathbb{R}^n \eval[2]{} \vec{x} \in V,\, \vec{x} \in W \right\}$, $V + W = \left\{ \vec{x} + \vec{y} \eval[2]{} x \in V,\, y \in W \right\}$
	\begin{itemize}
		\item Dimension Formula: $\dim[V + W] + \dim[V \cap W] = \dim[V] + \dim[W]$
		\item Let $V = C(\coxmat{A})$, $W = C(\coxmat{B})$, then:
	\end{itemize}
	\vspace{0.5em}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Sums\end{center}
		\index{Subspace!sum}
		\vspace{-1em}
		\begin{align*}
			V + W &= C([\coxmat{A} ~~\coxmat{B}])\\
			\dim[V + W] &= \dim([\coxmat{A}~~\coxmat{B}])
		\end{align*}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Intersections\end{center}
		\index{Subspace!intersection}
		\vspace{-1em}
		\begin{align*}
			N(\coxmat{A}) \cap N(\coxmat{B}) &= N\left( \begin{bmatrix}\coxmat{A}\\\coxmat{B}\end{bmatrix} \right)\\
			N([\coxmat{A}~~\coxmat{B}]) ~~&\to~~ (\dim[V] + \dim[W])\times 1 \text{ vector}
		\end{align*}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Orthogonality}\end{center}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Vectors\end{center}
		\index{Orthogonality!vector}
		Vectors $\vec{g}_1, ~\vec{g}_2,~\dots,~\vec{g}_n$ are orthogonal iff
		\[\vec{g}_i^T \vec{g}_i = \left\{ \begin{array}{l l} 0 & i \neq j\\1 & i = j \end{array} \right.\]
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Matrices\end{center}
		\index{Orthogonality!matrix}
		Matrix $\coxmat{Q}$ is orthogonal iff $\coxmat{Q}$ is square and $\coxmat{Q}^T\coxmat{Q} = \coxmat{I}$.
		\begin{itemize}
			\item Multiplying a vector by $\coxmat{Q}$ preserves its length, i.e., $\norm{\coxmat{Q}\vec{x}} = \norm{\vec{x}}$
			\item DCMs, permutation matrices, reflection matrices: all orthogonal
		\end{itemize}
	\end{minipage}
	
	\vspace{1em}
	
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Gram-Schmidt Process:\\Constructing an Orthonormal Basis\end{center}
		\index{Orthogonality!basis construction|seealso{Gram-Schmidt}}
		\index{Gram-Schmidt}
		Given three lin. indep. vectors $\vec{a}$, $\vec{b}$, $\vec{c}$, find orthonormal vectors $\hat{q}_i$ that span the same space
		\begin{enumerate}
			\item $\hat{q}_1 = \vec{a}/a = \hat{a}$
			\item Define $\vec{b}' = \vec{b} - (\hat{q}_1^T \vec{b}) \hat{q}_1$ (project $\vec{b}$ onto $\vec{q}_1$, get orthogonal complement)
			\item $\hat{q}_2 = \vec{b}'/b'$
			\item Define $\vec{c}' = \vec{c} - (\hat{q}_1^T \vec{c}) \hat{q}_1 - (\hat{q}_2^T \vec{c}) \hat{q}_2$ (orthogonal complement to projections of $\vec{c}$ onto $\hat{q}_1$ and $\hat{q}_2$)
			\item $\hat{q}_3 = \vec{c}'/c'$
		\end{enumerate}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}QR Factorization\end{center}
		\index{Matrix!Decomposition!$QR$}
		Applies to any $\coxmat{A}$ with indep. columns
		\begin{align*}
			\coxmat{A} &= \coxmat{QR}\\
 			&=
			\begin{bmatrix}\hat{q}_1 & \hat{q}_2 & \hat{q}_3 \end{bmatrix}
			\begin{bmatrix}
				\hat{q}_1^T \vec{a} & \hat{q}_1 \vec{b} & \hat{q}_1 \vec{c}\\
				0 & \hat{q}_2 \vec{b} & \hat{q}_2 \vec{c}\\
				0 & 0 & \hat{q}_3 \vec{c}
			\end{bmatrix}
		\end{align*}	
		where
		\begin{itemize}
			\item $\vec{a}$, $\vec{b}$, $\vec{c}$ are cols. of $\coxmat{A}$
			\item cols of $\coxmat{Q}$ are orthonormal; $\coxmat{Q}$ is orthogonal if $m = n$
		\end{itemize}
		$\coxmat{P} = \coxmat{QQ}^T$ projects into $C(\coxmat{A}) = C(\coxmat{Q})$; useful to solve least squares: solve $\coxmat{R}\vec{x} = \coxmat{Q}^T \vec{b}$ for $\vec{x}$ (re $\coxmat{A}\vec{x} = \vec{b}$)
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Determinants}\end{center}
	\index{Matrix!determinant}
	In general,
	\begin{itemize}
		\item $\coxmat{PA} = \coxmat{LDU}$, $\det[ \coxmat{A}] = \pm \det[ \coxmat{D}] = \pm \prod \text{pivots}$
		\item $\det[ \coxmat{A}] = \sum\limits_{j = 1}^n a_{i,j} C_{i,j}$, $i$ is chosen arbitrarily, $C_{i,j} = (-1)^{i + j} \det[ \coxmat{M}_{ij}]$, and $\coxmat{M}_{ij}$ is the submatrix of $\coxmat{A}$ formed by deleting the $i$th row and $j$th column
	\end{itemize}

	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Properties\end{center}
		\begin{itemize}
			\item $\det[ \coxmat{I}] = 1$
			\item Swapping rows of $\coxmat{A}$ reverses the sign of $\det[\coxmat{A}]$
			\item $\det[ \coxmat{AB}] = \det[ \coxmat{A}] \det[ \coxmat{B}]$
			\item $\det[ \coxmat{A}^T] = \det[ \coxmat{A}]$
			\item $\det[ \coxmat{A} ] = \prod \lambda$ (eigenvalues)
		\end{itemize}
	\end{minipage}%
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}More Properties\end{center}
		\begin{itemize}
			\item $\coxmat{A}$ is singular iff $\det [\coxmat{A}] = 0$
			\item Subtracting one row from another in $\coxmat{A}$ does not affect $\det[ \coxmat{A}]$
			\item Triangular $\coxmat{A}$: $\det [\coxmat{A}] = \prod$(diag elements)
			\item $\det [k\coxmat{A}] = k^n \det [\coxmat{A}]$
			\item $\det[ \coxmat{A}^{-1}] = 1/\det[ \coxmat{A}]$
		\end{itemize}
	\end{minipage}
	
	\begin{center}Cramer's Rule: Solving $\coxmat{A}\vec{x} = \vec{b}$\end{center}
	\index{Cramer's Rule}
	\index{Linear System!solution|seealso{Cramer's Rule}}
	\[ 
		\vec{b} = \begin{Bmatrix}b_1\\ \vdots \\ b_n\end{Bmatrix} \qquad
		\vec{x} = \begin{Bmatrix}x_1\\ \vdots \\ x_n\end{Bmatrix} \qquad
		\to \qquad x_j = \frac{\det[\coxmat{B}_j]}{\det[\coxmat{A}]}\,, \qquad
		\coxmat{B}_j = \begin{bmatrix}
			a_{11} & \dots & b_1 & \dots & a_{1n}\\
			\vdots & & \vdots & & \vdots\\
			a_{1n} & \dots & b_j & \dots & a_{nn}
		\end{bmatrix} \quad (\text{replace jth col of } \coxmat{A} \text{ w/ } \vec{b})
	\]
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Eigenvalues and Eigenvectors}\end{center}
	\index{Eigenvalue}
	\index{Eigenvector}
	\begin{center}$\lambda$ and $\vec{v}$ are an eigenvalue/vector pair of $n \times n$ matrix $\coxmat{A}$ if $\coxmat{A}\vec{v} = \lambda \vec{v}$\end{center}
	
	\begin{minipage}[t]{0.5\textwidth}
		\begin{itemize}
			\item Eigenvalues: solve $\det[ \lambda \coxmat{I} - \coxmat{A} ] = 0$ or $\det[ \coxmat{A} - \lambda \coxmat{I}]$ for $\lambda$; determinant forms ``characteristic polynomial'' in $\lambda$
			\item Eigenvalues of real matrices are real or occur in complex conjugate pairs
			\item Eigenvectors: solve $[\lambda \coxmat{I} - \coxmat{A}]\vec{v} = \vec{0}$ for $\vec{v} \neq \vec{0}$, i.e., $\vec{v} \in N(\lambda \coxmat{I} - \coxmat{A})$
			\item If $\lambda$, $\vec{v}$ are an eigenvalue/vector pair of $\coxmat{A}$, then $\coxmat{A} + \alpha \coxmat{I}$ has an eigenvalue $\lambda + \alpha$ with eigenvector $\vec{v}$
			\item Eigenvalues of $\coxmat{A}^k$ are $\lambda^k$; eigenvectors are unchanged
			\item The eigenvectors associated with the same eigenvalue of $\coxmat{A}$, together with $\vec{0}$, form a vector space called an ``eigenspace'' of $\coxmat{A}$.
		\end{itemize}
	\end{minipage}%
	\hspace{0.05\textwidth}%
	\begin{minipage}[t]{0.4\textwidth}
		\begin{center}Multiplicity\end{center}
		Values associated with an eigenvalue
		\begin{itemize}
			\item Algebraic Multiplicity, $M_{\lambda}$ = \# eigenvalues with same value \index{Eigenvalue!algebraic multiplicity}
			\item Geometric Multiplicity, $m_{\lambda}$ = \# lin. indep. eigenvectors associated with a single eigenvalue \index{Eigenvalue!geometric multiplicity}
			\item $\sum M_{\lambda} = n$, ($n$ solutions to char. poly)
			\item In general, $m_{\lambda} \leq M_{\lambda} \leq n$
			\item Defect of $\lambda$, $\Delta_{\lambda} = M_{\lambda} - m_{\lambda}$ \index{Eigenvalue!defect}
			\item If $\Delta_{\lambda} = 0$, $\lambda$ is ``nondefective,'' else, ``defective''
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Defective Matrices}\end{center}
	\index{Matrix!Type!defective}
	\begin{minipage}[t]{0.47\textwidth}
		A matrix is \textit{defective} if
		\begin{itemize}
			\item It has one or more defective eigenvalues
		\end{itemize}	
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.47\textwidth}
		A matrix is \textit{nondefective} if
		\begin{itemize}
			\item $\coxmat{A}$ is $n\times n$ and has $n$ distinct eigenvalues
			\item $\coxmat{A}$ is Hermitian
			\item $\coxmat{A}$ is Unitary
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Definite Matrices, Quadratic Form}\end{center}
	\begin{minipage}[t]{0.57\textwidth}
		A quadratic form in $\mathbb{R}^n$ is an expression $Q(\vec{x}) = \vec{x}^T \coxmat{A} \vec{x}$ ($\coxmat{A}$ is square, $\vec{x} \in \mathbb{R}^n$). In general, $Q(\vec{x}) = \sum\limits_{i=1}^n \sum\limits_{j=1}^n a_{ij} x_i x_j$; can always be achieved by a symmetric matrix by replacing $a_{ij}$ and $a_{ji}$ with their average.
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.35\textwidth}
		\begin{center}Hermitian Quadratic\end{center}
		\index{Matrix!Type!hermitian}
		For $\vec{x} \in \mathbb{C}^n$ and Hermitian $\coxmat{A}$, $Q(\vec{x}) = \vec{x}^H \coxmat{A} \vec{x}$ is real
	\end{minipage}
	
	\vspace{1em}
	\begin{minipage}[t]{0.47\textwidth}
		$\coxmat{A}$ and $Q(\vec{x})$ are 		
		\begin{itemize}
			\item positive definite if $Q(\vec{x}) > 0 ~\forall~ \vec{x} \neq \vec{0}$
			\begin{itemize}
				\item Notation: $\coxmat{A} > 0$
				\item All eigenvalues of $\coxmat{A}$ are positive
				\item All principal minors (det. of square submatrices include $a_{11}$) are positive
			\end{itemize}
			\item positive semidefinite if $Q(\vec{x}) \geq 0 ~\forall~ \vec{x} \neq \vec{0}$
			\begin{itemize}
				\item Notation: $\coxmat{A} \geq 0$
				\item All eigenvalues are non-negative
				\item All leading principle minors are non-negative
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.47\textwidth}
		$\coxmat{A}$ and $Q(\vec{x})$ are 		
		\begin{itemize}
			\item negative definite if $Q(\vec{x}) < 0 ~\forall~ \vec{x} \neq \vec{0}$
			\begin{itemize}
				\item Or if $\coxmat{-A} > 0$
				\item All eigenvalues of $\coxmat{A}$ are negative
			\end{itemize}
			\item negative semidefinite if $Q(\vec{x}) \leq 0 ~\forall~ \vec{x} \neq \vec{0}$
			\begin{itemize}
				\item Or if $\coxmat{-A} \geq 0$
			\end{itemize}
			\item indefinite if $Q(\vec{x})$ takes both positive and negative values
			\begin{itemize}
				\item Eigenvalues of $\coxmat{A}$ are positive and negative
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\index{Matrix!Type!definite (positive, negative)}
	\index{Matrix!Type!semidefinite (positive, negative)}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Diagonalization - Similarity Transform}\end{center}	
	\index{Transformation!similarity}
	\index{Matrix!diagonalization}
	Similarity transform: $\coxmat{A} = \coxmat{T \Lambda T}^{-1}$, $\coxmat{\Lambda}$ is diagonal, $\coxmat{T}$ is invertible. $\coxmat{T}$ transforms coordinates $\vec{x} = \coxmat{T} \vec{\xi}$
	
	\begin{center}
	\begin{tabular}{c c}
		Continuous & Discrete\\
		$\dot{\vec{x}} = \coxmat{A} \vec{x} \quad \to \quad \dot{\vec{\xi}} = \coxmat{\Lambda} \vec{\xi}$ & $\vec{x}(k+1) = \coxmat{A} \vec{x}(k) \quad \to \quad \vec{\xi}(k+1) = \coxmat{\Lambda} \vec{\xi}(k)$
	\end{tabular}
	\end{center}
	
	\begin{minipage}[t]{0.3\textwidth}
		$\coxmat{A}$ is ``diagonalizable'' iff
		\begin{itemize}
			\item it is similar to a diagonal matrix
			\item it is nondefective		
		\end{itemize}	
	\end{minipage}
	\begin{minipage}[t]{0.65\textwidth}
		Common application: $\coxmat{T}$ is eigenvector matrix $[ \vec{v}_1~~\dots~~\vec{v}_n ]$, $\coxmat{\Lambda} = \diag[ \lambda_1 ~~\dots~~ \lambda_n ]$
		\begin{itemize}
			\item Every matrix with distinct eigenvalues is diagonalizable
			\item Every nondefective matrix is diagonalizable
			\item $\coxmat{A}$ is diagonalizable iff $\coxmat{A}$ has lin. indep. eigenvectors
			\item If $\coxmat{A}$ and $\coxmat{B}$ are diagonalizable and commute ($\coxmat{AB} = \coxmat{BA}$), they share a common eigenvector matrix
		\end{itemize}		
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Complex Vectors, Matrices, and the Hermitian}\end{center}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Operator\end{center}
		\index{Complex!conjugate transpose}
		Complex conjugate transpose is the ``Hermitian'' operator, $\bar{\vec{x}}^T = \vec{x}^H$
		\begin{itemize}
			\item $(\vec{x}^H)^H = \vec{x}$
			\item $\vec{x}$ and $\vec{y}$ are orthogonal if $\vec{x}^H \vec{y} = 0$
			\item $\norm{\vec{x}} = \sqrt{\vec{x}^H \vec{x}}$
			\item $(\coxmat{AB})^H = \coxmat{B}^H \coxmat{A}^H$
		\end{itemize}
	\end{minipage}
	\hspace{0.05\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}Matrix\end{center}
		\index{Matrix!Type!hermitian}
		A matrix $\coxmat{A}$ is Hermitian if $\coxmat{A} = \coxmat{A}^H$
		\begin{itemize}
			\item $\coxmat{A}\vec{x} \in \mathbb{C}$
			\item $\vec{x}^H \coxmat{A} \vec{x} \in \mathbb{R}$
			\item All eigenvalues are real
			\item Eigenvectors are orthogonal to each other if they come from distinct eigenvalues (also true for real, symmetric matrices)
			\item A matrix is unitary if $\coxmat{A}^H \coxmat{A} = \coxmat{AA}^H = \coxmat{I}$ (analogue of orthogonal matrix)
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Similarity and the Jordan Form}\end{center}
	
	\begin{minipage}[t]{0.35\textwidth}
		Similar matrices \index{Matrix!similarity}
		\begin{itemize}
			\item have the same eigenvalues (necessary, not sufficient)
			\item repeat same linear operator with respect to different bases
			\item share same Jordan form (necessary and sufficient)
		\end{itemize}
		Given $\coxmat{B} = \coxmat{M}^{-1}\coxmat{AM}$, every eigenvector $\vec{v}$ of $\coxmat{A}$ corresponds to eigenvector $\coxmat{M}^{-1}\vec{v}$ of $\coxmat{B}$
	\end{minipage}
	\hspace{0.03\textwidth}
	\begin{minipage}[t]{0.5\textwidth}
		If $\coxmat{A}$ has a full set of eigenvectors, then $\coxmat{J} = \coxmat{T}^{-1}\coxmat{AT}$. However, if $\coxmat{A}$ is defective, then, for every missing eigenvector, the Jordan form will have a 1 just above its main diagonal.
		
		If $\coxmat{A}$ has $s$ lin. indep. eigenvectors, the Jordan form is block diagonal,
		\index{Matrix!Decomposition!Jordan}
		\begin{align*}
			\coxmat{J} &= \diag \left[\coxmat{J}_i ~~\dots~~ \coxmat{J}_s \right]\\
			\coxmat{J}_i &= \begin{bmatrix}
				\lambda_i & 1 & 0 & \dots & 0\\
				0 & \lambda_i & 1 & & \vdots\\
				\vdots & & \ddots & \ddots & 0\\
				\vdots & & & \lambda_i & 1 \\
				0 & \dots & \dots & 0 & \lambda_i
			\end{bmatrix}
			\quad \begin{array}{l} \text{\# cols is number of lin. depen.}\\\text{eigenvectors associated with } \lambda_i \end{array}
		\end{align*}
		$\lambda$ will appear in number of blocks consistent with its algebraic multiplicity $M_{\lambda}$
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Singular Value Decomposition: $SVD$}\end{center}
	\index{Matrix!Decomposition!$SVD$}
	\index{Matrix!singular values|seealso{$SVD$}}
	\[ \coxmat{A} = \coxmat{U \Sigma V}^H \qquad \coxmat{A} \in \mathbb{R}^{m\times n},\quad \coxmat{U}_{m\times m},\quad \coxmat{\Sigma}_{m\times n},\quad \coxmat{V}_{n \times n}\]
	\begin{enumerate}
		\item Compute eigenvalues $\lambda_i$ of $\coxmat{AA}^H$ and associated eigenvectors $\vec{v}_i$
		\item Singular values $\sigma_i = \sqrt{\lambda_i}$, sorted from greatest to least ($i = 1,\dots,n$)
		\item Find first $r = \rank\coxmat{A}$ columns of $\coxmat{U}$ via $\vec{u}_j = \sigma_j^{-1}\coxmat{A}\vec{v}_j$, $j=1,\dots,r$
		\item Pick remaining columns so $\coxmat{U}$ is unitary
		\item $\coxmat{V}$ contains eigenvectors as columns, sorted in order of eigenvalue from greatest to least
		\item $\coxmat{\Sigma} = \diag[\sigma_i, \dots, \sigma_n]$
	\end{enumerate}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Special Real Matrices}\end{center}
	\begin{center}\textit{Subset of special complex matrices with $Im$ part = 0}\end{center}

	\vspace{0.5em}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Symmetric}\end{center} \index{Matrix!Type!symmetric}
		\[ \coxmat{A}^T = \coxmat{A} \]
		\begin{itemize}
			\item Eigenvalues: real
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Skew-Symmetric}\end{center} \index{Matrix!Type!skew-symmetric}
		\[ \coxmat{A}^T = -\coxmat{A} \]
		\begin{itemize}
			\item Eigenvalues: imaginary or zero
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Orthogonal}\end{center} \index{Matrix!Type!orthogonal}
		\[ \coxmat{A}^T = \coxmat{A}^{-1} \]
		\begin{itemize}
			\item Eigenvalues: real or complex-conjugate pairs, magnitude 1
			\item Determinant: $\pm 1$
		\end{itemize}
	\end{minipage}
\end{contentBox}

\begin{contentBox}
	\footnotesize%
	\begin{center}\textbf{Special Complex Matrices}\end{center}
	
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Hermitian}\end{center} \index{Matrix!Type!hermitian}
		\[ \coxmat{A}^H = \coxmat{A} \]
		\begin{itemize}
			\item Eigenvalues: real
			\item Always has orthonormal basis of eigenvectors
			\item $\therefore$ is always diagonalizable
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Skew-Hermitian}\end{center} \index{Matrix!Type!skew-hermitian}
		\[ \coxmat{A}^H = -\coxmat{A} \]
		\begin{itemize}
			\item Eigenvalues: imaginary or zero
		\end{itemize}
	\end{minipage}
	\begin{minipage}[t]{0.32\textwidth}
		\begin{center}\textit{Unitary}\end{center} \index{Matrix!Type!unitary}
		\[ \coxmat{A}^H = \coxmat{A}^{-1} \]
		\begin{itemize}
			\item Eigenvalues: magnitude 1 (not necessarily conjugate pairs)
			\item Preserve inner product
		\end{itemize}
	\end{minipage}
\end{contentBox}