TARGET = Aero_Toolbox

all:	doc

full: 	clean all

install: doc
	#rm /Users/andrew/Google_Drive/Purdue/Quals/Notes/Summaries_Cox.pdf
	cp Aero_Toolbox.pdf /Users/andrew/Google_Drive/Purdue/Quals/Notes/Summaries_Cox.pdf

show: doc
	#okular $(TARGET).pdf
	open $(TARGET).pdf

clean:
	-rm -f *.aux
	-rm -f *.toc
	-rm -f *.log
	-rm -f *.bbl
	-rm -f *.blg
	-rm -f *.lof
	-rm -f *.lot
	-rm -f *.out

nuke: clean
	-rm -f $(TARGET).pdf

doc:
	pdflatex $(TARGET)
	makeindex $(TARGET)
	#bibtex $(TARGET)
	pdflatex $(TARGET)
	#bibtex $(TARGET)	
	#pdflatex $(TARGET)


