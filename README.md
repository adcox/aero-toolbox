# Aerospace Toolbox
This document includes summaries of important topics for the study of Aerospace Engineering.

## Installation
Simply run the makefile,

```bash
	cd aero-toolbox
	make
```

You'll need LaTeX installed with the ability to run `pdflatex` and `bibtex`.
