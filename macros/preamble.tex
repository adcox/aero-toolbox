%%
% This file contains the preamble for the whole book. Use
%	input(macros/preamble.tex) to include this in files
%
% Author: Andrew Cox
%%

\documentclass[twoside]{book}		% Define the type of document

%%
% Basic Packages and Options
%%

\usepackage{alphalph,etoolbox}				% Advanced Appendix numbering
\usepackage{amsmath, amsfonts, amssymb}		% basic math symbol libraries
\usepackage{afterpage}						% Allows us to create blank pages
\usepackage{array}
\usepackage{calc}
\usepackage{color} 							% For text coloring
\usepackage{commath}
\usepackage[title,titletoc]{appendix}		% Smart appendix handling
\usepackage{caption}
\usepackage{commath}
\usepackage{enumitem}						% Advanced list options
\usepackage{esint}
\usepackage{fancyvrb}						% extended verbatim environments
\usepackage{fullpage}
\usepackage{framed}							% make frames around stuff
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}	
\usepackage{graphicx}
\usepackage{hyperref}						% hyperlinks		
\usepackage{listings}						% code Listings	
\usepackage{longtable}						% longtable environment
\usepackage{mathtools}
\usepackage{makeidx}
\usepackage{mdframed}
\usepackage{multicol}						% allow multicolumn elements
\usepackage{multirow}						% for table alignment stuff
\usepackage[numbers]{natbib}				% Better control of citations
\usepackage{nomencl}						% nomenclature generation via makeindex
\usepackage{pdflscape}						% Allow landscape orientation environment
\usepackage{setspace}
\usepackage{subcaption}						% subcaptions for subfigures
\usepackage{tablefootnote}					% Allow footnotes in tables
\usepackage{tikz}							% for drawings
\usepackage{titlesec}						% Allow customization of section title
\usepackage[nottoc]{tocbibind}				% include bibliography in TOC
\usepackage{varioref}						% smart referencing
\usepackage[nointegrals]{wasysym}			% Astrological symbols
\usepackage{xcolor}

\makeindex

%%
% Set up filepaths
%%
\graphicspath{{./figs/}}

%%
% Advanced List Properties
%%
\setlist[enumerate]{noitemsep}
\setlist[itemize]{noitemsep}

%%
% Color Definitions
%%
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{lightest-gray}{gray}{0.98}
\definecolor{gray75}{gray}{0.75}

%%
% Code Formatting Rules
%%
\lstset{
  frame=single,
  language=Matlab,
  backgroundcolor=\color{lightest-gray},
  frame=single,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=left,
  numbersep=5pt,                   		% how far the line-numbers are from the code
  numberstyle=\small\color{gray}, 		% the style that is used for the line-numbers
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{orange},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  xleftmargin=2em,
  framexleftmargin=2.0em
}

%%
% mdframed Setup
%%
\newmdenv[skipabove=0, skipbelow=1mm, leftmargin=0, rightmargin=0,%
			innertopmargin=1mm, innerbottommargin=1mm,%
			innerrightmargin=1mm, innerleftmargin=1mm,%
			hidealllines=false, splittopskip=3mm]{contentBox}

%%
% Define the headers and footers for the document
%%

\usepackage{fancyhdr}
\setlength{\headheight}{20.4pt}
\pagestyle{fancy}
\fancypagestyle{normal}{%
  	\fancyhf{} 						% clear all footer fields
  	\fancyhead[LE,RO]{\leftmark}
	\fancyhead[RE,LO]{\rightmark}
  	\fancyfoot[R]{\textcolor{gray}{Aerospace Toolbox} $\vert$ \thepage}
  	
	% Define widths of header and footer lines
	\renewcommand{\headrulewidth}{0.4pt}
	\renewcommand{\footrulewidth}{0.4pt}
	
	% Make those lines gray
	\renewcommand{\headrule}{\hbox to\headwidth{%
    \color{gray75}\leaders\hrule height \headrulewidth\hfill}}
	\renewcommand{\footrule}{\hbox to\headwidth{%
    \color{gray75}\leaders\hrule height \footrulewidth\hfill}}
}
\fancypagestyle{plain}{
	\fancyhf{}
	\fancyfoot[R]{\textcolor{gray}{Aerospace Toolbox} $\vert$ \thepage}
	
	% Define widths of header and footer lines
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0.4pt}
	
	% Make those lines gray
	\renewcommand{\headrule}{\hbox to\headwidth{%
    \color{gray75}\leaders\hrule height \headrulewidth\hfill}}
	\renewcommand{\footrule}{\hbox to\headwidth{%
    \color{gray75}\leaders\hrule height \footrulewidth\hfill}}
}

% Renew commands so that chapter or section names will appear in the header/footer
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}{}}

%%
% Customize Chapter Headings
%%

\titleformat{\chapter}						% Which command we're overriding
	[hang]									% Display style
	{\Huge\bfseries}						% Format
	{\thechapter \hspace{20pt} \textcolor{gray75}{$\vert$} \hspace{15pt}}
	{0pt}									% Separation
	{\Huge\bfseries}						% Before-code
	[\vspace{1ex}{\titlerule[1pt]}]			% After-code
	
\titlespacing{\chapter}{0pt}{-20pt}{20pt}	% 20 pt spacing before (-20pt) and after (20pt) the chapter title

% Renew other commands
\renewcommand{\arraystretch}{1.1}					% Pad tables a tiny bit for readability
\renewcommand{\thefootnote}{\alph{footnote}}		% Make footnotes letters instead of numbers
\renewcommand{\figurename}{Fig.}					% Use Fig. instead of Figure for figure names

%%
% Set up filepaths
%%
\graphicspath{{./figs/}}

%%
% Define custom commands
%%
\newcommand{\link}[2]{\textcolor{blue}{\href{#1}{#2}}}
\renewcommand{\citet}[1]{\cite{#1}}

%%
% Math Stuff
%%

%{display size}{text size}{script size}{scriptscript size}
\DeclareMathSizes{10}{10}{8}{8}

% New definition of square root:
% \let\oldsqrt\sqrt
% % it defines the new \sqrt in terms of the old one
% \def\sqrt{\mathpalette\DHLhksqrt}
% \def\DHLhksqrt#1#2{%
% \setbox0=\hbox{$\oldsqrt[#1]{#2\,}$}\dimen0=\ht0
% \advance\dimen0-0.2\ht0
% \setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
% {\box0\lower0.4pt\box2}}

% Given symbol for conditional probability
\newcommand\givenbase[1][]{\:#1\lvert\:}
\let\given\givenbase
\newcommand\sgiven{\givenbase[\delimsize]}
\DeclarePairedDelimiterX\Basics[1](){\let\given\sgiven #1}

% Other math operators
\DeclareMathOperator{\Lagr}{\text{L}}
\DeclareMathOperator{\Lapl}{\mathcal{L}}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\real}{Re}
\DeclareMathOperator{\imag}{Im}
\DeclareMathOperator{\grad}{grad}	% gradient
\DeclareMathOperator{\curl}{curl}	% curl
\DeclareMathOperator{\dvg}{div}		% divergence

% Extra trig operators
\DeclareMathOperator{\sech}{sech}
\DeclareMathOperator{\csch}{csch}
\DeclareMathOperator{\arcsec}{arcsec}
\DeclareMathOperator{\arccot}{arccot}
\DeclareMathOperator{\arccsc}{arccsc}
\DeclareMathOperator{\arccosh}{arccosh}
\DeclareMathOperator{\arcsinh}{arcsinh}
\DeclareMathOperator{\arctanh}{arctanh}
\DeclareMathOperator{\arcsech}{arcsech}
\DeclareMathOperator{\arccsch}{arccsch}
\DeclareMathOperator{\arccoth}{arccoth} 

% Extra linear algebra operators, commands, and environments
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\trace}{tr}
\newcommand{\coxmat}[1]{\boldsymbol{#1}}

% Augmented matrix usage:
%
%	\begin{augmat}{3}
%		1 & 2 & 3 & 4\\
%		a & b & c & d
%	\end{augmat}
%
% The result is a matrix with a line between the third and fourth columns
\newenvironment{augmat}[1]{%
  \left[\begin{array}{@{}*{#1}{c}|c@{}}
}{%
  \end{array}\right]
}

% Bigger dot product operator
\makeatletter
\newcommand*\dotp{\mathpalette\dotp@{.5}}
\newcommand*\dotp@[2]{\mathbin{\vcenter{\hbox{\scalebox{#2}{$\m@th#1\bullet$}}}}}
\makeatother

%%
% Drawing Stuff
%%
\usetikzlibrary{calc,patterns,snakes}

%%
% Page Stuff
%%
\fvset{fontsize=\footnotesize,xleftmargin=2em}		% set footnote size
\linespread{1.25}
\pagestyle{normal}

\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \newpage}

\input{macros/centerMassSymbol.tex}